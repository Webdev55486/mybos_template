<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="mybos team">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  @yield('metaSection')

  <title>@yield('title')mybos</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:700,400&amp;subset=cyrillic,latin,greek,vietnamese">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/bootstrap/css/bootstrap.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/animate.css/animate.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('fonts/font-awesome/css/font-awesome.min.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('node-waves/waves.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('js/sweetalert/sweetalert.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/mbs-additional.css') }}" type="text/css">
  @yield('headScripts')
  @yield('style')
</head>
<body>
  <div class="page-loader-wrapper">
      <div class="loader">
          <div class="preloader">
              <div class="spinner-layer pl-red">
                  <div class="circle-clipper left">
                      <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                      <div class="circle"></div>
                  </div>
              </div>
          </div>
          <p>Please wait...</p>
      </div>
  </div>
  <section class="mbs-navbar mbs-navbar--freeze mbs-navbar--absolute mbs-navbar--transparent mbs-navbar--sticky mbs-navbar--auto-collapse" id="menu-0">
      <div class="mbs-navbar__section mbs-section">
          <div class="mbs-section__container container-fluid">
              <div class="mbs-navbar__container">
                  <div class="col-md-3 mbs-navbar__column mbs-navbar__column--s mbs-navbar__brand animated fadeInLeft">
                      <span class="mbs-navbar__brand-link mbs-brand mbs-brand--inline">
                          <span class="mbs-brand__logo animated bounceInLeft">
                            <a href="{{url('/')}}">
                              <img src="assets/images/logo_white.png" class="mbs-navbar__brand-img mbs-brand__img logo-white" alt="mybos">
                              <img src="assets/images/logo_black.png" class="mbs-navbar__brand-img mbs-brand__img logo-black" alt="mybos">
                            </a>
                          </span>
                      </span>
                  </div>
                  <div class="mbs-navbar__hamburger mbs-hamburger"><span class="mbs-hamburger__line"></span></div>
                  <a class="mbs-navbar__hamburger mbs-hamburger-login"><i class="material-icons" style="vertical-align:bottom;">perm_identity</i><span class="mbs-haburger-login-text">&nbsp;&nbsp;Login</span></a>
                  <div class="col-md-2 mbs-navbar__column mbs-navbar-column-right animated fadeInRight">
                    <a class="mbs-buttons__link btn mbs-buttons-login" href="http://mybos.com"><i class="material-icons" style="vertical-align:bottom;">perm_identity</i>&nbsp;&nbsp;<span>Login</span>&nbsp;</a>
                  </div>
                  <div class="col-md-7 mbs-navbar__column mbs-navbar__menu mbs-navbar-column-center animated fadeInDown">
                      <nav class="mbs-navbar__menu-box mbs-navbar__menu-box--inline-right">
                          <div class="mbs-navbar__column">
                              <ul class="mbs-navbar__items mbs-navbar__items--right float-left mbs-buttons mbs-buttons--freeze mbs-buttons--right btn-decorator mbs-buttons--active mbs-buttons--only-links">
                                <li class="mbs-navbar__item mbs-navbar-dropdown">
                                  <a class="mbs-buttons__link btn text-white dropdown-toggle" data-toggle="mbs-navbar-dropdown">Products</a>
                                  <ul class="mbs-navbar-dropdown-menu">
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('software')}}">Facility Management Software</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('hardware')}}">CloudSense Hardware</a></li>
                                  </ul>
                                </li>
                                <li class="mbs-navbar__item mbs-navbar-dropdown">
                                  <a class="mbs-buttons__link btn text-white dropdown-toggle" data-toggle="mbs-navbar-dropdown">Industries</a>
                                  <ul class="mbs-navbar-dropdown-menu">
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('industries-facility-management')}}">Building & Facility Management</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('industries-property-management')}}">Strata & Property Management</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('industries-construction-development')}}">Construction & Development</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('industries-manage-agents')}}">Occupants, Owners & Managing Agents</a></li>
                                  </ul>
                                </li>
                                <li class="mbs-navbar__item mbs-navbar-dropdown">
                                  <a class="mbs-buttons__link btn text-white dropdown-toggle" data-toggle="mbs-navbar-dropdown">Resources</a>
                                  <ul class="mbs-navbar-dropdown-menu">
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('resources-case-studies')}}">Case Studies</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('resources-documentation')}}">Documentation</a></li>
                                  </ul>
                                </li>
                                <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('news')}}">News</a></li>
                                <li class="mbs-navbar__item mbs-navbar-dropdown">
                                  <a class="mbs-buttons__link btn text-white dropdown-toggle" data-toggle="mbs-navbar-dropdown">Company</a>
                                  <ul class="mbs-navbar-dropdown-menu">
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('company-support')}}">Support</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('company-support')}}">About Us</a></li>
                                    <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="{{url('company-support')}}">Our Location</a></li>
                                  </ul>
                                </li>
                                <li class="mbs-navbar__item"><a class="mbs-buttons__link btn text-white" href="http://mybos.com">Contact</a></li>
                              </ul>
                          </div>
                      </nav>
                  </div>
              </div>
          </div>
      </div>
      <div class="mbs-sidebar__menu-mobile">
          <ul class="mbs-sidebar__menu-items">
            <li class="mbs-sidebar__item mbs-sidebar-dropdown">
              <span class="btn dropdown-toggle">Products</span>
              <ul class="mbs-sidebar-dropdown-menu">
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('software')}}">Facility Management Software</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('hardware')}}">CloudSense Hardware</a></li>
              </ul>
            </li>
            <li class="mbs-sidebar__item mbs-sidebar-dropdown">
              <span class="btn dropdown-toggle">Industries</span>
              <ul class="mbs-sidebar-dropdown-menu">
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('industries-facility-management')}}">Building & Facility Management</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('industries-property-management')}}">Strata & Property Management</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('industries-construction-development')}}">Construction & Development</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('industries-manage-agents')}}">Occupants, Owners & Managing Agents</a></li>
              </ul>
            </li>
            <li class="mbs-sidebar__item mbs-sidebar-dropdown">
              <span class="btn dropdown-toggle">Resources</span>
              <ul class="mbs-sidebar-dropdown-menu">
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('resources-case-studies')}}">Case Studies</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('resources-documentation')}}">Documentation</a></li>
              </ul>
            </li>
            <li class="mbs-sidebar__item"><a class="btn" href="{{url('news')}}">News</a></li>
            <li class="mbs-sidebar__item mbs-sidebar-dropdown">
              <span class="btn dropdown-toggle">Company</span>
              <ul class="mbs-sidebar-dropdown-menu">
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('company-support')}}">Support</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('company-support')}}">About Us</a></li>
                <li class="mbs-sidebar__item"><a class="btn" href="{{url('company-support')}}">Our Location</a></li>
              </ul>
            </li>
            <li class="mbs-sidebar__item"><a class="btn" href="http://mybos.com">Contact</a></li>
          </ul>
      </div>
  </section>

  @yield('content')

  <footer class="mbs-section mbs-section--relative mbs-section--fixed-size" id="footer-0" style="background-color: rgb(70, 73, 78);">

      <div class="mbs-section__container container">
          <div class="mbs-footer mbs-footer--wysiwyg row">
              <div class="col-md-4" style="text-align:center;">
                  <div class="" style="display:inline-block;">
                    <div class="footer-social-icon-div waves-effect">
                      <a href="" class="text-white" style="font-size:20px;"><i class="fa fa-envelope"></i></a>
                    </div>
                    <div class="footer-social-icon-div waves-effect">
                      <a href="https://www.facebook.com/mybosau/" class="text-white" style="font-size:20px;"><i class="fa fa-facebook"></i></a>
                    </div>
                    <div class="footer-social-icon-div waves-effect">
                      <a href="https://twitter.com/mybosfm" class="text-white" style="font-size:20px;"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
              </div>
              <div class="col-md-8">
                  <p class="mbs-footer__copyright text-white"><a class="mbs-footer__link text-white" href="http://mybos.com/">Privacy Policy</a>&nbsp;|&nbsp;<a class="mbs-footer__link text-white" href="http://mybos.com/">Terms of Conditions</a> | © 2017 MYBOS PTY LIMITED  </p>
              </div>
          </div>
      </div>
  </footer>

  <script src="{{ cdn('assets/web/assets/jquery/jquery.min.js') }}"></script>
  <script src="{{ cdn('assets/web/assets/jquery/jquery.hoverIntent.js') }}"></script>
  <script src="{{ cdn('js/modernizr.min.js') }}"></script>
  <script src="{{ cdn('node-waves/waves.js') }}"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="{{ cdn('assets/mybos/js/jquery.uploadPreview.min.js') }}"></script>
  <script src="{{ cdn('assets/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ cdn('assets/smooth-scroll/smooth-scroll.js') }}"></script>
  <script src="{{ cdn('assets/jarallax/jarallax.js') }}"></script>
  <script src="{{ cdn('js/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ cdn('assets/mybos/js/script.js') }}"></script>
  <script src="{{ cdn('assets/mybos/js/custom.js') }}"></script>
  @yield('scripts')

</body>
</html>
