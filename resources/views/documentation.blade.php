@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
  <style type="text/css">
    #image-preview {
      width: 170px;
      height: 130px;
      position: relative;
      overflow: hidden;
      background-color: #ffffff;
      color: #ecf0f1;
      display: inline-block;
    }
    #image-preview input {
      line-height: 200px;
      font-size: 200px;
      position: absolute;
      opacity: 0;
      z-index: 10;
    }
    #image-preview label {
      position: absolute;
      z-index: 5;
      opacity: 0.8;
      cursor: pointer;
      background-color: #ffffff;
      width: 150px;
      height: 50px;
      font-size: 20px;
      line-height: 50px;
      text-transform: uppercase;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      text-align: center;
      color: #707070;
    }
  </style>
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/resource_documentation_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">DOCUMENTATION</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#edf3f3;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>RESOURCES</label><label class="path-eclips"></label><label>DOCUMENTATION</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <p style="padding-left: 30px;padding-right: 30px;margin: 0;cursor:pointer;font-family:lato-bold;font-size:15px;"class="mbs-show-upload-form">+ Add Document</p>
          <div class="mbs-document-upload-form__container">
            <div class="mbs-header mbs-header--wysiwyg row">
              <form class="mbs-document-upload-form" id="document_upload" role="form" method="POST" enctype="multipart/form-data" action="{{ url('document_upload') }}" data-parsley-validate>
                {{ csrf_field() }}
                <input type="hidden" name="for_upload_posttype" id="for_upload_posttype" value="add">
                <input type="hidden" name="for_upload_document_id" id="for_upload_document_id" value="">
                <div class="col-md-3">
                  <div id="image-preview" style="background-repeat: no-repeat;background-size: cover;background-position: center center;">
                    <label for="image-upload" id="image-label">Choose File</label>
                    <input type="file" name="document_img" id="image-upload" />
                  </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-8">
                  <div>
                    <textarea class="mbs-document-upload-description-field" name="document_description" placeholder="Input description"></textarea>
                  </div>
                  <div>
                    <button class="btn btn-default" type="submit">Save</button>
                    <button class="btn btn-default" id="document-post-and-edit-cancel" type="button">cancel</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    @foreach($documents as $document)
    <div class="mbs-section__container container-fluid mbs-section__container--middle" style="padding-top: 30px;" id="document_{{$document->id}}">
      <div class="mbs-header mbs-header--wysiwyg row" style="margin-bottom: 50px;">
          <div class="col-sm-8 col-sm-offset-2">
              <div class="col-md-3 mbs-resource-documentation-imagediv">
                <img style="box-shadow: 3px 3px 5px 2px rgba(0,0,0,0.3);" style="" src="{{ cdn($document->image_path) }}" />
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-md-8" style="text-align:center;margin-top:25px;">
                <a class="mbs-document-delete" data-documentid="{{$document->id}}" data-type="cancel">Delete</a>
                <a class="mbs-document-edit" data-documentid="{{$document->id}}" data-documentimagepath="{{$document->image_path}}" data-documentdescription="{{$document->description}}">Edit</a>
                <p style="font-family:roboto-light;font-weight:bold;text-align:left;">{{$document->description}}</p>
                <p style="font-family:roboto-light;font-weight:bold;text-align:left;">{{$document->image_size}}</p>
                <p style="font-family:roboto-light;font-weight:bold;text-align:left;"><a style="color:#00aeef;text-decoration:none;cursor:pointer;"><i class="fa fa-download"></i>&nbsp;Download</a> or <a style="color:#00aeef;text-decoration:none;cursor:pointer;">request a hard copy</a></p>
              </div>
          </div>
      </div>
    </div>
    @endforeach
  </section>
@endsection
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $.uploadPreview({
        input_field: "#image-upload",
        preview_box: "#image-preview",
        label_field: "#image-label"
      });
    });
  </script>
@endsection
