@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/construction_development_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">DESTINE YOUR DEVELOPMENT FOR EXCELLENCE</h1>
                    <p class="mbs-software-backround__subtext" style="font-family: lato-regular;">EQUIP IT WITH THE SYSTEM YOUR BUILDING MANAGERS, OWNERS AND OCCUPIERS LOVE.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ebebeb;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>INDUSTRIES</label><label class="path-eclips"></label><label>CONSTRUCTION & DEVELOPMENT</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown" style="font-family:roboto-light;color:#363636;">FOR CONSTRUCTION & DEVELOPMENT COMPANIES</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-facility-section-div">
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-green" d="M91.5,83.5h-7V35.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H66.5c-1.65723,0-3,1.34326-3,3s1.34277,3,3,3h9.99219    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5H59.50781V18.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H23.50391    C19.09082,10.5,15.5,14.09033,15.5,18.50391V83.5h-7c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h83c1.65723,0,3-1.34277,3-3    S93.15723,83.5,91.5,83.5z M21.5,18.50391c0-1.10498,0.89941-2.00391,2.00391-2.00391H51.5    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5h-9.76654c0.0025-0.04218,0.01257-0.08221,0.01257-0.125v-11.75    c0-1.17365-0.95142-2.125-2.125-2.125h-8.25c-1.17365,0-2.125,0.95135-2.125,2.125v11.75c0,0.04279,0.01007,0.08282,0.01257,0.125    H21.5V18.50391z"/>
                    <path class="mbs-svg-green" d="M67.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S65.84277,43.5,67.5,43.5z"/>
                    <path class="mbs-svg-green" d="M67.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C64.5,52.15723,65.84277,53.5,67.5,53.5z"/>
                    <path class="mbs-svg-green" d="M67.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,63.5,67.5,63.5z"/>
                    <path class="mbs-svg-green" d="M67.5,73.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,73.5,67.5,73.5z"/>
                    <path class="mbs-svg-green" d="M42.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,43.5,42.5,43.5z"/>
                    <path class="mbs-svg-green" d="M27.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,43.5,27.5,43.5z"/>
                    <path class="mbs-svg-green" d="M42.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C39.5,52.15723,40.84277,53.5,42.5,53.5z"/>
                    <path class="mbs-svg-green" d="M27.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C24.5,52.15723,25.84277,53.5,27.5,53.5z"/>
                    <path class="mbs-svg-green" d="M42.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S40.84277,63.5,42.5,63.5z"/>
                    <path class="mbs-svg-green" d="M35.5,60.5c0-1.65723-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h5C34.15723,63.5,35.5,62.15723,35.5,60.5z    "/>
                    <path class="mbs-svg-green" d="M42.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,33.5,42.5,33.5z"/>
                    <path class="mbs-svg-green" d="M27.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,33.5,27.5,33.5z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Turnkey solution to manage your facilities right from commissioning.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 40 40" id="Слой_1" version="1.1" viewBox="0 0 40 40" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g>
                  <path class="mbs-svg-green" d="M37.9,28.6l-10-10c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l10,10c1.8,1.8,1.8,4.8,0,6.6c-0.9,0.9-2.1,1.4-3.3,1.4   s-2.4-0.5-3.3-1.4L17.4,24.2c3.5-1.5,6.1-4.5,7.2-8.2c0.4-1.4,0.5-2.8,0.4-4.2C24.6,5.6,19.4,0.4,13.2,0c-2.1-0.1-4.1,0.3-6,1.1   C7,1.3,6.8,1.5,6.7,1.9C6.6,2.2,6.7,2.5,7,2.7l7,7.1c1.2,1.2,1.2,3.1,0,4.3c-1.2,1.1-3.2,1.1-4.3,0l-7-7.1C2.4,6.8,2.1,6.7,1.8,6.8   C1.4,6.9,1.2,7.1,1,7.4C0.2,9.1-0.2,11-0.1,12.9C0,18.9,4.6,24.1,10.4,25c0.7,0.1,1.4,0.2,2.1,0.2c0.9,0,1.9-0.1,2.8-0.3L28.5,38   c1.3,1.3,2.9,2,4.7,2s3.5-0.7,4.7-2C40.5,35.4,40.5,31.2,37.9,28.6z M15.2,22.8c-1.4,0.4-3,0.5-4.5,0.2C5.8,22.2,2,17.9,1.9,12.9   c0-1.1,0.1-2.2,0.4-3.3l6,5.9c1,1,2.2,1.5,3.6,1.5c1.4,0,2.6-0.5,3.6-1.5c2-2,2-5.2,0-7.2L9.5,2.4C10.6,2.1,11.9,2,13.1,2   c5.2,0.3,9.6,4.6,9.9,9.9c0.1,1.2,0,2.4-0.4,3.5c-1,3.5-3.8,6.3-7.3,7.3C15.3,22.8,15.3,22.8,15.2,22.8z"/>
                </g>
                <g>
                  <path class="mbs-svg-green" d="M31.6,34.7c-1.7,0-3-1.3-3-3c0-1.7,1.3-3,3-3s3,1.3,3,3C34.6,33.3,33.3,34.7,31.6,34.7z M31.6,30.7c-0.6,0-1,0.4-1,1   c0,0.6,0.4,1,1,1s1-0.4,1-1C32.6,31.1,32.2,30.7,31.6,30.7z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Integrated maintenance ensuring your building always functions at 100%.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-green" d="M78,37.33517c1.65674,0,3-1.34326,3-3V22.28439c0-3.375-2.74609-6.12109-6.12158-6.12109h-7.28839v-0.00555    c0-1.66003-1.34003-3-3-3h-6.39001C57.96997,8.82773,54.39001,5.37772,50,5.37772s-7.96997,3.45001-8.20001,7.78003h-6.39001    c-1.65997,0-3,1.33997-3,3v0.00555h-7.28839C21.74609,16.1633,19,18.90939,19,22.28439v66.2168    c0,3.375,2.74609,6.12109,6.12158,6.12109h49.75684c3.37549,0,6.12158-2.74609,6.12158-6.12109V45.32834c0-1.65674-1.34326-3-3-3    s-3,1.34326-3,3v34.41797H25V22.28439c0-0.06689,0.05469-0.12109,0.12158-0.12109h49.75684    C74.94531,22.1633,75,22.2175,75,22.28439v12.05078C75,35.99191,76.34326,37.33517,78,37.33517z M75,85.74631v2.75488    c0,0.06641-0.05469,0.12109-0.12158,0.12109H25.12158C25.05469,88.62228,25,88.5676,25,88.50119v-2.75488H75z M47.84003,13.15775    c0.19995-1.02002,1.08997-1.78003,2.15997-1.78003s1.96002,0.76001,2.15997,1.78003    c0.04004,0.14001,0.05005,0.27997,0.05005,0.42999c0,1.21997-0.99005,2.21002-2.21002,2.21002s-2.21002-0.99005-2.21002-2.21002    C47.78998,13.43771,47.79999,13.29776,47.84003,13.15775z"/>
                    <path class="mbs-svg-green" d="M64.5918,31.80197H47.04688c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3H64.5918c1.65674,0,3-1.34326,3-3    S66.24854,31.80197,64.5918,31.80197z"/>
                    <path class="mbs-svg-green" d="M38.66022,32.56723c-1.16748-1.15674-3.25201-1.15674-4.41949,0c-0.14655,0.14606-0.27075,0.31299-0.38574,0.47943    c-0.11499,0.16693-0.20868,0.35474-0.29211,0.54199c-0.07227,0.18781-0.13538,0.38574-0.17712,0.58374    c-0.04169,0.198-0.06207,0.40662-0.06207,0.60461c0,0.20862,0.02039,0.41681,0.06207,0.61475    c0.04175,0.19849,0.10486,0.39648,0.17712,0.58374c0.08344,0.18781,0.17712,0.37561,0.29211,0.54199    c0.11499,0.16693,0.2392,0.33386,0.38574,0.47943c0.14557,0.13586,0.30231,0.27124,0.47943,0.38574    c0.16693,0.1145,0.34406,0.20868,0.5415,0.28143c0.18829,0.0835,0.38574,0.14606,0.58423,0.18781    c0.19745,0.04175,0.40613,0.05188,0.60461,0.05188c0.82343,0,1.63568-0.33331,2.20972-0.90686    c0.58325-0.58374,0.91705-1.39648,0.91705-2.21991C39.57727,33.96426,39.24347,33.15097,38.66022,32.56723z"/>
                    <path class="mbs-svg-green" d="M64.5918,42.22433H47.04688c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3H64.5918c1.65674,0,3-1.34326,3-3    S66.24854,42.22433,64.5918,42.22433z"/>
                    <path class="mbs-svg-green" d="M38.66022,42.98996c-1.16748-1.1568-3.25201-1.1568-4.41949,0c-0.14655,0.14606-0.27075,0.31299-0.38574,0.47943    c-0.11499,0.16687-0.20868,0.35468-0.29211,0.54199c-0.07227,0.18774-0.13538,0.38574-0.17712,0.58374    c-0.04169,0.19794-0.06207,0.40662-0.06207,0.60455c0,0.20868,0.02039,0.41681,0.06207,0.61481    c0.04175,0.19849,0.10486,0.39642,0.17712,0.58374c0.08344,0.18774,0.17712,0.37555,0.29211,0.54199    c0.11499,0.16693,0.2392,0.33386,0.38574,0.47937c0.14557,0.13593,0.30231,0.2713,0.47943,0.3858    c0.16693,0.1145,0.34406,0.20862,0.5415,0.28143c0.18829,0.08344,0.38574,0.14606,0.58423,0.18781    c0.19745,0.04169,0.40613,0.05188,0.60461,0.05188c0.82343,0,1.63568-0.33331,2.20972-0.90692    c0.58325-0.58368,0.91705-1.39648,0.91705-2.21991C39.57727,44.38693,39.24347,43.5737,38.66022,42.98996z"/>
                    <path class="mbs-svg-green" d="M64.5918,52.6467H47.04688c-1.65674,0-3,1.34277-3,3s1.34326,3,3,3H64.5918c1.65674,0,3-1.34277,3-3    S66.24854,52.6467,64.5918,52.6467z"/>
                    <path class="mbs-svg-green" d="M38.66022,53.41269c-1.16748-1.1568-3.25201-1.1568-4.41949,0c-0.14655,0.14606-0.27075,0.31299-0.38574,0.47937    c-0.11499,0.16693-0.20868,0.35474-0.29211,0.54199c-0.07227,0.18781-0.13538,0.3858-0.17712,0.58374    c-0.04169,0.198-0.06207,0.40662-0.06207,0.60461c0,0.20868,0.02039,0.41681,0.06207,0.61475    c0.04175,0.19849,0.10486,0.39648,0.17712,0.58374c0.08344,0.18781,0.17712,0.37561,0.29211,0.54199    c0.11499,0.16693,0.2392,0.33386,0.38574,0.47943c0.14557,0.13586,0.30231,0.27124,0.47943,0.38574    c0.16693,0.1145,0.34406,0.20868,0.5415,0.28143c0.18829,0.0835,0.38574,0.14606,0.58423,0.18781    c0.19745,0.04175,0.40613,0.05194,0.60461,0.05194c0.82343,0,1.63568-0.33337,2.20972-0.90692    c0.58325-0.58374,0.91705-1.39648,0.91705-2.21991C39.57727,54.80966,39.24347,53.99643,38.66022,53.41269z"/>
                    <path class="mbs-svg-green" d="M64.5918,63.06955H47.04688c-1.65674,0-3,1.34277-3,3s1.34326,3,3,3H64.5918c1.65674,0,3-1.34277,3-3    S66.24854,63.06955,64.5918,63.06955z"/>
                    <path class="mbs-svg-green" d="M34.24072,63.83536c-0.14655,0.14606-0.27075,0.31299-0.38574,0.47943c-0.11499,0.16693-0.20868,0.35468-0.29211,0.54199    c-0.07227,0.18781-0.13538,0.38574-0.17712,0.58374c-0.04169,0.19794-0.06207,0.40662-0.06207,0.60461    c0,0.20862,0.02039,0.41681,0.06207,0.61475c0.04175,0.19849,0.10486,0.39642,0.17712,0.58374    c0.08344,0.18781,0.17712,0.37555,0.29211,0.54199c0.11499,0.16693,0.2392,0.33386,0.38574,0.47943    c0.14557,0.13586,0.30231,0.27124,0.47943,0.38574c0.16693,0.1145,0.34406,0.20868,0.5415,0.28143    c0.18829,0.08344,0.38574,0.14606,0.58423,0.18781c0.19745,0.04169,0.40613,0.05188,0.60461,0.05188    c0.82343,0,1.63568-0.33331,2.20972-0.90686c0.58325-0.58374,0.91705-1.39648,0.91705-2.21991    c0-0.81281-0.3338-1.62604-0.91705-2.20978C37.49274,62.67862,35.4082,62.67862,34.24072,63.83536z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Never miss anything with inspection scheduling checklist.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_1">
                  <g>
                    <g>
                      <g>
                        <path class="mbs-svg-green" d="M83.015137,64.65332H74.30957c-1.656738,0-3-1.342773-3-3s1.343262-3,3-3h8.705566      C84.661133,58.65332,86,57.314453,86,55.667969V14.770508c0-1.646484-1.338867-2.985352-2.984863-2.985352H16.984863      C15.338867,11.785156,14,13.124023,14,14.770508v40.897461c0,1.646484,1.338867,2.985352,2.984863,2.985352h42.226563      c1.656738,0,3,1.342773,3,3s-1.343262,3-3,3H16.984863C12.030762,64.65332,8,60.62207,8,55.667969V14.770508      c0-4.954102,4.030762-8.985352,8.984863-8.985352h66.030273C87.969238,5.785156,92,9.816406,92,14.770508v40.897461      C92,60.62207,87.969238,64.65332,83.015137,64.65332z"/>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="mbs-svg-green" d="M50.347168,94.214844c-0.335938,0-0.674805-0.056641-1.003418-0.172852      c-1.196777-0.424805-1.996094-1.557617-1.996094-2.827148V71.62793c0-1.657227,1.343262-3,3-3s3,1.342773,3,3v11.140625      l18.702148-23.007813c1.04541-1.285156,2.935059-1.480469,4.220215-0.435547      c1.286133,1.044922,1.480957,2.93457,0.436035,4.220703L52.675781,93.107422      C52.094727,93.821289,51.23291,94.214844,50.347168,94.214844z"/>
                      </g>
                    </g>
                  </g>
                  <g>
                    <g>
                      <g>
                        <path class="mbs-svg-green" d="M30,44.195313c-2.404297,0-4.664063-0.936523-6.364258-2.636719C21.936035,39.859375,21,37.599609,21,35.195313      s0.936035-4.664063,2.635742-6.364258c3.509766-3.507813,9.219238-3.509766,12.728027,0h0.000488      C38.063965,30.53125,39,32.791016,39,35.195313s-0.936035,4.664063-2.63623,6.363281      C34.664063,43.258789,32.404297,44.195313,30,44.195313z M30,32.196289c-0.768555,0-1.536621,0.292969-2.121582,0.876953      C27.312012,33.640625,27,34.393555,27,35.195313c0,0.800781,0.312012,1.554688,0.878418,2.121094      c1.133789,1.132813,3.109863,1.132813,4.242676,0C32.687988,36.75,33,35.996094,33,35.195313      c0-0.801758-0.312012-1.554688-0.878906-2.12207C31.536133,32.488281,30.768066,32.196289,30,32.196289z"/>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="mbs-svg-green" d="M50,44.195313c-2.404297,0-4.664063-0.936523-6.364258-2.636719C41.936035,39.859375,41,37.599609,41,35.195313      s0.936035-4.664063,2.635742-6.364258c3.509766-3.507813,9.219238-3.509766,12.728027,0h0.000488      C58.063965,30.53125,59,32.791016,59,35.195313s-0.936035,4.664063-2.63623,6.363281      C54.664063,43.258789,52.404297,44.195313,50,44.195313z M50,32.196289c-0.768555,0-1.536621,0.292969-2.121582,0.876953      C47.312012,33.640625,47,34.393555,47,35.195313c0,0.800781,0.312012,1.554688,0.878418,2.121094      c1.133789,1.132813,3.109863,1.132813,4.242676,0C52.687988,36.75,53,35.996094,53,35.195313      c0-0.801758-0.312012-1.554688-0.878906-2.12207C51.536133,32.488281,50.768066,32.196289,50,32.196289z"/>
                      </g>
                    </g>
                    <g>
                      <g>
                        <path class="mbs-svg-green" d="M70,44.195313c-2.404297,0-4.664063-0.936523-6.364258-2.636719C61.936035,39.859375,61,37.599609,61,35.195313      s0.936035-4.664063,2.635742-6.364258c3.509766-3.507813,9.219238-3.509766,12.728027,0h0.000488      C78.063965,30.53125,79,32.791016,79,35.195313s-0.936035,4.664063-2.63623,6.363281      C74.664063,43.258789,72.404297,44.195313,70,44.195313z M70,32.196289c-0.768555,0-1.536621,0.292969-2.121582,0.876953      C67.312012,33.640625,67,34.393555,67,35.195313c0,0.800781,0.312012,1.554688,0.878418,2.121094      c1.133789,1.132813,3.109863,1.132813,4.242676,0C72.687988,36.75,73,35.996094,73,35.195313      c0-0.801758-0.312012-1.554688-0.878906-2.12207C71.536133,32.488281,70.768066,32.196289,70,32.196289z"/>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Build communities - connect your residents and management with SMS, in-app and email notifications.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 512 512" id="Layer_12" version="1.1" viewBox="0 0 512 512" xml:space="preserve">
                <path class="mbs-svg-green" clip-rule="evenodd" d="M512,303.844c0,44.18-35.82,80-80,80h-32c-8.844,0-16-7.156-16-16  s7.156-16,16-16h32c26.516,0,48-21.492,48-48s-21.484-48-48-48h-0.25c-20.812,0-38.539-13.25-45.188-31.766  c-0.047-0.117-0.156-0.234-0.195-0.352c-12.492-35.078-44.727-60.758-83.398-63.531c-8.359,0.539-14.969,7.406-14.969,15.906  v191.742c0,8.844-7.156,16-16,16s-16-7.156-16-16V176.102c0-26.32,21.156-47.617,47.398-47.945  c52.242,2.984,95.961,37.297,112.891,84.438c0.078,0.078,0.234,0.156,0.258,0.234c2.094,6.398,8.102,11.016,15.203,11.016H432  c0.891,0,1.766,0.102,2.656,0.141C434.812,224,435.047,224,435.18,224C477.891,225.688,512,260.734,512,303.844L512,303.844z   M208,383.844c-8.844,0-16-7.156-16-16v-176c0-8.844,7.156-16,16-16s16,7.156,16,16v176C224,376.688,216.844,383.844,208,383.844  L208,383.844z M144,383.844c-8.844,0-16-7.156-16-16V183.836c0-8.828,7.156-16,16-16s16,7.172,16,16v184.008  C160,376.688,152.844,383.844,144,383.844L144,383.844z M80,383.844c-8.844,0-16-7.156-16-16v-128c0-8.844,7.156-16,16-16  s16,7.156,16,16v128C96,376.688,88.844,383.844,80,383.844L80,383.844z M16,351.844c-8.844,0-16-7.156-16-16v-64  c0-8.844,7.156-16,16-16s16,7.156,16,16v64C32,344.688,24.844,351.844,16,351.844L16,351.844z M336,351.844c8.844,0,16,7.156,16,16  s-7.156,16-16,16s-16-7.156-16-16S327.156,351.844,336,351.844L336,351.844z"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Monitor critical equipment and assets with notifications through integrated wireless smart sensors.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" >
                <g id="Layer_1">
                  <path class="mbs-svg-green" d="M92.496521,33.06134c-0.000427-0.02655-0.00293-0.052429-0.004028-0.078796   c-0.070557-8.20459-6.866211-14.859497-15.217102-14.859497H63.842651c0.002747-0.052856,0.015747-0.102722,0.015747-0.15625   V16.70459c0-4.960449-4.035156-8.996094-8.996094-8.996094h-9.78418c-4.960938,0-8.996094,4.035645-8.996094,8.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H22.666016c-8.307922,0-15.069214,6.637817-15.15686,14.82959   c-0.002258,0.042786-0.006226,0.084656-0.006592,0.127686c0,0.01062-0.001587,0.020874-0.001587,0.031494l0.029297,41.301758   c0,3.710327,2.461121,6.861511,5.860901,7.975281c0.330811,5.591125,6.010376,9.902649,13.222107,9.902649h46.713867   c7.213135,0,12.893494-4.313232,13.22229-9.905884c3.383484-1.116211,5.831421-4.263367,5.831421-7.963257l0.118164-41.3125   C92.499023,33.093445,92.496582,33.077698,92.496521,33.06134z M42.082031,17.966797V16.70459   c0-1.651855,1.34375-2.996094,2.996094-2.996094h9.78418c1.652344,0,2.996094,1.344238,2.996094,2.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H42.066284C42.069031,18.07019,42.082031,18.020325,42.082031,17.966797z    M22.666016,24.123047h54.609375c4.915283,0,9.070679,3.978882,9.210449,8.733398   c-1.340881,7.173462-4.711609,13.725525-9.775879,18.958008c-1.151367,1.190918-1.120117,3.090332,0.070313,4.242188   c0.582031,0.563965,1.333984,0.844238,2.085938,0.844238c0.78418,0,1.567383-0.305664,2.15625-0.913574   c2.057556-2.126526,3.869507-4.443542,5.430847-6.90918l-0.072449,25.335449c0,1.306641-1.158203,2.410156-2.529297,2.410156   H60.384766H16.088867c-1.363281,0-2.558594-1.126465-2.558594-2.412109l-0.018005-25.390076   c7.704041,12.219727,21.28595,20.121033,36.486755,20.121033c6.727539,0,13.444336-1.594238,19.424805-4.609863   c1.479492-0.746094,2.074219-2.550293,1.328125-4.029297c-0.746094-1.47998-2.551758-2.07373-4.029297-1.328125   c-2.703491,1.363281-5.584839,2.37915-8.54895,3.045837c0.201599-0.726685,0.317749-1.488953,0.317749-2.279846   c0-4.706116-3.815063-8.521118-8.521118-8.521118s-8.521057,3.815002-8.521057,8.521118   c0,0.79248,0.116516,1.556335,0.318909,2.284302C27.581909,59.01532,16.293762,47.665283,13.512939,32.878418   C13.639771,28.030396,17.691956,24.123047,22.666016,24.123047z M49.970337,62.781616   c-1.568665,0-2.840332-1.271667-2.840332-2.840332c0-1.568726,1.271667-2.840393,2.840332-2.840393   c1.568726,0,2.840393,1.271667,2.840393,2.840393C52.81073,61.509949,51.539063,62.781616,49.970337,62.781616z    M73.327148,86.291504H26.613281c-3.633606,0-6.323853-1.681396-7.051086-3.467773h60.81604   C79.651001,84.610107,76.960754,86.291504,73.327148,86.291504z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Manage and rectify defects with the advanced case management module.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-insight-section-div">
          <p class="mbs-insight-section-text insight-section-text-big">User Insight</p>
          <p class="mbs-insight-section-text insight-section-text-small">“User-friendly and straight to the point - saves a lot of time and energy and gets the job done easily!”  </p>
          <p class="mbs-insight-section-text insight-section-text-big" style="margin:0;">Jack Maalouf</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;margin:0;">Dyldam Developments</p>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-2" style="background-color: #ffffff;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_construction_top.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_construction_top_mobile.jpg') }}" />
            </div>
        </div>
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_construction_under.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_construction_under_mobile.jpg') }}" />
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-product-overview-undertext">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-product-overview-undertext-text">FOR A FULL LIST OF FEATURES <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CONTACT US</a> TODAY</h3>
                <h3 class="mbs-product-overview-undertext-text">OR <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CLICK HERE</a> TO DOWNLOAD OUR BROCHURES</h3>
            </div>
        </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-3" style="background-color: #ebebeb;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Additional Pluses</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/Rectangle_imac-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div"  style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/cloud_product-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div">
                <p style="margin:0;font-size: 18px;">Cloud-based system guarantees regular back-up, maximum service uptime and no additional hardware cost to the subscriber</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/productpage-mybos-svgimg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div" style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="mbs-section__container container" style="padding-bottom:100px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse" data-animation="zoomIn"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Request A Demo</a></div>
            </div>
        </div>
    </div>
  </section>
@endsection
