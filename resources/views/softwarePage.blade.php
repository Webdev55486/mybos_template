@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/cloud-path-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/software_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text">IT’S ALL ABOUT FACILITY MANAGEMENT</h1>
                    <p class="mbs-software-backround__subtext">YOUR ONE STOP SOLUTION FOR ALL YOUR FACILITY MANAGEMENT NEEDS</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#eaecee;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>PRODUCTS</label><label class="path-eclips"></label><label>FACILITY MANAGEMENT SOFTWARE</label>
    </div>
    <div class="mbs-section__container container">
        <div class="row mbs-section-imac-div">
          <div class="mbs-imac-img mbs-software-imac-with-img1" data-animation="rubberBand" data-timeout="500" style="background-image: url({{ cdn('assets/images/imac_with_img_1.png') }});">
            <div class="imac-logo-div facility-logo" data-animation="zoomIn" data-timeout="800"><div class="imac-logo-subdiv"><p>FACILITY MANAGEMENT PORTAL</p></div></div>
          </div>
          <div class="mbs-imac-img mbs-software-imac-with-img2" data-animation="rubberBand" data-timeout="500" style="background-image: url({{ cdn('assets/images/imac_with_img_2.png') }});">
            <div class="imac-logo-div resident-logo" data-animation="zoomIn" data-timeout="800"><div class="imac-logo-subdiv"><p>RESIDENT & COMMUNITY PORTAL</p></div></div>
          </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="revealOnScroll mbs-brand-header__text" data-animation="zoomIn">Cloud-based designed to make life easier for Facilities Managers, Strata Managers, Developers & Building Occupants.</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
        <div class="row">
            <div class="mbs-article mbs-article--wysiwyg col-sm-8 col-sm-offset-2">
              <p class="revealOnScroll mbs-software-imac-subtext" data-animation="zoomIn" data-timeout="200">
                The MYBOS web-based solution is fully scalable and customised to meet the needs of any sized community.
              </p>
              <p class="revealOnScroll mbs-software-imac-subtext" data-animation="zoomIn" data-timeout="200">
                The software combines a simple to use and durable mobile working solution with a remodelled high performance back-end system optimised to provide a convenient package covering the full spectrum of tasks the building and facilities managers, concierge staff and supervisors need to successfully manage the day to day requirements of a residential or commercial building complex.
              </p>
              <p class="revealOnScroll mbs-software-imac-subtext" data-animation="zoomIn" data-timeout="200">
                In addition to reducing cost and time of traditional facilities management operations, MYBOS provides tenants with a private, amenity-rich web portal that includes engaging community features and continuous updated information about their property, community and neighbourhood.
              </p>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle-1">
      <div class="mbs-cloud-div__container-form">
        <div class="row mbs-software-cloud-form-cloud-row">
          <div class="col-sm-4 col-sm-offset-4 mbs-software-cloud-form-cloud">
            <h3 class="revealOnScroll mbs-software-cloud-form-cloud__text" data-animation="zoomIn">MYBOS CLOUDPLATFORM</h3>
            <div class="revealOnScroll mbs-software-cloud-form-cloud__svg" data-animation="zoomIn">
              <svg enable-background="new 0 0 512 512" class="cloud-svg-color" id="cloud" version="1.1" viewBox="0 0 512 512" xml:space="preserve">
                <path d="M285.928,113.067c62.492,0,113.327,50.827,113.327,113.327c0,0.344-0.041,0.705-0.066,1.049  c-0.049,0.836-0.107,1.672-0.123,2.525l-0.426,17.133l17.159,0.065c41.53,0.115,75.313,34.005,75.313,75.535  c0,41.415-33.718,75.305-75.157,75.518l-3.664,0.016H104.977c-46.244-0.049-83.872-37.693-83.872-83.929  c0-35.825,22.806-67.714,56.737-79.356l9.444-3.229l1.664-9.838c4.115-24.282,24.97-41.907,49.588-41.907  c7.846,0,15.428,1.82,22.536,5.394l15.306,7.689l7.386-15.444C202.531,138.398,242.635,113.067,285.928,113.067 M285.928,96.277  c-51.778,0-96.358,30.315-117.303,74.092c-9.059-4.558-19.256-7.182-30.086-7.182c-33.233,0-60.762,24.184-66.14,55.893  C32.82,232.657,4.316,270.104,4.316,314.307c0,55.597,45.063,100.669,100.644,100.718h311.084v-0.016  c50.777-0.263,91.856-41.481,91.856-92.308c0-50.909-41.177-92.177-92.053-92.324c0.033-1.344,0.197-2.639,0.197-3.984  C416.044,154.532,357.79,96.277,285.928,96.277L285.928,96.277z"/>
              </svg>
            </div>
            <div class="mbs-software-cloud-form-path-div">
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-center-1" data-animation="zoomIn" data-timeout="100"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-center-2" data-animation="zoomIn" data-timeout="200"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-center-3" data-animation="zoomIn" data-timeout="300"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-center-4" data-animation="zoomIn" data-timeout="400"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-1" data-animation="zoomIn" data-timeout="500"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-2" data-animation="zoomIn" data-timeout="600"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-3" data-animation="zoomIn" data-timeout="700"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-4" data-animation="zoomIn" data-timeout="800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-5" data-animation="zoomIn" data-timeout="900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-6" data-animation="zoomIn" data-timeout="1000"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-7" data-animation="zoomIn" data-timeout="1100"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-8" data-animation="zoomIn" data-timeout="1200"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-9" data-animation="zoomIn" data-timeout="1300"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-10" data-animation="zoomIn" data-timeout="1400"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-11" data-animation="zoomIn" data-timeout="1500"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-12" data-animation="zoomIn" data-timeout="1600"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-13" data-animation="zoomIn" data-timeout="1700"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-1" data-animation="zoomIn" data-timeout="500"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-2" data-animation="zoomIn" data-timeout="600"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-3" data-animation="zoomIn" data-timeout="700"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-4" data-animation="zoomIn" data-timeout="800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-5" data-animation="zoomIn" data-timeout="900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-6" data-animation="zoomIn" data-timeout="1000"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-7" data-animation="zoomIn" data-timeout="1100"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-8" data-animation="zoomIn" data-timeout="1200"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-9" data-animation="zoomIn" data-timeout="1300"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-10" data-animation="zoomIn" data-timeout="1400"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-11" data-animation="zoomIn" data-timeout="1500"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-12" data-animation="zoomIn" data-timeout="1600"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-13" data-animation="zoomIn" data-timeout="1700"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-1-center-1" data-animation="zoomIn" data-timeout="1800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-1-center-2" data-animation="zoomIn" data-timeout="1900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-1-center-3" data-animation="zoomIn" data-timeout="2000"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-2-center-1" data-animation="zoomIn" data-timeout="1800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-2-center-2" data-animation="zoomIn" data-timeout="1900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-left-2-center-3" data-animation="zoomIn" data-timeout="2000"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-1-center-1" data-animation="zoomIn" data-timeout="1800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-1-center-2" data-animation="zoomIn" data-timeout="1900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-1-center-3" data-animation="zoomIn" data-timeout="2000"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-2-center-1" data-animation="zoomIn" data-timeout="1800"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-2-center-2" data-animation="zoomIn" data-timeout="1900"></label>
              <label class="revealOnScroll cloudpath-eclips cloudpath-eclips-right-2-center-3" data-animation="zoomIn" data-timeout="2000"></label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-3 mbs-software-cloud-form-subdiv">
            <div class="revealOnScroll mbs-software-cloud-form-webportal__svg" data-animation="zoomIn" data-timeout="100">
              <svg version="1.1" viewBox="0 0 60 60" id="imac-cloud" class="cloud-svg-color">
                <path d="M39,54 L36,54 L36,49 C36,48.447 35.553,48 35,48 C34.447,48 34,48.447 34,49 L34,55 C34,55.553 34.447,56 35,56 L39,56 C40.122,56 41.295,56.914 41.774,58 L18.225,58 C18.705,56.914 19.877,56 21,56 L31,56 C31.553,56 32,55.553 32,55 C32,54.447 31.553,54 31,54 L26,54 L26,49 C26,48.447 25.552,48 25,48 C24.448,48 24,48.447 24,49 L24,54 L21,54 C18.43,54 16,56.43 16,59 C16,59.553 16.448,60 17,60 L43,60 C43.553,60 44,59.553 44,59 C44,56.43 41.57,54 39,54 M28,40 C28,41.103 28.897,42 30,42 C31.103,42 32,41.103 32,40 C32,38.897 31.103,38 30,38 C28.897,38 28,38.897 28,40 M60,5 L60,41 C60,43.804 57.804,46 55,46 L5,46 C2.196,46 0,43.804 0,41 L0,35 C0,34.447 0.448,34 1,34 L55,34 C55.553,34 56,34.447 56,35 C56,35.553 55.553,36 55,36 L2,36 L2,41 C2,42.71 3.29,44 5,44 L55,44 C56.71,44 58,42.71 58,41 L58,5 C58,3.346 56.654,2 55,2 L5,2 C3.252,2 2.101,3.56 1.998,5.068 L2,31 C2,31.553 1.552,32 1,32 C0.448,32 0,31.553 0,31 L0,5 C0.191,2.166 2.386,0 5,0 L55,0 C57.757,0 60,2.243 60,5"/>
              </svg>
            </div>
            <h3 class="revealOnScroll mbs-software-cloud-form__subtext" data-animation="zoomIn" data-timeout="100">FM & COMMUNITY WEB PORTAL</h3>
          </div>
          <div class="col-sm-3 mbs-software-cloud-form-subdiv">
            <div class="revealOnScroll mbs-software-cloud-form-mobile__svg" data-animation="zoomIn" data-timeout="100">
              <svg id="cloud-mobile" style="enable-background:new 0 0 512 512;" class="cloud-svg-color" version="1.1" viewBox="0 0 512 512" xml:space="preserve">
                  <path class="st0" d="M117.2,91c0-11.7,0-23.3,0-35c0-23,13.4-40.2,34.6-44.5c3.6-0.7,7.3-0.8,10.9-0.8c62.3,0,124.7-0.1,187,0   c26.8,0,45,18,45,44.8c0,87.2,0,174.3,0,261.5"/>
                  <path class="st0" d="M394.8,337c0,0.3,0,0,0,0.3"/>
                  <path class="st0" d="M394.8,356.3c0,33.4,0,66.8,0,100.1c0,26.7-18.2,44.7-45,44.8c-62.5,0-125,0-187.5,0c-27.1,0-45-18.1-45-45.2   c0-66.7,0-133.3,0-200c0-40,0-99,0-139"/>
                  <path class="st1" d="M256.1,426.8c11.9,0.1,21.1,9.4,21.1,21.4c-0.1,11.9-9.5,21.1-21.4,21.1c-11.9-0.1-21.1-9.4-21.1-21.4   C234.8,435.9,244.2,426.7,256.1,426.8z"/>
                  <path d="M286.7,58.1h-61.5c-2.8,0-5.1-2.3-5.1-5.1v0c0-2.8,2.3-5.1,5.1-5.1h61.5c2.8,0,5.1,2.3,5.1,5.1v0   C291.8,55.8,289.5,58.1,286.7,58.1z"/>
                </svg>
            </div>
            <h3 class="revealOnScroll mbs-software-cloud-form__subtext" data-animation="zoomIn" data-timeout="100">MOBILE MAINTENANCE</h3>
          </div>
          <div class="col-sm-3 mbs-software-cloud-form-subdiv">
            <div class="revealOnScroll mbs-software-cloud-form-lobby__svg" data-animation="zoomIn" data-timeout="100">
              <svg version="1.1" id="Layer_1" x="0px" y="0px" class="cloud-svg-color" viewBox="0 0 116.5 90.3" xml:space="preserve">
          			<path d="M110,67.6H6c-3.3,0-6-2.7-6-6v-54c0-3.3,2.7-6,6-6h104c3.3,0,6,2.7,6,6v54C116,64.9,113.3,67.6,110,67.6z
          				 M6,5.6c-1.1,0-2,0.9-2,2v54c0,1.1,0.9,2,2,2h104c1.1,0,2-0.9,2-2v-54c0-1.1-0.9-2-2-2H6z"/>
          			<path d="M72,90.3H44c-1.1,0-2-0.9-2-2s0.9-2,2-2h28c1.1,0,2,0.9,2,2S73.1,90.3,72,90.3z"/>
          			<polygon points="64,88.6 60,88.6 60,67.6 56,67.6 56,88.6 52,88.6 52,63.6 64,63.6 			"/>
            		<path id="one-finger" d="M62.5,55.8c6,0,10.9-5.2,10.9-10.9c0,0,0,3.4,0,0v-5.5v-3.3c0-1.2-1-2.2-2.2-2.2
            			c-1.2,0-2.2,1-2.2,2.2v0.7h-1.5v-3.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2v2.2h-1.5v-3.6c0-1.2-1-2.2-2.2-2.2
            			c-1.2,0-2.2,1-2.2,2.2v5.1H56V21.6c0-1.2-1-2.2-2.2-2.2c-1.2,0-2.2,1-2.2,2.2v16.4c-3-3.2-6.9-6.7-8.5-5.1
            			c-1.6,1.6,2.5,6,8.2,15.5C53.8,52.8,57.1,55.8,62.5,55.8L62.5,55.8L62.5,55.8z M74.9,44.9c0,6.8-5.5,12.4-12.4,12.4
            			c-4.5,0-9.3-2.1-12.6-8.1c-4.7-8.6-10.8-14.3-7.9-17.2c2.1-2.1,5.3-0.1,8.2,2.6l0,0V21.6c0-2,1.6-3.6,3.6-3.6c2,0,3.6,1.6,3.6,3.6
            			v7.3c0.6-0.5,1.4-0.7,2.2-0.7c1.6,0,2.9,1,3.4,2.4c0.6-0.6,1.5-0.9,2.4-0.9c2,0,3.6,1.6,3.6,3.6v0c0.6-0.5,1.4-0.7,2.2-0.7
            			c2,0,3.6,1.6,3.6,3.6V44.9L74.9,44.9z"/>
              </svg>
            </div>
            <h3 class="revealOnScroll mbs-software-cloud-form__subtext" data-animation="zoomIn" data-timeout="100">LOBBY TOUCH KIOSK</h3>
          </div>
          <div class="col-sm-3 mbs-software-cloud-form-subdiv">
            <div class="revealOnScroll mbs-software-cloud-form-display__svg" data-animation="zoomIn" data-timeout="100">
              <svg version="1.1" id="Layer_1" class="cloud-svg-color" x="0px" y="0px" viewBox="0 0 113 85" xml:space="preserve">
                <style type="text/css">
                	.st0{fill:none;enable-background:new    ;}
                	.st1{fill:#636363;}
                </style>
                <image style="overflow:visible;" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGMAAAA5CAQAAACIJ12PAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
                AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZ
                cwAACxIAAAsSAdLdfvwAAAAHdElNRQfhCAsOKCsNwtSnAAABOklEQVRo3u3avUrDUByG8SchIMUO
                gk7uUpdal9BBF2e9i6N7dmd39+QySvdCnVz86GCR3oGFUnBwqkNOB8ekgfOPvL/pENL2fVq6JXJ4
                fU7Zp02++eC9ACByACk5g9CrannlrniGGEiZtDQCBkxu0zIjpxN6zQ465BC5Pm8AjMhYht5UySGP
                3ABwltDzFzMWoXdVtCLzGb2Yrr/Yrl/i7+ZuHHpJM5RhiTIsUYYlyrBEGZYowxJlWKIMS5RhiTIs
                UYYlyrBEGZYowxJlWKIMS5RhiTIsUYYlyrBEGZb8m4y1Px2HnlLDdvM6YeaPOfesarzVS2Ojzive
                f8CDP80iB1MudvjwqLGMTc3XPRWXMeBa+SzV1hJX/sXnDBnX/i5C2jBmWMwhAWDBNUecsBd01FXF
                +3/45Kt8DvcXu78vV3Kx8kAAAAAASUVORK5CYII=" transform="matrix(1 0 0 1 7 0)">
                </image>
              	<path d="M69.3,58.2c-2.2,5.1-7.1,9.3-11.8,9.3c-5.7,0-9.9-4.2-12.1-9.3c-12.9,6.7-11.3,23.7-11.3,23.7c0,2.3,1.8,2.7,4,2.7h19.5H77
              		c2.2,0,4-0.3,4-2.7C81,81.9,82.3,64.9,69.3,58.2z M77,80.9H57.5H38.1c-0.1,0-0.2,0-0.4,0c-0.1-2.6,0-11.9,6.4-17.4
              		c3.3,4.9,8.1,7.7,13.4,7.7c4.8,0,9.7-3.1,13-7.8c6.4,5.4,6.8,14.8,6.8,17.4C77.2,80.9,77.1,80.9,77,80.9z"/>
              	<path d="M57.5,64.7c6.5,0,11.6-9.3,11.6-16.9c0-7.7-5.3-13.9-11.7-13.9c-6.5,0-11.7,6.2-11.7,13.9C45.7,55.5,51,64.7,57.5,64.7z
              		 M57.4,37.6c4.4,0,8.1,4.6,8.1,10.2c0,6.3-4.2,13.3-8,13.3c-3.8,0-8.2-7.1-8.2-13.3C49.4,42.2,53,37.6,57.4,37.6z"/>
              </svg>
            </div>
            <h3 class="revealOnScroll mbs-software-cloud-form__subtext" data-animation="zoomIn" data-timeout="100">PUBLIC DISPLAY BOARD</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container mbs-section__container--middle-2">
      <div class="row" style="padding-bottom:40px;">
        <div class="col-sm-8 col-sm-offset-2">
            <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Facility Management Portal Features</h3>
        </div>
      </div>
      <div class="row mbs-software-facility-button-div">
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-microphone"></i><label>BroadCast</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-briefcase"></i><label>Cases</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-send"></i><label>BroadCast</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-calendar"></i><label>Calendar</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-th-large"></i><label>Modules</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="material-icons" style="font-size:33px;">person</i><label>Resident Database</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-key"></i><label>Key Management</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-gift"></i><label>Parcel Management</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-database"></i><label>Contractor Database</label></a>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container mbs-section__container--middle-2">
      <div class="row" style="padding-bottom:40px;">
        <div class="col-sm-8 col-sm-offset-2">
            <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Facility Management Portal Features</h3>
        </div>
      </div>
      <div class="row mbs-software-facility-button-div">
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-microphone"></i><label>BroadCast</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-shopping-cart"></i><label>Market Place</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="material-icons" style="font-size33px;">contact_mail</i><label>Update My Info</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-wrench"></i><label>Maintenance Request</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-book"></i><label>Bookings</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-file-text"></i><label>Document Library</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-map-marker" style="transform: rotate(-10deg);"></i><label>Directory</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="material-icons" style="font-size:33px;">room_service</i><label>Inhouse Service</label></a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4" style="margin-bottom:20px;">
          <div class="mbs-software-facility-management-button-div">
            <a class="mbs-software-buttons__btn btn waves-effect" data-animation="fadeInUp"><i class="fa fa-credit-card-alt"></i><label>Certified Contractors</label></a>
          </div>
        </div>
      </div>
      <div class="row mbs-software-facility-button-div">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="mbs-buttons mbs-buttons--center btn-inverse"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Contact Us To Learn More</a></div>
        </div>
      </div>
  </section>

  <section class="mbs-section-service mbs-section--relative mbs-section--fixed-size" id="section-3" style="background-color:#ffffff;">
    <div class="mbs-section__container container-fluid mbs-section-service__container" style="padding-top:0!important;">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-md-3">
              <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
                <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                  <g id="Layer_1">
                    <path d="M50,6.4668C25.99609,6.4668,6.4668,25.99609,6.4668,50c0,11.76154,4.69647,22.44122,12.30286,30.2832   c0.1272,0.16779,0.27582,0.32098,0.43982,0.4613C27.09412,88.64087,37.98633,93.5332,50,93.5332   c11.73438,0,22.7373-4.59863,30.98047-12.9502c0.06873-0.06958,0.11963-0.14911,0.18018-0.22345   c0.03253-0.03998,0.06622-0.07825,0.0965-0.11963c0.08905-0.12146,0.1651-0.24762,0.23352-0.3783   c0.02014-0.03839,0.04114-0.07581,0.05957-0.11499c0.06439-0.13702,0.11578-0.27661,0.1582-0.41998   c0.01233-0.04138,0.02533-0.08191,0.03589-0.12378c0.03522-0.14075,0.05823-0.2821,0.07269-0.42578   c0.00537-0.05139,0.01105-0.10205,0.01373-0.15375c0.00665-0.1355,0.0022-0.26965-0.0094-0.40485   c-0.00513-0.06122-0.0097-0.12158-0.01868-0.18268c-0.0191-0.12921-0.04919-0.25537-0.08521-0.38159   c-0.01807-0.06372-0.03503-0.12689-0.05762-0.18988c-0.04376-0.12213-0.09894-0.23932-0.15906-0.35602   c-0.02197-0.04303-0.03412-0.08844-0.05841-0.1308c-0.02197-0.03833-0.04944-0.07446-0.07172-0.11267   c-0.01831-0.02869-0.03522-0.0575-0.05457-0.08569c-2.0614-3.48193-5.51935-6.42334-10.29266-8.73621   c-5.69238-2.77295-9.54645-5.63574-9.83514-10.42505c4.17902-5.29523,6.77557-12.77283,6.77557-18.46851   c0-9.94727-8.09277-18.04004-18.04004-18.04004c-9.94824,0-18.04102,8.09277-18.04102,18.04004   c0,5.76123,2.65594,13.34668,6.92004,18.65143c-0.37408,4.68073-4.20563,7.50409-9.82043,10.2392   c-3.3233,1.61035-5.99536,3.52844-8.01819,5.71423C15.65729,67.27917,12.4668,59.00592,12.4668,50   c0-20.69629,16.83691-37.5332,37.5332-37.5332S87.5332,29.30371,87.5332,50c0,6.1123-1.4209,11.93945-4.22363,17.31738   c-0.76563,1.46973-0.19531,3.28125,1.27344,4.04688c1.47168,0.76758,3.28223,0.19531,4.04688-1.27344   C91.83789,63.93555,93.5332,56.98828,93.5332,50C93.5332,25.99609,74.00391,6.4668,50,6.4668z M49.92383,27.10938   c6.63867,0,12.04004,5.40137,12.04004,12.04004c0,6.98828-5.94922,19.02734-12.04004,19.02734S37.88281,46.1377,37.88281,39.14941   C37.88281,32.51074,43.28418,27.10938,49.92383,27.10938z M31.60352,73.4375c3.43475-1.67285,9.8241-4.80096,12.25458-11.06689   c1.88007,1.13721,3.9162,1.80615,6.06573,1.80615c2.19568,0,4.27399-0.6955,6.1875-1.87805   c2.40881,6.31525,8.83813,9.45996,12.29102,11.14172c2.7926,1.35284,4.96094,2.91089,6.48212,4.64722   C68.0163,84.19153,59.27618,87.5332,50,87.5332c-9.53485,0-18.24512-3.57971-24.87268-9.45795   C26.64825,76.34375,28.81323,74.78925,31.60352,73.4375z"/>
                  </g>
                </svg>
              </div>
              <div class="mbs-service-div">
                <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Occupants, Owners and Managing Agents</h3>
                <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                  Benefit from increased rental and asset value through a better maintained building, assets and facilities.
                </p>
                <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-manage-agents')}}">Read more</a></label>
              </div>
            </div>
            <div class="col-md-3">
              <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
                <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                  <g id="Layer_1">
                    <path d="M92.496521,33.06134c-0.000427-0.02655-0.00293-0.052429-0.004028-0.078796   c-0.070557-8.20459-6.866211-14.859497-15.217102-14.859497H63.842651c0.002747-0.052856,0.015747-0.102722,0.015747-0.15625   V16.70459c0-4.960449-4.035156-8.996094-8.996094-8.996094h-9.78418c-4.960938,0-8.996094,4.035645-8.996094,8.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H22.666016c-8.307922,0-15.069214,6.637817-15.15686,14.82959   c-0.002258,0.042786-0.006226,0.084656-0.006592,0.127686c0,0.01062-0.001587,0.020874-0.001587,0.031494l0.029297,41.301758   c0,3.710327,2.461121,6.861511,5.860901,7.975281c0.330811,5.591125,6.010376,9.902649,13.222107,9.902649h46.713867   c7.213135,0,12.893494-4.313232,13.22229-9.905884c3.383484-1.116211,5.831421-4.263367,5.831421-7.963257l0.118164-41.3125   C92.499023,33.093445,92.496582,33.077698,92.496521,33.06134z M42.082031,17.966797V16.70459   c0-1.651855,1.34375-2.996094,2.996094-2.996094h9.78418c1.652344,0,2.996094,1.344238,2.996094,2.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H42.066284C42.069031,18.07019,42.082031,18.020325,42.082031,17.966797z    M22.666016,24.123047h54.609375c4.915283,0,9.070679,3.978882,9.210449,8.733398   c-1.340881,7.173462-4.711609,13.725525-9.775879,18.958008c-1.151367,1.190918-1.120117,3.090332,0.070313,4.242188   c0.582031,0.563965,1.333984,0.844238,2.085938,0.844238c0.78418,0,1.567383-0.305664,2.15625-0.913574   c2.057556-2.126526,3.869507-4.443542,5.430847-6.90918l-0.072449,25.335449c0,1.306641-1.158203,2.410156-2.529297,2.410156   H60.384766H16.088867c-1.363281,0-2.558594-1.126465-2.558594-2.412109l-0.018005-25.390076   c7.704041,12.219727,21.28595,20.121033,36.486755,20.121033c6.727539,0,13.444336-1.594238,19.424805-4.609863   c1.479492-0.746094,2.074219-2.550293,1.328125-4.029297c-0.746094-1.47998-2.551758-2.07373-4.029297-1.328125   c-2.703491,1.363281-5.584839,2.37915-8.54895,3.045837c0.201599-0.726685,0.317749-1.488953,0.317749-2.279846   c0-4.706116-3.815063-8.521118-8.521118-8.521118s-8.521057,3.815002-8.521057,8.521118   c0,0.79248,0.116516,1.556335,0.318909,2.284302C27.581909,59.01532,16.293762,47.665283,13.512939,32.878418   C13.639771,28.030396,17.691956,24.123047,22.666016,24.123047z M49.970337,62.781616   c-1.568665,0-2.840332-1.271667-2.840332-2.840332c0-1.568726,1.271667-2.840393,2.840332-2.840393   c1.568726,0,2.840393,1.271667,2.840393,2.840393C52.81073,61.509949,51.539063,62.781616,49.970337,62.781616z    M73.327148,86.291504H26.613281c-3.633606,0-6.323853-1.681396-7.051086-3.467773h60.81604   C79.651001,84.610107,76.960754,86.291504,73.327148,86.291504z"/>
                  </g>
                </svg>
              </div>
              <div class="mbs-service-div">
                <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Strata & Property Managers</h3>
                <p class="" data-animation="zoomIn" data-timeout="200">
                  Take the advantage with smarter facility management to smoothen building functioning and communication.
                </p>
                <label class="mbs-service-header__subtext" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-property-management')}}">Read more</a></label>
              </div>
            </div>
            <div class="col-md-3">
              <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
                <svg style="enable-background:new 0 0 100 100;" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                  <g id="Layer_1">
                    <path d="M75.4583,32.15699c-0.1001-1.82324-0.40088-3.63867-0.89404-5.39844c-0.44727-1.5957-2.10547-2.5293-3.69824-2.0791   c-1.59521,0.44727-2.52637,2.10254-2.0791,3.69824c0.37549,1.33887,0.604,2.7207,0.68066,4.11328   c0.32129,5.64844-1.80518,11.14551-5.83594,15.08398l-0.98438,0.95313c-3.35107,3.23242-5.99854,5.78516-5.99854,11.91406v2.25879   H43.3538v-2.25879c0-6.26367-2.62012-8.73828-6.24707-12.16406l-0.52637-0.49805c-3.9209-3.72168-6.08008-8.74609-6.08008-14.14844   c0-5.35449,2.12939-10.34766,5.99609-14.06055c3.86426-3.70899,8.94385-5.64649,14.31787-5.41406   c3.16113,0.12695,6.27734,1.04492,9.01221,2.65332c1.42969,0.8418,3.26758,0.36426,4.10693-1.06543   c0.83985-1.42773,0.36329-3.2666-1.06494-4.10645c-3.5835-2.10742-7.66797-3.30957-11.81104-3.47656   c-7.01904-0.28613-13.66211,2.22949-18.7168,7.08105c-5.05566,4.85449-7.84033,11.38477-7.84033,18.38867   c0,7.06445,2.82324,13.63477,7.95068,18.50098l0.53564,0.50684c3.2666,3.08594,4.36719,4.125,4.36719,7.80273v3.3819   c-2.21075,1.25317-3.7085,3.6239-3.7085,6.34173c0,1.63684,0.54883,3.14404,1.46307,4.362   c-0.91522,1.24408-1.46307,2.77435-1.46307,4.4339c0,3.99152,3.13348,7.25635,7.06848,7.4856   c0.07416,0.00549,0.14502,0.02222,0.22058,0.02222h2.30939c0.70569,3.08215,3.4646,5.39063,6.75751,5.39063   s6.05182-2.30847,6.75751-5.39063h3.88654c1.65674,0,3-1.34277,3-3s-1.34326-3-3-3H41.15263   c-0.83105,0-1.50732-0.67676-1.50732-1.50781s0.67627-1.50684,1.50732-1.50684h19.49268c1.65674,0,3-1.34277,3-3s-1.34326-3-3-3   H40.93437c-0.71094,0-1.28906-0.57813-1.28906-1.28906s0.57813-1.28906,1.28906-1.28906c0.3432,0,0.66748-0.0697,0.97461-0.17578   h17.73975c1.65674,0,3-1.34277,3-3v-5.25879c0-3.58008,1.13379-4.67285,4.16358-7.59473   c0.3252-0.31348,0.66357-0.63965,1.01367-0.98242C73.09599,46.71656,75.87773,39.53003,75.4583,32.15699z"/>
                  </g>
                </svg>
              </div>
              <div class="mbs-service-div">
                <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Building & Facility Managers</h3>
                <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                  Empower yourself with total control over your facility and seamless interaction with your community and contractors.
                </p>
                <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-facility-management')}}">Read more</a></label>
              </div>
            </div>
            <div class="col-md-3">
              <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
                <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                  <g id="Layer_1">
                    <g>
                      <path d="M91.5,83.5h-7V35.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H66.5c-1.65723,0-3,1.34326-3,3s1.34277,3,3,3h9.99219    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5H59.50781V18.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H23.50391    C19.09082,10.5,15.5,14.09033,15.5,18.50391V83.5h-7c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h83c1.65723,0,3-1.34277,3-3    S93.15723,83.5,91.5,83.5z M21.5,18.50391c0-1.10498,0.89941-2.00391,2.00391-2.00391H51.5    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5h-9.76654c0.0025-0.04218,0.01257-0.08221,0.01257-0.125v-11.75    c0-1.17365-0.95142-2.125-2.125-2.125h-8.25c-1.17365,0-2.125,0.95135-2.125,2.125v11.75c0,0.04279,0.01007,0.08282,0.01257,0.125    H21.5V18.50391z"/><path d="M67.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S65.84277,43.5,67.5,43.5z"/><path d="M67.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C64.5,52.15723,65.84277,53.5,67.5,53.5z"/>
                      <path d="M67.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,63.5,67.5,63.5z"/>
                      <path d="M67.5,73.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,73.5,67.5,73.5z"/>
                      <path d="M42.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,43.5,42.5,43.5z"/>
                      <path d="M27.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,43.5,27.5,43.5z"/>
                      <path d="M42.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C39.5,52.15723,40.84277,53.5,42.5,53.5z"/>
                      <path d="M27.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C24.5,52.15723,25.84277,53.5,27.5,53.5z"/>
                      <path d="M42.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S40.84277,63.5,42.5,63.5z"/>
                      <path d="M35.5,60.5c0-1.65723-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h5C34.15723,63.5,35.5,62.15723,35.5,60.5z"/>
                      <path d="M42.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,33.5,42.5,33.5z"/>
                      <path d="M27.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,33.5,27.5,33.5z"/>
                    </g>
                  </g>
                </svg>
              </div>
              <div class="mbs-service-div">
                <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Developers & &nbsp;&nbsp; Builders</h3>
                <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                  Destine your development for excellence – Equip it with the system managers, owners and occupants love.
                </p>
                <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-construction-development')}}">Read more</a></label>
              </div>
            </div>
        </div>
    </div>
  </section>
@endsection
