@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/resource_studies_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">CASE STUDIES</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#edf3f3;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>RESOURCES</label><label class="path-eclips"></label><label>CASE STUDY</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/resource_case_studies.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/resource_case_studies_mobile.jpg') }}" />
            </div>
        </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#edf3f3;padding-top:10px;">
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown" style="font-family:roboto-light;color:#363636;">FOR CONSTRUCTION & DEVELOPMENT COMPANIES</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <p style="color:#00aeef;font-family:lato-medium;font-size:20px;">Challenges</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">Without the right tools at his disposal, the life of a building manager can be pretty chaotic given that he/she has to report to and keep happy the residents, the committees and the strata managers alike and all the time. This is no doubt a stressful job and the problems start to grow exponentially with the number of units in a building. </p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">Ask almost any building manager about the typical issues – and he will tell you that managing assets and repair history, keeping track of preventative maintenance, staying on top of parcels and keys, ensuring delivery of messages and notifications to residents and most of all preparing regular reports for strata committees with the expectation of remembering historical detail of each and every item are among the most common headaches. No matter how hard you try, something is bound to slip through the cracks and would cast a shadow over all the good work you have toiled for. </p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">My initial days as a budding building manager were not much different. After a few years of toil, I learnt that working harder was not going to solve the problem. The solution lied in working smarter.</p>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
          <p style="color:#00aeef;font-family:lato-medium;font-size:20px;">How the Product Helped</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">When I came across MYBOS, I wasn’t quite sure if it would really help solve my problems. Still curious, I opted for a free-of-charge trial run to see if it was really up to the task. After playing with the system for over one month, I decided to hop on board and I am glad I did.
            In my view, no software simplifies building management like MYBOS. It’s made just for the job and it does it extremely well. It provides all the functions I need on a daily basis as a building manager. Very user friendly, and the support is also excellent.</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">It is hard for me to miss any maintenance, inspection or an amenity booking request because all such items are auto-listed on the dashboard. The all-in-one calendar lays out the whole schedule of activities without having to scroll through pages.</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">The best part is that I don’t have to scan through bulky folders to find out history of any case or asset I am dealing with because the system maintains the total history.</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">All data is exportable which makes it easier to do any other work on the sheets without requiring to maintain information separately.
            Communicating with residents is much easier with SMS and email broadcasts. Residents have their own portal through which they can directly create maintenance requests and book amenities, and keep abreast of the latest community news and notices.</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">Previously I had to spend weeks preparing a standard report for the committee meetings. No more, because all I do is just one click and the complete report is auto-generated. It is just fantastic.</p>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
          <p style="color:#00aeef;font-family:lato-medium;font-size:20px;">Benefits</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">I can confidently say that I am more productive with MYBOS. I am now managing two buildings having about 250 units with satisfied resident communities. I don’t have to scan thick folders to get hold of apartment, asset or case history. Preventative maintenance has become much simpler and I cannot seem to miss anything important. All I need to do is use the system to its potential and whenever I need, it creates highly readable reports that I can present straight away to the owners and strata managers. </p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">At a personal level, I seem to have a touch more time available. I am more relaxed than I used to be. Long hours at work are not a norm anymore, but an exception.</p>
          <p style="color:#636363;font-family:lato-light;font-size:15px;font-weight: bold;text-align:justify;">Thank you MYBOS.</p>
        </div>
      </div>
    </div>
  </section>
@endsection
