@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/self_managed_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">BENEFIT FROM INCREASED RENTAL</h1>
                    <p class="mbs-software-backround__subtext" style="font-family: lato-regular;">AND ASSET VALUE THROUGH A BETTER MAINTAINED BUILDING.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ebebeb;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>INDUSTRIES</label><label class="path-eclips"></label><label>OCCUPANTS, OWNERS & MANAGING AGENTS</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown" style="font-family:roboto-light;color:#363636;">FOR OCCUPANTS, OWNERS & MANAGING AGENTS</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-facility-section-div">
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 40 40" id="Слой_1" version="1.1" viewBox="0 0 40 40" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g>
                  <path class="mbs-svg-blue" d="M37.9,28.6l-10-10c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l10,10c1.8,1.8,1.8,4.8,0,6.6c-0.9,0.9-2.1,1.4-3.3,1.4   s-2.4-0.5-3.3-1.4L17.4,24.2c3.5-1.5,6.1-4.5,7.2-8.2c0.4-1.4,0.5-2.8,0.4-4.2C24.6,5.6,19.4,0.4,13.2,0c-2.1-0.1-4.1,0.3-6,1.1   C7,1.3,6.8,1.5,6.7,1.9C6.6,2.2,6.7,2.5,7,2.7l7,7.1c1.2,1.2,1.2,3.1,0,4.3c-1.2,1.1-3.2,1.1-4.3,0l-7-7.1C2.4,6.8,2.1,6.7,1.8,6.8   C1.4,6.9,1.2,7.1,1,7.4C0.2,9.1-0.2,11-0.1,12.9C0,18.9,4.6,24.1,10.4,25c0.7,0.1,1.4,0.2,2.1,0.2c0.9,0,1.9-0.1,2.8-0.3L28.5,38   c1.3,1.3,2.9,2,4.7,2s3.5-0.7,4.7-2C40.5,35.4,40.5,31.2,37.9,28.6z M15.2,22.8c-1.4,0.4-3,0.5-4.5,0.2C5.8,22.2,2,17.9,1.9,12.9   c0-1.1,0.1-2.2,0.4-3.3l6,5.9c1,1,2.2,1.5,3.6,1.5c1.4,0,2.6-0.5,3.6-1.5c2-2,2-5.2,0-7.2L9.5,2.4C10.6,2.1,11.9,2,13.1,2   c5.2,0.3,9.6,4.6,9.9,9.9c0.1,1.2,0,2.4-0.4,3.5c-1,3.5-3.8,6.3-7.3,7.3C15.3,22.8,15.3,22.8,15.2,22.8z"/>
                </g>
                <g>
                  <path class="mbs-svg-blue" d="M31.6,34.7c-1.7,0-3-1.3-3-3c0-1.7,1.3-3,3-3s3,1.3,3,3C34.6,33.3,33.3,34.7,31.6,34.7z M31.6,30.7c-0.6,0-1,0.4-1,1   c0,0.6,0.4,1,1,1s1-0.4,1-1C32.6,31.1,32.2,30.7,31.6,30.7z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Stay on top of your property’s upkeep through online logging and tracking of maintenance requests .</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-blue" d="M18.64648,34.27417c0,1.65674,1.34326,3,3,3H88v42.94385c0,1.10693-0.90088,2.00781-2.00781,2.00781H14.00781    C12.90088,82.22583,12,81.32495,12,80.21802V26.25659h79c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3H76.83234    c0.00189-0.06226,0.00262-0.11658,0.00262-0.15234c0-4.59326-3.74121-8.33008-8.33984-8.33008    c-4.59326,0-8.33008,3.73682-8.33008,8.33008c0,0.03577,0.00073,0.09009,0.00262,0.15234H39.83234    c0.00189-0.06226,0.00262-0.11658,0.00262-0.15234c0-4.59326-3.74121-8.33008-8.33984-8.33008    c-4.59326,0-8.33008,3.73682-8.33008,8.33008c0,0.03577,0.00073,0.09009,0.00262,0.15234H6v59.96143    c0,4.41553,3.59229,8.00781,8.00781,8.00781h71.98438c4.41553,0,8.00781-3.59229,8.00781-8.00781V31.27417H21.64648    C19.98975,31.27417,18.64648,32.61743,18.64648,34.27417z M66.1665,20.02515c0.0415-1.24805,1.07031-2.25098,2.32861-2.25098    c1.26172,0,2.29346,0.99951,2.33838,2.25391c-0.00592,0.07721,0.00366,0.1521,0.00372,0.22852h-4.67426    C66.16302,20.1792,66.17255,20.10333,66.1665,20.02515z M29.1665,20.02515c0.0415-1.24805,1.07031-2.25098,2.32861-2.25098    c1.26172,0,2.29346,0.99951,2.33838,2.25391c-0.00592,0.07721,0.00366,0.1521,0.00372,0.22852h-4.67426    C29.16302,20.1792,29.17255,20.10333,29.1665,20.02515z"/>
                    <path class="mbs-svg-blue" d="M40.71631,48.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S39.05957,48.27417,40.71631,48.27417z"/>
                    <path class="mbs-svg-blue" d="M71.14941,48.27417H74.5c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S69.49268,48.27417,71.14941,48.27417z"/>
                    <path class="mbs-svg-blue" d="M55.93311,48.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S54.27637,48.27417,55.93311,48.27417z"/>
                    <path class="mbs-svg-blue" d="M25.5,57.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3H25.5c-1.65674,0-3,1.34326-3,3    S23.84326,57.27417,25.5,57.27417z"/>
                    <path class="mbs-svg-blue" d="M40.71631,57.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S39.05957,57.27417,40.71631,57.27417z"/>
                    <path class="mbs-svg-blue" d="M71.14941,57.27417H74.5c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S69.49268,57.27417,71.14941,57.27417z"/>
                    <path class="mbs-svg-blue" d="M55.93311,57.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S54.27637,57.27417,55.93311,57.27417z"/>
                    <path class="mbs-svg-blue" d="M25.5,66.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3H25.5c-1.65674,0-3,1.34326-3,3    S23.84326,66.27417,25.5,66.27417z"/>
                    <path class="mbs-svg-blue" d="M40.71631,66.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S39.05957,66.27417,40.71631,66.27417z"/>
                    <path class="mbs-svg-blue" d="M71.14941,66.27417H74.5c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S69.49268,66.27417,71.14941,66.27417z"/>
                    <path class="mbs-svg-blue" d="M55.93311,66.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S54.27637,66.27417,55.93311,66.27417z"/>
                    <path class="mbs-svg-blue" d="M25.5,75.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3H25.5c-1.65674,0-3,1.34326-3,3    S23.84326,75.27417,25.5,75.27417z"/>
                    <path class="mbs-svg-blue" d="M40.71631,75.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S39.05957,75.27417,40.71631,75.27417z"/>
                    <path class="mbs-svg-blue" d="M55.93311,75.27417h3.35059c1.65674,0,3-1.34326,3-3s-1.34326-3-3-3h-3.35059c-1.65674,0-3,1.34326-3,3    S54.27637,75.27417,55.93311,75.27417z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Book building amenities online in advance.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 78 49">
                <image id="Layer_0" data-name="Layer 0" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE4AAAAxCAMAAABDAG87AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAk1BMVEUArvAAre8Aru8Are8Aru8Are8ArvAAre8Are8Aru8Aru8Aru8Are8ArvAAre8Aru8Are8Are8Aru8Aru8Aru8Aru8ArvAAre8Aru8Are8Aru8Are8Aru8ArvAAre8Aru8ArvAArvAAre8ArvAArvAAre8Aru8ArvAArvAArvAArvAArvAArvAAre8ArvAAru8AAABqHkS9AAAALXRSTlMAAABEiLu7iBG7Ecyq3Xd33SKZIt1VVe5mZu5VM8zMRETuM5kzmaqqEXdmiCLTy7/lAAAAAWJLR0Qwrtwt5AAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+EIDgczGs0l1LoAAAXySURBVEjHVVeLQuJKDE2mtkJ1W/GxICKiqy64zsz//91NTpLBW4XOM5NkTk4CEbP8y4eIE3NK+GdKSYe0TdRd9MNlhxn54GFdzLZEetFQUQm7VYj0CeIIbRtaLHOpJdd+lBFdKOM4Tjtki1Qp6ROEaks10g8WQQnGXl5c1VxKrbVcjSqGXAPGi6AntpuWGDbdcYpOuUH4XItmv6b5RuRdu7kiFOpBtRTSMIEWwcZkbtRZCoG0qrneqhK3tdQ7mOEGMvSEejrUVMG7qY9D7DJ09ZxzHXXNQuydCfZg01n/EEOmlzrErs1Mh/XmocRTLb2tH3KZfSfOt3lmB4COQKLfkUHEGn6+TMwl3zMMFO/N2Myuu6tHZ3jFlSR3PzAQxuDcBzH2QQ36Lff7gO1EZ+/H1UEmYS/8iRXkTXZ56or7UtabxI/bWm4MCqZ3ahLNy8k3NcvIkBP2QtvxaZkFKbnq0z/dWUAgYnAlYadDn1rkOAJ/QCntLp9zzfIokHNRmfvLHcEyV9K3UwtLPvvOwjZCZ3OVTVT1B7GW636Ds5siDo3ke7npajdolz9vVa+St8M0dd3L4eUwT/2rSJOAe53UqeYcs96AaqB2KFM4Uda8/Cry5P20a5cNG98mMV/0ve/s4ICcgVptJXabw/tE4wVc9ueNTX3y+AHg3j+qXs7lyI4FjydIbV5t6yVMn9VRn6sfceQGmNTVp/rzeXUGFDd+8whzGKv2b2s5/aZrVw6HsBOZobKTCMnbncmLNUZ6P4CL43dbub6/x4i4u99TP9zU+/dYAeCOf8Xe1zfHryHGKZnMgU4nolvJk+8cT3sgWBz56aey8TWfhK+275yokUYEltGhOXX3KrewMRtGBYsCrshrdooxppUNj+KStdnriMH1c4t+eY73BdIUG09itV7iTX+apq+Agtmm/t6IU9ajKcYu1pnADqW0FJc8wunjB6wcvlb8P9oymyBY9KsDWMz9Ze3kAcM8i27/cI1HjbC63KWgm+Z1coPl65+E3amRPQ6kwIic/S2RMADWi70E/esTn4HUbPJkDNZbigHfbL7xfOupU59ewLTStaPqdrVw2nDMWFI0VrcAT98Cg2WKROVygu47CfkJDroWNf8ck+UtT+zOvJagPZZYE1MXCdFSNVlgst7DHq1JoHdlRlnMOhSciAwPdpF7vQ24nygIynZ2IuRFdbmTnH8zBu+T50DnB0sN7R7FotwFBXhdY774FOVAqydx8LvRuzGPJ5lgc2o5UGTc5HrNfp9xsqp3FDd8ATFvr3VunOBZ0wmIQ7FWFn1JBC48LTOTBxfxppbXhRnFd2ZK8gqDg7kpigjP6TK7EMZ/NC5xJjZuua71I0o7vyXy46LACOWMMAzL6VOtTS2ugp3XEqxnlkwBMvKsfC6MUqJWZcnrK9etSyZyGKf0pgVS4+rUDvMyJrU6xCxw/pX3tySjHQd5WepK6rot/ygGLIk0wPohkdoN1bZ7XeqGPANYjtJKqeQ+ygs+s1rkB1fHY7yVbvoZapmiQrOVosttrv9akXfmxFYv+VUbBVnMkrHDZc4fQQ8tI/ZyBnHzk12xYa25MuCcnMotCLUCtCQhOx/6fqnPtgjtamPo+6EfhqFfytegXRnul/jWKflHZ1hieNnfC2AxtxyeSPCheT1bDaIFv/cFnhjTyqJgqqD8wQDWIiX5Eqyta9rqq1rtAWHoYlHRbxWC7VlTkAnweRNqI6gM8pZW02n+1N7FNM/zNE3yOWljRl+a82k6aW9Cd9LOacKK+SR/8rrQ2uqvDK3gebrVHyGLqA4iDXtJbz+zKH5NeGCQQ0pmJK/U/GE7DC23oun+eK43DKERmuRMZpUxOzwDlce9eOFP0Cx0keRQSn/opJLD071oU3sHbdvAATNS63WHg4532jgchgpVnD7txHFf2zXAy0j9uEzcsPkfv87M/dkncM/Px+RRF79JFnvsKS4o265ikkv2yZLjqVpO4shcro5nrjFikGTYG0xK4AJochUq6sco4U2eIhP46UcvTDj9B2s3p5IrDk5WAAAAAElFTkSuQmCC"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Order in-house services anytime, from anywhere.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 60 60">
                <image id="Layer_0" data-name="Layer 0" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAk1BMVEUAru8Are8ArvAAru8Aru8Aru8Aru8Aru8Are8Are8Aru8Are8Aru8Aru8Aru8Are8ArvAAru8Aru8Are8Are8Are8Aru8Are8ArvAArvAAru8ArvAArvAAre8ArvAAre8Are8Are8ArvAAru8ArvAAre8ArvAArvAArvAArvAArvAArvAAre8Aru8ArvAAre8AAABegaNUAAAALXRSTlMAAABEEarMZoi7me6Id90RmbsimWZE7t13RFXM3XcRM8wiuzMiqu4zVYhmqlV9fPXZAAAAAWJLR0Qwrtwt5AAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+EIDgc1A/8U2/wAAAYKSURBVEjHTVeJQuJADJ2kBS0wYLlxLQp47KqZ+f+/25xVzjKdI3l5eQkp2QP0hfzJDwRI8m6apuULHkeUSSijCMhTx7d8yV0e101Av5oJUak0vQPfPOk6WYKyPdpRSXfXl06UOXhfiKjWStS1tlRmop4kK81evsJxa0SzHmaF5gs2Oy/58Bb1GJkQh4GeZBaYZ34275d5ycrsfCi1l1uI5hUKKo4U2maoBoDdgDXxcXZOShuiLYbdipp7p2Oow/YyaxZUd+qcbNju61RnCqzgyOqnbq/Owg8scKCpDKFBe1/qOrwdV9rLImZemz0Jajkmd413OLEdijA6UI6fAW8T1XE1FImyEkJig9jUevKA6ET09XooeswsCnJdaKEeKMjwWGmnDLJIj+ApuSCijQYX4B960giqZzgjWslatKiadcn9NweMe+pFGqg2QaHUzmtnMTKbLSYW57DcKSb303le989gZp3/1HJKHtMxYh6Z4ILa7AinO6J9oz/XU6oPTgwIBqrp6FgZcfWJGrI0cEq8HIc8KbX2LYxh9vgCRtTB6Aq/AEzpcc4JWSWzstJajwLPKF/vQGg8QcOhKPCd1dDz2pf7S/LQOpzOxIhvXFjOYKQ0ePIFfRLEWjShCZEY0wLD+fjtjDScTUNCrdQWAw984oVhGnLOxzzwlV7mgR87z3clpsUomRBYeqn3PePEElRMhBS0qlcjp8xmhNFGDHNSR7pUpsuzKOL8pIShXOiOG8dtE9AwdXT92dfm8HuoRXEajUU/K1xX2yGXAmNElEZycWUhc1iDDZY4lpvgqbkttI4a4GnGz2WdQJDXPiBFevtOcvKt0J17oq4okG0tWaPk5IrUitxwVkM61AfjeBCEb9/V+hpWj+KNIUT+lseG9qs0Zq3t29PBwgGR5RgMU41VRsvwM+eCEcaqEk9Zl3q0REKbmXBkNzrF1EPAruzPY94og97q+8oLaIIQTYuTsw285GBDZWGVwIsEDxzBo+Y67bXmR/mj4GDnBcbqEl72dX52DOPkWIoj5Zwn6TIvvDqo8Lws9dG5b/Ym+MWCkP9ItPQ8p9rvVBhfuc7VbfpVlt1CY2ByrNNIdEy3PafEh0zcc17dKcg4cl1VPvQTQ8I8eLzxakFlJ9dE/aul0ViXI5v9rBh2JZTRhqiRn5WOGggvXKYuXnzMA/wR0I/HvPjL++xKEadBSJ3g49+wbaJpQVMPi6wHEFZ/Fz2jU8qcf3Np3Gm11ZMnIgk0nX22VssdM2tLEq62X4XXqYrc842m1EagK1xtIX3uRVREU16O69B+TFanT5siSkPTzdCs1IuTdhYJCw3G0t3nPxZyUbN+CxEq/t51onLLxeMqjWq8q3QSf9hnj6oY2xxf2IBy+PQmCs73st2scaWGU/5ml05FfUYpN5xW+fvVS/R6sWfXurWmQTvl683aVGN9fGHE3nnbk/jMIwwYb/nG9j5tdgZ2m/cM6k1OeuF+7Wbl6SJgM2A9/zzV+qe7dn0tgxRNU/FlY7nQflF9v2gt3Xj1ZE4STXJzFisaUXoBmGVAAtIcvxhx+jThhbta3iBN69I73fRFZVhFv9we81ErzvE2Ct92X+bRRsyothJHr3V4sCbTeyWvt8nTSddviNs5o4b0V6myYJk2AW/2NKy9J7LuGMZiypowLImWUXYb6a8mZdm6GLSdMOUwy7vb2PWbnt8e89tBCLq8hehMyrvokzXHauh2qvVQeXztuv56Zbxf5KfUunLIZ2vrgK0sWVsXejqBSzSujzPegLTIFiE7ScHlkcMk30w4eOmF63Cn1nFjXrrG2jpLzOZxGK79te97Pr3r7/Nn0449X0qXGe8+O1sJaJa88zS/RiseOWqpGxngStx+TtiF+QDeUUCb51Wz7cP/+4SkOl6BOV7u3iT56kObfinYKh/EU5p3+XvnLQyERJug7vJEAaf94nVUwuR/lT4WrM/ajxTqurdB+xjtZh66bq7tiew++1Ylda30CiVbXfJmKkhrtLyVEcirxWo5GW72Twi82w0KjdXgg6FmnJek3vHyOYN+r/oXfx3RaiX8BxI4g2/f9NPTAAAAAElFTkSuQmCC"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Know your neighbourhood like none other through directory services.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-blue" d="M90.97687,55.19925c-0.62646-1.53418-2.37646-2.27002-3.91162-1.64307c-1.53369,0.62646-2.26953,2.37744-1.64307,3.91162    c0.4292,1.05127,0.14844,1.85205-0.05322,1.9668l-3.96149-0.30878l-18.99603-46.5094l2.62353-3.00848    c0.22461-0.03662,0.979,0.34082,1.40771,1.3916c0.62695,1.5332,2.37842,2.26709,3.91211,1.64258    c1.53369-0.62695,2.26904-2.37793,1.64258-3.91211c-1.09619-2.68311-3.3584-4.61279-5.9043-5.03613    c-2.13476-0.35693-4.17089,0.37109-5.58545,1.99316L40.30341,28.83994l-20.56824,8.40082    c-9.34131,3.81543-13.8374,14.52002-10.02246,23.86182c1.84863,4.52539,5.34863,8.06006,9.85498,9.95313    c2.28174,0.95801,4.68311,1.4375,7.08545,1.4375c1.86475,0,3.72784-0.30176,5.53809-0.87988    c0.013,0.03558,0.01727,0.07208,0.03174,0.10742l7.77734,19.04053c0.91406,2.23828,2.64453,3.98682,4.87402,4.92285    c1.12793,0.47412,2.31543,0.71094,3.50342,0.71094c1.15918,0,2.31836-0.22559,3.42383-0.67676    c4.61963-1.88721,6.84277-7.18115,4.95605-11.80078l-4.99414-12.22705c-0.62695-1.53418-2.37842-2.26855-3.91162-1.64307    c-1.53418,0.62646-2.26953,2.37793-1.64307,3.91162l4.99414,12.22705c0.63623,1.55713-0.11328,3.34131-1.67041,3.97754    c-0.75439,0.30908-1.5835,0.3042-2.33496-0.01123S45.8631,89.2476,45.555,88.4932l-7.77734-19.04102    c-0.00537-0.01306-0.01398-0.02368-0.01947-0.03662l15.77838-6.4444l31.36609,2.44489    c0.16895,0.01318,0.33643,0.02002,0.50244,0.02002c1.94775,0,3.70654-0.89648,4.88184-2.50684    C91.80841,60.84476,92.07257,57.88285,90.97687,55.19925z M74.71313,58.60404l-17.44342-1.35968L46.4964,30.86661l11.5011-13.1886    L74.71313,58.60404z M21.89142,65.52348c-0.24792-0.10413-0.48389-0.22705-0.72253-0.34576l16.70056-6.82123    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62646-1.53418-2.37988-2.26758-3.91162-1.64307l-19.36206,7.90833    c-0.37012-0.59338-0.7016-1.21582-0.97144-1.87659c-0.27179-0.66541-0.46753-1.34204-0.61658-2.02142L34.0091,48.9058    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62695-1.53418-2.37842-2.26904-3.91162-1.64307l-16.69421,6.81824    c1.11835-3.22827,3.55353-5.98364,6.9574-7.3739l19.25922-7.86627c0.10065,0.01013,0.20093,0.02643,0.30182,0.02643    c0.03876,0,0.07678-0.01056,0.11548-0.01202L50.9173,57.5596l-19.61182,8.01026    C28.26349,66.81303,24.91974,66.79545,21.89142,65.52348z"/>
                    <path class="mbs-svg-blue" d="M69.50226,19.39896l12.09961,29.62451c0.2688,0.65771,0.74786,1.16174,1.32422,1.48151    c0.18164,0.14124,0.37665,0.2691,0.59521,0.36859c0.40332,0.18359,0.82568,0.27051,1.24121,0.27051    c1.13965,0,2.22949-0.65332,2.73242-1.75781c2.87354-6.3125,3.01758-13.61523,0.39551-20.03662    c-2.62256-6.4209-7.83838-11.53467-14.31006-14.03027c-0.99597-0.38507-2.06665-0.19604-2.86493,0.3963    C69.48425,16.47007,68.93854,18.01883,69.50226,19.39896z M82.33575,31.6182c0.88116,2.15778,1.32825,4.44666,1.3927,6.74341    l-5.11902-12.53339C80.17145,27.51328,81.45459,29.46061,82.33575,31.6182z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Never miss an important notice or announcement from your building managers.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-blue" d="M87.95965,79.85107c-9.89453,0.14258-26.50195,3.43359-27.2041,3.57324c-0.10498,0.02148-0.20898,0.04785-0.31152,0.08008    c-13.01465,4.08496-23.9458,3.69043-32.14551-1.21582c-11.51953-6.9209-13.25195-20.12598-13.27539-20.31445    c-0.08057-0.5957,0.07422-1.16406,0.4541-1.65039c0.43457-0.56055,1.05469-0.90527,1.81738-0.99902    c1.40186-0.19238,2.69629,0.74805,2.87939,2.08105c1.21484,8.83203,7.41992,15.58105,16.59863,18.05176    c3.15234,0.85059,11.89697,0.82422,14.5918-1.20898c0.93164-0.70313,1.4541-1.69141,1.4707-2.7832    c0.01465-0.96582-0.375-1.87305-1.09668-2.55371c-0.65869-0.62012-2.40869-2.26855-13.94727-1.41992    c-1.44092,0.09473-2.6958-0.88086-2.80322-2.21387c-0.05029-0.58887,0.14307-1.17285,0.54102-1.63965    c0.4624-0.54102,1.09863-0.85547,1.83984-0.91016c1.44189-0.10742,2.68457-0.2041,3.78809-0.29004    c4.41113-0.34277,6.42529-0.49609,10.05176-0.45508c0.05225-0.00098,0.14453-0.00195,0.19873-0.00488l0.21973-0.00098    c0.17432,0,0.38037,0,0.53809-0.00586c10.02051,0.03418,11.98681,2.77051,12.93164,4.08594    c0.55908,0.77734,1.59961,2.2334,3.57471,2.19238c0.17822-0.00391,0.35596-0.02246,0.53076-0.05762l8.57373-1.69824    c1.18604-0.17969,2.19141-0.37891,3.16504-0.57129c2.09912-0.41602,3.91211-0.77539,7.14941-0.87012    c1.65576-0.04883,2.95947-1.43066,2.91064-3.08691c-0.04785-1.65527-1.396-2.93652-3.08643-2.91113    c-3.7373,0.10938-5.97559,0.55371-8.14014,0.98242c-0.96729,0.19141-1.88086,0.37305-2.9585,0.53418    c-0.0459,0.00684-0.09229,0.01465-0.13818,0.02441l-7.12061,1.41016c-2.38232-3.04883-6.50097-5.99707-17.46289-6.0332    c-0.16895,0.00488-0.31152,0.00488-0.43359,0.00488c-0.17676-0.00195-0.32666,0.00098-0.4585,0.00488    c-3.8457-0.04004-6.08496,0.12988-10.51074,0.47461c-1.09717,0.08496-2.33252,0.18164-3.76709,0.28711    c-2.33789,0.1748-4.45264,1.2373-5.95947,2.99902c-1.00586,1.18164-1.64844,2.57813-1.88428,4.05176    c-1.56934-2.03613-2.58643-4.46973-2.96289-7.20801c-0.6333-4.59082-4.94043-7.82129-9.58398-7.21387    c-2.32471,0.28809-4.38379,1.4502-5.79199,3.2666C9.36981,58.396,8.77899,60.57764,9.07,62.71924    c0.01855,0.16211,2.01514,16.22656,16.14307,24.71484c5.40869,3.23633,11.72461,4.87793,18.77197,4.87793    c5.55176,0,11.64111-1.02051,18.1001-3.03418c5.83447-1.15723,18.54492-3.32129,25.96045-3.42676    c1.65674-0.02441,2.98047-1.38672,2.95654-3.04297C90.97869,81.15088,89.61053,79.81299,87.95965,79.85107z"/>
                    <path class="mbs-svg-blue" d="M53.75781,41.00128c-1.13306,0.84467-2.60602,1.26697-4.41888,1.26697c-1.31848,0-2.38251-0.20911-3.19318-0.6283    c-0.81061-0.41925-1.43896-0.95074-1.88495-1.59662c-0.44598-0.64581-0.86212-1.44922-1.24634-2.41028    c-0.31622-0.81067-0.69733-1.42145-1.14337-1.8335c-0.44598-0.41199-0.99194-0.61798-1.63776-0.61798    c-0.7962,0-1.45233,0.26471-1.96741,0.79309c-0.51501,0.52844-0.77252,1.16394-0.77252,1.90558    c0,1.27728,0.42957,2.59265,1.2876,3.94507c0.85797,1.35248,1.97455,2.43402,3.3476,3.24463    c1.23468,0.71783,2.70581,1.18085,4.36475,1.44116v4.76282c0,1.65723,1.34326,3,3,3s3-1.34277,3-3v-4.75641    c1.25879-0.20166,2.42035-0.52246,3.45844-0.99432c1.81287-0.82404,3.19006-1.96429,4.13049-3.41974    s1.41113-3.09735,1.41113-4.92358c0-1.52448-0.27087-2.81207-0.81372-3.86267c-0.54285-1.05066-1.29785-1.91901-2.26611-2.60602    c-0.9682-0.68707-2.14246-1.27008-3.52271-1.7511c-1.38031-0.48102-2.92224-0.9198-4.62494-1.31842    c-1.35962-0.34302-2.33508-0.60468-2.92529-0.78284c-0.59021-0.17822-1.17426-0.42542-1.7511-0.74164    c-0.57678-0.31622-1.03003-0.69324-1.35962-1.13306c-0.32965-0.43982-0.49445-0.961-0.49445-1.56567    c0-0.97546,0.48413-1.80975,1.45239-2.50299c0.9682-0.69324,2.24237-1.04034,3.82141-1.04034    c1.7027,0,2.93872,0.31934,3.70819,0.95795c0.76941,0.63861,1.42865,1.52753,1.97766,2.66779    c0.42542,0.79626,0.82098,1.37,1.18457,1.72021c0.36359,0.35016,0.89612,0.52527,1.59656,0.52527    c0.76941,0,1.41113-0.2915,1.92615-0.87549c0.51508-0.58405,0.77258-1.23914,0.77258-1.96741    c0-0.7962-0.20605-1.61407-0.61804-2.45148c-0.41205-0.83746-1.06403-1.63782-1.95709-2.40002s-2.01581-1.37305-3.36823-1.8335    c-0.53613-0.1825-1.12317-0.31824-1.73828-0.42834v-5.10211c0-1.65723-1.34326-3-3-3s-3,1.34277-3,3v5.06    c-1.13171,0.17157-2.18085,0.43805-3.12866,0.82068c-1.7171,0.69324-3.02832,1.68622-3.93475,2.97687    c-0.90649,1.29059-1.35968,2.7677-1.35968,4.42914c0,1.7439,0.4295,3.20343,1.28754,4.37769s2.01892,2.10132,3.48157,2.78113    s3.27863,1.27417,5.44891,1.78198c1.62024,0.37079,2.91504,0.72101,3.88324,1.05066    c0.96826,0.32959,1.7583,0.80652,2.36914,1.43176c0.61078,0.62524,0.91675,1.43896,0.91675,2.44116    C55.45739,39.10291,54.89087,40.15668,53.75781,41.00128z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Achieve more gains on your property value through smarter, efficient building.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-insight-section-div insight-section-div-nocolor">
          <p class="mbs-insight-section-text insight-section-text-big" style="color:#000!important;">User Insight</p>
          <p class="mbs-insight-section-text insight-section-text-small">Over 40 years of professional engineering and management practice, in searching for suppliers for systems or services, I generally put weight on testing the vendor’s claims through specification, discussion, and consultation with other users. That is how we found MYBOS in the first place. Great find, no regrets.”</p>
          <p class="mbs-insight-section-text insight-section-text-big" style="margin:0;color:#000!important;">Colin Knowles</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;padding-bottom: 0;margin:0;">Colin Knowles</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;margin:0;">Distillery Hill, Sydney</p>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-2" style="background-color: #ffffff;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown" style="font-family:roboto-light;color:#707070;">Cherish the Experience</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_owner_top.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_owner_top_mobile.jpg') }}" />
            </div>
        </div>
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_owner_under.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_owner_under_mobile.jpg') }}" />
            </div>
        </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-3" style="background-color: #ebebeb;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown" style="font-family:roboto-light;color:#707070;">Additional Pluses</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/Rectangle_imac-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div"  style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/cloud_product-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div">
                <p style="margin:0;font-size: 18px;">Cloud-based system guarantees regular back-up, maximum service uptime and no additional hardware cost to the subscriber</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/productpage-mybos-svgimg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div" style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="mbs-section__container container" style="padding-bottom:100px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse" data-animation="zoomIn"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Request A Demo</a></div>
            </div>
        </div>
    </div>
  </section>
@endsection
