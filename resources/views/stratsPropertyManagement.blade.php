@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/property_management_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.2; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">TAKE ADVANTAGE OF SMARTER FACILITY MANAGEMENT</h1>
                    <p class="mbs-software-backround__subtext" style="font-family: lato-regular;">TO SMOOTHEN BUILDING OPERATIONS AND OCCUPANT INTERACTION.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ebebeb;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>INDUSTRIES</label><label class="path-eclips"></label><label>STRATA & PROPERTY MANAGEMENT</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">FOR STRATA & PROPERTY MANAGEMENT</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-facility-section-div">
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-red" d="M90.97687,55.19925c-0.62646-1.53418-2.37646-2.27002-3.91162-1.64307c-1.53369,0.62646-2.26953,2.37744-1.64307,3.91162    c0.4292,1.05127,0.14844,1.85205-0.05322,1.9668l-3.96149-0.30878l-18.99603-46.5094l2.62353-3.00848    c0.22461-0.03662,0.979,0.34082,1.40771,1.3916c0.62695,1.5332,2.37842,2.26709,3.91211,1.64258    c1.53369-0.62695,2.26904-2.37793,1.64258-3.91211c-1.09619-2.68311-3.3584-4.61279-5.9043-5.03613    c-2.13476-0.35693-4.17089,0.37109-5.58545,1.99316L40.30341,28.83994l-20.56824,8.40082    c-9.34131,3.81543-13.8374,14.52002-10.02246,23.86182c1.84863,4.52539,5.34863,8.06006,9.85498,9.95313    c2.28174,0.95801,4.68311,1.4375,7.08545,1.4375c1.86475,0,3.72784-0.30176,5.53809-0.87988    c0.013,0.03558,0.01727,0.07208,0.03174,0.10742l7.77734,19.04053c0.91406,2.23828,2.64453,3.98682,4.87402,4.92285    c1.12793,0.47412,2.31543,0.71094,3.50342,0.71094c1.15918,0,2.31836-0.22559,3.42383-0.67676    c4.61963-1.88721,6.84277-7.18115,4.95605-11.80078l-4.99414-12.22705c-0.62695-1.53418-2.37842-2.26855-3.91162-1.64307    c-1.53418,0.62646-2.26953,2.37793-1.64307,3.91162l4.99414,12.22705c0.63623,1.55713-0.11328,3.34131-1.67041,3.97754    c-0.75439,0.30908-1.5835,0.3042-2.33496-0.01123S45.8631,89.2476,45.555,88.4932l-7.77734-19.04102    c-0.00537-0.01306-0.01398-0.02368-0.01947-0.03662l15.77838-6.4444l31.36609,2.44489    c0.16895,0.01318,0.33643,0.02002,0.50244,0.02002c1.94775,0,3.70654-0.89648,4.88184-2.50684    C91.80841,60.84476,92.07257,57.88285,90.97687,55.19925z M74.71313,58.60404l-17.44342-1.35968L46.4964,30.86661l11.5011-13.1886    L74.71313,58.60404z M21.89142,65.52348c-0.24792-0.10413-0.48389-0.22705-0.72253-0.34576l16.70056-6.82123    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62646-1.53418-2.37988-2.26758-3.91162-1.64307l-19.36206,7.90833    c-0.37012-0.59338-0.7016-1.21582-0.97144-1.87659c-0.27179-0.66541-0.46753-1.34204-0.61658-2.02142L34.0091,48.9058    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62695-1.53418-2.37842-2.26904-3.91162-1.64307l-16.69421,6.81824    c1.11835-3.22827,3.55353-5.98364,6.9574-7.3739l19.25922-7.86627c0.10065,0.01013,0.20093,0.02643,0.30182,0.02643    c0.03876,0,0.07678-0.01056,0.11548-0.01202L50.9173,57.5596l-19.61182,8.01026    C28.26349,66.81303,24.91974,66.79545,21.89142,65.52348z"/>
                    <path class="mbs-svg-red" d="M69.50226,19.39896l12.09961,29.62451c0.2688,0.65771,0.74786,1.16174,1.32422,1.48151    c0.18164,0.14124,0.37665,0.2691,0.59521,0.36859c0.40332,0.18359,0.82568,0.27051,1.24121,0.27051    c1.13965,0,2.22949-0.65332,2.73242-1.75781c2.87354-6.3125,3.01758-13.61523,0.39551-20.03662    c-2.62256-6.4209-7.83838-11.53467-14.31006-14.03027c-0.99597-0.38507-2.06665-0.19604-2.86493,0.3963    C69.48425,16.47007,68.93854,18.01883,69.50226,19.39896z M82.33575,31.6182c0.88116,2.15778,1.32825,4.44666,1.3927,6.74341    l-5.11902-12.53339C80.17145,27.51328,81.45459,29.46061,82.33575,31.6182z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Interact with residents - broadcast notices and announcements via in-app messaging, email or SMS.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 58 64">
                <image id="Layer_0" data-name="Layer 0" x="4" y="8" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAADAFBMVEXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFTwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXxVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXwVFXxVFXwVFXwVFXwVFXwVFXwVFXwVFXwU1TwVFXwVFXwVFXxVFXwVFXwVFXwVFXxVFXwU1TxVFTwVFTxU1QAAAAUAQQwAAAA+XRSTlMAAiZlntHGgD8XCS9tn9C7eDUWAV360lAyoMsSkfnl/Gpa6OzwHC3Y/NtCSqP4aFTTYjudbgghs/6nB0Tm+mEw4fycFF7i/nIDUqGpBTjz3h2Wm1nudAZ7GOncKgSMKwv9kkjvUygx9sFWW3lVqoZXvRnKOaT76pTn1fTxvwyZ9+C5jtcf1nesz1+QsK2uQ5M3gq862c4KY7gQfH/tbPUkfatr5Gf9LkyPqHYzimY89rRp+MX0SXFAWBtHos1zmJoahfKH8bJ1pVzrPZVvpku1iIRRQd8R3SIPZI1+8Ivbye/3KRXADbqJx8JP5sixICcONvCBTWDzvszNa9/NAAAAAWJLR0T/pQfyxQAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+EIDRcXKPqJaAIAAAVFSURBVEjHhVYJWFRVFD5jmASW4TioJaRCjpgogiZqSiAOgyK5jLgwJjWIpYNkuKSImmgkEWUklhoqQZoLuZRLliJooVaalaWtpNFetlj9Z6Kvc+cNPEiT+30zdzn/efcs/z33EqlmaHGNV8trW3lfR1doPr6tr7+hzY1+bfUlYzsTwAD821+O79ARcCnpTTfrCp3AAYG3MDM6/xffpauAOShY/m/VV7uZ0T2kx209Q9G9V1N8bxMjrE94RDCjr9Gz1k9+t/dH5AAaOOgODB7SGB91J6JjhlLsMFjilK/ys8beEK+sGt4dIxJoZCLfNUrHJ4wGxhCNtXHoOJkmjR5vpQkTYVOuGvx5UjLZJwONtrjb9Ld5CtF4pNwjM7sX417yFm8cqTJt4zJPJUqb5rovqUHhfmC6dDNgcsonOwo0nWZaQlPYlkH0AJTCrAc5cEo93tAJyNQUxpLPbGAO5tI8PDQfWBAYmMUhyURDFyIou0FhERankTIJjsAlwMKHEUBLwT28JG2Sm5xlEq4QXu7TYNI4EUr3iORUzMmd+iiH0grlZd5j+fmP948VWQHjCWODwpPASiuR7xiRP+VlKLTgaUoGViVRdlHRFLuCrGY8k9Cg0Dqazc+qQZHIybAUWENF6cBaPZCZNnCu1TPpvA54zq7Lijl4PVH4Yk7c0M+9IjJjbxuwUZuW2NidX00/KfN5D9VKy9g0oHDqhBc2bZZoU4ZovFgkg16y2Rbpt27bPjJta3nMS+AdGv927hLyBu4OllCoD6Y6wCPsVJrFrPhQaAbzywHgurq8eta8EqaixohmqE+2V36slI32yGSvmdVJUYdln+7q/lcPzA95bcvaIFjUR+ODGCnA6zL0zgHSu8w7eGhbj4omvLf7FPUj4+EUjcbOSnCZwocvB3ITyGAwGOmKrcryj/Jj9Vy47Xfj7XS1dlhcyRhyBHDjc8DpA6+KF6ssyJqo+etthmuADzXTDFUm2WSP5i8fTWoOL3ywceQbKv4SH+Q2r1Aq8Vf4zOUutgDNmlR6REt3eQ5cb1Yfa9bpnQ524/fmMB/woXhbHf9fWI0Gg+InIhX+uOBDV8gg1eZOhCEhoWnikqKq8w6dmL7lGFs88RdqBLwlwwwH89vv5OWfrBq0X8f7nUjR+KWRL1Pyu/EUtGQ4HfCQLyy1Hl89URFyGnvoXW520/tdAartTivaFweoKrFTwzsDpN5Wl3fIdEb0kel7OdoBss6UbdWG3oviwtO2Fgwvxhy3Rrf3pX5+oLnijifXaUfUeipFC4FWFAYWBGKOBMKeB8ToIfDNaVQEqhKBksZFAGcq6MNKPvOR7v9ZxlG9zJRE8xL93rFOZjjpNNCLYg+qVi6LBcA5fT8/YJtS367EH5PvJzhJnwKF7bLcV9zCCimVn6FJqcReFRY57ALouexzOKQYf9FxGFATOhghXxKdv4Car+rxUowtftJvQGRijQWu2lopxl9bIFdoJ+f0b9zlvu23qNSrd56n3HNx3L4ZXVFWww76TiVYJaiWlcL67/kHnZ/VQDW574d4rV7hR/opS6OAwR9yZdF2melOR5nY331llakrS+oV4sk6LrpEyYZH84gkarGOs37Wg5wwmrFZ7moHdikjNq1reV66iyrHEcI3MiaHyHXcmMa//MoLfrtIM2q0epVdH8CLMYi+4Ot9TtzvP6oJ8YcEALW/R0WI4X11U41r5OlQa1Z03dEUT3SpUl4Uf/x5TIT608F4KVSj/KSzsy47ioX5nqPyl04SMiTHtQlbNXtfxZUOb/bxVrvnrxrjrT1//gX/cuSTXo0/kAAAAABJRU5ErkJggg=="/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Manage repairs for properties where there is no on-site management or caretaking services.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-red" d="M69.24394,61.43231c0.00525,0.10773,0.01544,0.21344,0.03217,0.31903c0.01471,0.09406,0.0332,0.18567,0.05646,0.27667    c0.02429,0.09442,0.05292,0.18604,0.08612,0.27734c0.03394,0.09351,0.07086,0.18433,0.11359,0.27325    c0.04041,0.08435,0.08508,0.1651,0.13312,0.2453c0.05054,0.08411,0.10315,0.16516,0.16144,0.24365    c0.05725,0.07745,0.11853,0.15027,0.18323,0.22211c0.0636,0.07043,0.12872,0.13763,0.19861,0.20178    c0.07184,0.06604,0.14703,0.12714,0.22571,0.18622c0.07831,0.0589,0.15839,0.11359,0.24237,0.16473    c0.07971,0.04846,0.16144,0.09259,0.24658,0.13385c0.09637,0.04694,0.19476,0.08765,0.29657,0.12421    c0.0462,0.0166,0.08691,0.04181,0.1344,0.05627c0.05707,0.0174,0.11444,0.0249,0.17169,0.03876    c0.05524,0.01331,0.1095,0.02716,0.16583,0.03741c0.18018,0.03308,0.36029,0.05518,0.53845,0.05518    c0.18695,0,0.37042-0.02252,0.55115-0.05664c0.05658-0.01056,0.11035-0.02679,0.16583-0.04053    c0.11987-0.02985,0.23712-0.06519,0.35193-0.10925c0.06201-0.0238,0.12256-0.04858,0.1825-0.07629    c0.10925-0.05035,0.21356-0.10852,0.31604-0.17157c0.04919-0.0304,0.09979-0.0575,0.14703-0.09058    c0.28925-0.20172,0.54346-0.45392,0.74872-0.75043c0.02979-0.04291,0.05322-0.08966,0.08081-0.13416    c0.06537-0.1059,0.12653-0.21442,0.17908-0.3299c0.0296-0.06482,0.05377-0.13141,0.0788-0.19855    c0.02112-0.05682,0.04889-0.10938,0.06677-0.16809l11.13184-36.51465c0.97754-3.16113-0.79395-6.52637-3.94629-7.50146    l-7.05518-2.18872V9.72461c0-3.30566-2.68945-5.99463-5.99512-5.99463H19.49492c-3.30566,0-5.99463,2.68896-5.99463,5.99463    v69.53125c0,2.92657,2.10889,5.36578,4.88611,5.88763l39.72717,10.85846c0.58838,0.18164,1.18359,0.26807,1.76953,0.26807    c2.55713,0,4.9336-1.64893,5.73291-4.21533l2.10803-6.8042h1.51013c3.30566,0,5.99512-2.68896,5.99512-5.99463v-6.42188    c0-1.65674-1.34326-3-3-3s-3,1.34326-3,3l0.00488,6.4165l-49.63019,0.00537l-0.1037-0.02838l-0.00537-69.4975l49.73438-0.00537    v51.5625C69.22929,61.33685,69.24156,61.38318,69.24394,61.43231z M80.49492,23.88623l-5.26563,17.27234v-18.9137    L80.49492,23.88623z M61.44365,85.25049L59.89091,90.271l-18.36206-5.02051H61.44365z"/>
                    <path class="mbs-svg-red" d="M59.33867,35.8999H29.39091c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3h29.94775c1.65674,0,3-1.34326,3-3    S60.9954,35.8999,59.33867,35.8999z"/>
                    <path class="mbs-svg-red" d="M59.33867,46.87207H29.39091c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3h29.94775c1.65674,0,3-1.34326,3-3    S60.9954,46.87207,59.33867,46.87207z"/>
                    <path class="mbs-svg-red" d="M59.33867,57.84424H29.39091c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3h29.94775c1.65674,0,3-1.34326,3-3    S60.9954,57.84424,59.33867,57.84424z"/>
                    <path class="mbs-svg-red" d="M59.33867,24.92773H29.39091c-1.65674,0-3,1.34326-3,3s1.34326,3,3,3h29.94775c1.65674,0,3-1.34326,3-3    S60.9954,24.92773,59.33867,24.92773z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Schedule meetings with residents, owners and/or body corporate, circulate agenda and minutes.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg id="Layer_1" style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path class="mbs-svg-red" d="M23,12h-1v-1c0-4.5-2.9-8.3-7-9.5V1c0-0.6-0.4-1-1-1c-0.6,0-1,0.4-1,1v6c0,0.6,0.4,1,1,1c0.6,0,1-0.4,1-1V3.6  c2.9,1.2,5,4.1,5,7.4v1H4v-1c0-3.3,2.1-6.2,5-7.4V7c0,0.6,0.4,1,1,1c0.6,0,1-0.4,1-1V1c0-0.6-0.4-1-1-1C9.4,0,9,0.4,9,1v0.5  C4.9,2.7,2,6.5,2,11v1H1c-0.6,0-1,0.4-1,1s0.4,1,1,1h22c0.6,0,1-0.4,1-1S23.6,12,23,12z M12,22c-3.5,0-6.4-2.6-6.9-6h-2  c0.5,4.5,4.3,8,8.9,8c4.6,0,8.4-3.5,8.9-8h-2C18.4,19.4,15.5,22,12,22z M11,15H8v2h1C10.1,17,11,16.1,11,15z M16,17v-2h-3  c0,1.1,0.9,2,2,2H16z"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Manage contractors and suppliers, organize budgeting, issue work orders and process invoices.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 512 512" id="Layer_12" version="1.1" viewBox="0 0 512 512" xml:space="preserve">
                <path class="mbs-svg-red" clip-rule="evenodd" d="M512,303.844c0,44.18-35.82,80-80,80h-32c-8.844,0-16-7.156-16-16  s7.156-16,16-16h32c26.516,0,48-21.492,48-48s-21.484-48-48-48h-0.25c-20.812,0-38.539-13.25-45.188-31.766  c-0.047-0.117-0.156-0.234-0.195-0.352c-12.492-35.078-44.727-60.758-83.398-63.531c-8.359,0.539-14.969,7.406-14.969,15.906  v191.742c0,8.844-7.156,16-16,16s-16-7.156-16-16V176.102c0-26.32,21.156-47.617,47.398-47.945  c52.242,2.984,95.961,37.297,112.891,84.438c0.078,0.078,0.234,0.156,0.258,0.234c2.094,6.398,8.102,11.016,15.203,11.016H432  c0.891,0,1.766,0.102,2.656,0.141C434.812,224,435.047,224,435.18,224C477.891,225.688,512,260.734,512,303.844L512,303.844z   M208,383.844c-8.844,0-16-7.156-16-16v-176c0-8.844,7.156-16,16-16s16,7.156,16,16v176C224,376.688,216.844,383.844,208,383.844  L208,383.844z M144,383.844c-8.844,0-16-7.156-16-16V183.836c0-8.828,7.156-16,16-16s16,7.172,16,16v184.008  C160,376.688,152.844,383.844,144,383.844L144,383.844z M80,383.844c-8.844,0-16-7.156-16-16v-128c0-8.844,7.156-16,16-16  s16,7.156,16,16v128C96,376.688,88.844,383.844,80,383.844L80,383.844z M16,351.844c-8.844,0-16-7.156-16-16v-64  c0-8.844,7.156-16,16-16s16,7.156,16,16v64C32,344.688,24.844,351.844,16,351.844L16,351.844z M336,351.844c8.844,0,16,7.156,16,16  s-7.156,16-16,16s-16-7.156-16-16S327.156,351.844,336,351.844L336,351.844z"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Track facility performance through real-time cloudSense monitoring, reports - and much more.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-red" d="M87.95965,79.85107c-9.89453,0.14258-26.50195,3.43359-27.2041,3.57324c-0.10498,0.02148-0.20898,0.04785-0.31152,0.08008    c-13.01465,4.08496-23.9458,3.69043-32.14551-1.21582c-11.51953-6.9209-13.25195-20.12598-13.27539-20.31445    c-0.08057-0.5957,0.07422-1.16406,0.4541-1.65039c0.43457-0.56055,1.05469-0.90527,1.81738-0.99902    c1.40186-0.19238,2.69629,0.74805,2.87939,2.08105c1.21484,8.83203,7.41992,15.58105,16.59863,18.05176    c3.15234,0.85059,11.89697,0.82422,14.5918-1.20898c0.93164-0.70313,1.4541-1.69141,1.4707-2.7832    c0.01465-0.96582-0.375-1.87305-1.09668-2.55371c-0.65869-0.62012-2.40869-2.26855-13.94727-1.41992    c-1.44092,0.09473-2.6958-0.88086-2.80322-2.21387c-0.05029-0.58887,0.14307-1.17285,0.54102-1.63965    c0.4624-0.54102,1.09863-0.85547,1.83984-0.91016c1.44189-0.10742,2.68457-0.2041,3.78809-0.29004    c4.41113-0.34277,6.42529-0.49609,10.05176-0.45508c0.05225-0.00098,0.14453-0.00195,0.19873-0.00488l0.21973-0.00098    c0.17432,0,0.38037,0,0.53809-0.00586c10.02051,0.03418,11.98681,2.77051,12.93164,4.08594    c0.55908,0.77734,1.59961,2.2334,3.57471,2.19238c0.17822-0.00391,0.35596-0.02246,0.53076-0.05762l8.57373-1.69824    c1.18604-0.17969,2.19141-0.37891,3.16504-0.57129c2.09912-0.41602,3.91211-0.77539,7.14941-0.87012    c1.65576-0.04883,2.95947-1.43066,2.91064-3.08691c-0.04785-1.65527-1.396-2.93652-3.08643-2.91113    c-3.7373,0.10938-5.97559,0.55371-8.14014,0.98242c-0.96729,0.19141-1.88086,0.37305-2.9585,0.53418    c-0.0459,0.00684-0.09229,0.01465-0.13818,0.02441l-7.12061,1.41016c-2.38232-3.04883-6.50097-5.99707-17.46289-6.0332    c-0.16895,0.00488-0.31152,0.00488-0.43359,0.00488c-0.17676-0.00195-0.32666,0.00098-0.4585,0.00488    c-3.8457-0.04004-6.08496,0.12988-10.51074,0.47461c-1.09717,0.08496-2.33252,0.18164-3.76709,0.28711    c-2.33789,0.1748-4.45264,1.2373-5.95947,2.99902c-1.00586,1.18164-1.64844,2.57813-1.88428,4.05176    c-1.56934-2.03613-2.58643-4.46973-2.96289-7.20801c-0.6333-4.59082-4.94043-7.82129-9.58398-7.21387    c-2.32471,0.28809-4.38379,1.4502-5.79199,3.2666C9.36981,58.396,8.77899,60.57764,9.07,62.71924    c0.01855,0.16211,2.01514,16.22656,16.14307,24.71484c5.40869,3.23633,11.72461,4.87793,18.77197,4.87793    c5.55176,0,11.64111-1.02051,18.1001-3.03418c5.83447-1.15723,18.54492-3.32129,25.96045-3.42676    c1.65674-0.02441,2.98047-1.38672,2.95654-3.04297C90.97869,81.15088,89.61053,79.81299,87.95965,79.85107z"/>
                    <path class="mbs-svg-red" d="M53.75781,41.00128c-1.13306,0.84467-2.60602,1.26697-4.41888,1.26697c-1.31848,0-2.38251-0.20911-3.19318-0.6283    c-0.81061-0.41925-1.43896-0.95074-1.88495-1.59662c-0.44598-0.64581-0.86212-1.44922-1.24634-2.41028    c-0.31622-0.81067-0.69733-1.42145-1.14337-1.8335c-0.44598-0.41199-0.99194-0.61798-1.63776-0.61798    c-0.7962,0-1.45233,0.26471-1.96741,0.79309c-0.51501,0.52844-0.77252,1.16394-0.77252,1.90558    c0,1.27728,0.42957,2.59265,1.2876,3.94507c0.85797,1.35248,1.97455,2.43402,3.3476,3.24463    c1.23468,0.71783,2.70581,1.18085,4.36475,1.44116v4.76282c0,1.65723,1.34326,3,3,3s3-1.34277,3-3v-4.75641    c1.25879-0.20166,2.42035-0.52246,3.45844-0.99432c1.81287-0.82404,3.19006-1.96429,4.13049-3.41974    s1.41113-3.09735,1.41113-4.92358c0-1.52448-0.27087-2.81207-0.81372-3.86267c-0.54285-1.05066-1.29785-1.91901-2.26611-2.60602    c-0.9682-0.68707-2.14246-1.27008-3.52271-1.7511c-1.38031-0.48102-2.92224-0.9198-4.62494-1.31842    c-1.35962-0.34302-2.33508-0.60468-2.92529-0.78284c-0.59021-0.17822-1.17426-0.42542-1.7511-0.74164    c-0.57678-0.31622-1.03003-0.69324-1.35962-1.13306c-0.32965-0.43982-0.49445-0.961-0.49445-1.56567    c0-0.97546,0.48413-1.80975,1.45239-2.50299c0.9682-0.69324,2.24237-1.04034,3.82141-1.04034    c1.7027,0,2.93872,0.31934,3.70819,0.95795c0.76941,0.63861,1.42865,1.52753,1.97766,2.66779    c0.42542,0.79626,0.82098,1.37,1.18457,1.72021c0.36359,0.35016,0.89612,0.52527,1.59656,0.52527    c0.76941,0,1.41113-0.2915,1.92615-0.87549c0.51508-0.58405,0.77258-1.23914,0.77258-1.96741    c0-0.7962-0.20605-1.61407-0.61804-2.45148c-0.41205-0.83746-1.06403-1.63782-1.95709-2.40002s-2.01581-1.37305-3.36823-1.8335    c-0.53613-0.1825-1.12317-0.31824-1.73828-0.42834v-5.10211c0-1.65723-1.34326-3-3-3s-3,1.34277-3,3v5.06    c-1.13171,0.17157-2.18085,0.43805-3.12866,0.82068c-1.7171,0.69324-3.02832,1.68622-3.93475,2.97687    c-0.90649,1.29059-1.35968,2.7677-1.35968,4.42914c0,1.7439,0.4295,3.20343,1.28754,4.37769s2.01892,2.10132,3.48157,2.78113    s3.27863,1.27417,5.44891,1.78198c1.62024,0.37079,2.91504,0.72101,3.88324,1.05066    c0.96826,0.32959,1.7583,0.80652,2.36914,1.43176c0.61078,0.62524,0.91675,1.43896,0.91675,2.44116    C55.45739,39.10291,54.89087,40.15668,53.75781,41.00128z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Achieve more gains on property value and lessen insurance claims through smarter, efficient building.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-insight-section-div">
          <p class="mbs-insight-section-text insight-section-text-big">User Insight</p>
          <p class="mbs-insight-section-text insight-section-text-small">“Using MYBOS has assisted us in management of our buildings, residents, security and work orders.It is an effective tool to ensure compliance matters, maintenance schedules and work history. One of the best programs I have ever used.” </p>
          <p class="mbs-insight-section-text insight-section-text-big" style="margin:0;">Stella McWiggan</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;padding-bottom:1px;margin:0;">Senior Licenced Strata Manager</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;">Premium Strata</p>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-2" style="background-color: #ffffff;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_property_top.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_property_top_mobile.jpg') }}" />
            </div>
        </div>
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_property_under.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_property_under_mobile.jpg') }}" />
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-product-overview-undertext">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-product-overview-undertext-text">FOR A FULL LIST OF FEATURES <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CONTACT US</a> TODAY</h3>
                <h3 class="mbs-product-overview-undertext-text">OR <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CLICK HERE</a> TO DOWNLOAD OUR BROCHURES</h3>
            </div>
        </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-3" style="background-color: #ebebeb;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Additional Pluses</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/Rectangle_imac-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div"  style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/cloud_product-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div">
                <p style="margin:0;font-size: 18px;">Cloud-based system guarantees regular back-up, maximum service uptime and no additional hardware cost to the subscriber</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/productpage-mybos-svgimg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div" style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="mbs-section__container container" style="padding-bottom:100px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse" data-animation="zoomIn"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Request A Demo</a></div>
            </div>
        </div>
    </div>
  </section>
@endsection
