@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/company_support_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">SUPPORT</h1>
                    <p class="mbs-software-backround__subtext">HOW CAN WE HELP YOU ?</p>
                    <div style="width:100%;text-align:center;position:relative;">
                      <input class="mbs-company-support-searchtextinput" type="text" />
                      <i class="fa fa-search mbs-company-support-searchtextinput-searchbutton"></i>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#edf3f3;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>COMPANY</label><label class="path-eclips"></label><label>SUPPORT</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
              <p style="color:#00aeef;font-family:Roboto-regular;font-size:30px;padding-bottom:30px;">FAQ</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">How do I clear my browser's cache and cookies?</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">How to edit a Management Report?</p>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
              <p style="color:#00aeef;font-family:Roboto-regular;font-size:30px;padding-bottom:30px;">Knowledgebase</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">How to edit a Management Report?</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">Setting up SPF record for MYBOS to send email on behalf of your email domain</p>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
              <p style="color:#00aeef;font-family:Roboto-regular;font-size:30px;padding-bottom:30px;">Tutorials</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">Tutorial 1</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">Tutorial 2</p>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
              <p style="color:#00aeef;font-family:Roboto-regular;font-size:30px;padding-bottom:30px;">Contact Tech Support</p>
              <p style="color:#5c5c5b;font-family:Roboto-regular;font-size:18px;">1300 912 386</p>
            </div>
        </div>
    </div>
  </section>
@endsection
