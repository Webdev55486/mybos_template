@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/news_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text" style="font-family: lato-bold;">NEWS</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#edf3f3;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>NEWS</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
          <div class=" col-sm-8 col-sm-offset-2">
            <div class="col-sm-4 mbs-news-radiobutton">
              <input type="radio" id="news1" name="radio-group">
              <label for="news1" style="font-family:roboto-light;">MYBOS Related News</label>
            </div>
            <div class="col-sm-4 mbs-news-radiobutton">
              <input type="radio" id="news2" name="radio-group">
              <label for="news2" style="font-family:roboto-light;">Industry Related News</label>
            </div>
            <div class="col-sm-4 mbs-news-radiobutton">
              <input type="radio" id="news3" name="radio-group" checked>
              <label for="news3" style="font-family:roboto-light;">All News Articles</label>
            </div>
          </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <div class="mbs-news-container__div" style="background-color: #ffffff;">
            <div class="mbs-header mbs-header--wysiwyg row">
              <p style="font-family: Roboto-regular;color: #0072bc;font-weight:bold;margin-left:30px;">Featured</p>
              <div class="col-md-2" style="text-align:center;">
                <img src="{{ cdn('assets/images/newspage_newimg_1.jpg') }}" />
              </div>
              <div class="col-md-10" style="padding-left:30px;">
                <p style="font-family:lato-medium;font-weight:bold;margin:0;font-size:17px;white-space:nowrap;width:80%;text-overflow: ellipsis;overflow: hidden;">The changing face of facilities management and the 3 game ...</p>
                <p style="font-family:lato-regular;font-weight:bold;margin:0;">The Fifth Estate -13 Mar. 2017</p>
                <p style="font-family:lato-light;font-weight:bold;margin:0;">Sustainability, procurement and thermal comfort are three of the major trends changing the shape of the facilities management industry...</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first" >
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <div class="mbs-news-container__div">
            <div class="mbs-header mbs-header--wysiwyg row">
              <div class="col-md-2" style="text-align:center;">
                <img src="{{ cdn('assets/images/newspage_newimg_2.jpg') }}" />
              </div>
              <div class="col-md-10" style="padding-left:30px;">
                <p style="font-family:lato-medium;font-weight:bold;margin:0;font-size:17px;white-space:nowrap;width:80%;text-overflow: ellipsis;overflow: hidden;">Contractors Appointed to Fusion21's £290 Million Compliance and ...</p>
                <p style="font-family:lato-regular;font-weight:bold;margin:0;">Bdaily-14 hours ago</p>
                <p style="font-family:lato-light;font-weight:bold;margin:0;">Fusion21 has announced the contractors successfully appointed to its national Compliance and Facilities Management framework, worth up to ..</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first" >
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <div class="mbs-news-container__div">
            <div class="mbs-header mbs-header--wysiwyg row">
              <div class="col-md-2" style="text-align:center;">
                <img src="{{ cdn('assets/images/newspage_newimg_3.jpg') }}" />
              </div>
              <div class="col-md-10" style="padding-left:30px;">
                <p style="font-family:lato-medium;font-weight:bold;margin:0;font-size:17px;white-space:nowrap;width:80%;text-overflow: ellipsis;overflow: hidden;">Need of the hour: smart facilities management</p>
                <p style="font-family:lato-regular;font-weight:bold;margin:0;">gulfnews.com-7 Mar. 2017</p>
                <p style="font-family:lato-light;font-weight:bold;margin:0;">In fact, there is already great demand for facilities management (FM) services in the UAE, according to Jamal Abdullah Lootah, CEO of Imdaad, ...</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first" >
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-sm-8 col-sm-offset-2">
          <div class="mbs-news-container__div">
            <div class="mbs-header mbs-header--wysiwyg row">
              <div class="col-md-2" style="text-align:center;">
                <img src="{{ cdn('assets/images/newspage_newimg_2.jpg') }}" />
              </div>
              <div class="col-md-10" style="padding-left:30px;">
                <p style="font-family:lato-medium;font-weight:bold;margin:0;font-size:17px;white-space:nowrap;width:80%;text-overflow: ellipsis;overflow: hidden;">Contractors Appointed to Fusion21's £290 Million Compliance and ...</p>
                <p style="font-family:lato-regular;font-weight:bold;margin:0;">Bdaily-14 hours ago</p>
                <p style="font-family:lato-light;font-weight:bold;margin:0;">Fusion21 has announced the contractors successfully appointed to its national Compliance and Facilities Management framework, worth up to ..</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
