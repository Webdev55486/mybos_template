@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/industy-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/facility_management_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text">EMPOWER YOURSELF</h1>
                    <p class="mbs-software-backround__subtext">WITH TOTAL CONTROL OVER YOUR FACILITY AND SEAMLESS INTERACTION WITH YOUR COMMUNITY AND SUPPLIERS</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ebebeb;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>INDUSTRIES</label><label class="path-eclips"></label><label>BUIDING & FACILITY MANAGEMENT</label>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">A Whole Range Of Sensors Available</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-facility-section-div">
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve" >
                <g id="Layer_1">
                  <path class="mbs-svg-orange" d="M92.496521,33.06134c-0.000427-0.02655-0.00293-0.052429-0.004028-0.078796   c-0.070557-8.20459-6.866211-14.859497-15.217102-14.859497H63.842651c0.002747-0.052856,0.015747-0.102722,0.015747-0.15625   V16.70459c0-4.960449-4.035156-8.996094-8.996094-8.996094h-9.78418c-4.960938,0-8.996094,4.035645-8.996094,8.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H22.666016c-8.307922,0-15.069214,6.637817-15.15686,14.82959   c-0.002258,0.042786-0.006226,0.084656-0.006592,0.127686c0,0.01062-0.001587,0.020874-0.001587,0.031494l0.029297,41.301758   c0,3.710327,2.461121,6.861511,5.860901,7.975281c0.330811,5.591125,6.010376,9.902649,13.222107,9.902649h46.713867   c7.213135,0,12.893494-4.313232,13.22229-9.905884c3.383484-1.116211,5.831421-4.263367,5.831421-7.963257l0.118164-41.3125   C92.499023,33.093445,92.496582,33.077698,92.496521,33.06134z M42.082031,17.966797V16.70459   c0-1.651855,1.34375-2.996094,2.996094-2.996094h9.78418c1.652344,0,2.996094,1.344238,2.996094,2.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H42.066284C42.069031,18.07019,42.082031,18.020325,42.082031,17.966797z    M22.666016,24.123047h54.609375c4.915283,0,9.070679,3.978882,9.210449,8.733398   c-1.340881,7.173462-4.711609,13.725525-9.775879,18.958008c-1.151367,1.190918-1.120117,3.090332,0.070313,4.242188   c0.582031,0.563965,1.333984,0.844238,2.085938,0.844238c0.78418,0,1.567383-0.305664,2.15625-0.913574   c2.057556-2.126526,3.869507-4.443542,5.430847-6.90918l-0.072449,25.335449c0,1.306641-1.158203,2.410156-2.529297,2.410156   H60.384766H16.088867c-1.363281,0-2.558594-1.126465-2.558594-2.412109l-0.018005-25.390076   c7.704041,12.219727,21.28595,20.121033,36.486755,20.121033c6.727539,0,13.444336-1.594238,19.424805-4.609863   c1.479492-0.746094,2.074219-2.550293,1.328125-4.029297c-0.746094-1.47998-2.551758-2.07373-4.029297-1.328125   c-2.703491,1.363281-5.584839,2.37915-8.54895,3.045837c0.201599-0.726685,0.317749-1.488953,0.317749-2.279846   c0-4.706116-3.815063-8.521118-8.521118-8.521118s-8.521057,3.815002-8.521057,8.521118   c0,0.79248,0.116516,1.556335,0.318909,2.284302C27.581909,59.01532,16.293762,47.665283,13.512939,32.878418   C13.639771,28.030396,17.691956,24.123047,22.666016,24.123047z M49.970337,62.781616   c-1.568665,0-2.840332-1.271667-2.840332-2.840332c0-1.568726,1.271667-2.840393,2.840332-2.840393   c1.568726,0,2.840393,1.271667,2.840393,2.840393C52.81073,61.509949,51.539063,62.781616,49.970337,62.781616z    M73.327148,86.291504H26.613281c-3.633606,0-6.323853-1.681396-7.051086-3.467773h60.81604   C79.651001,84.610107,76.960754,86.291504,73.327148,86.291504z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Take charge of your cases linking together residents, you and the contractors.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 512 512" id="Layer_12" version="1.1" viewBox="0 0 512 512" xml:space="preserve">
                <path class="mbs-svg-orange" clip-rule="evenodd" d="M512,303.844c0,44.18-35.82,80-80,80h-32c-8.844,0-16-7.156-16-16  s7.156-16,16-16h32c26.516,0,48-21.492,48-48s-21.484-48-48-48h-0.25c-20.812,0-38.539-13.25-45.188-31.766  c-0.047-0.117-0.156-0.234-0.195-0.352c-12.492-35.078-44.727-60.758-83.398-63.531c-8.359,0.539-14.969,7.406-14.969,15.906  v191.742c0,8.844-7.156,16-16,16s-16-7.156-16-16V176.102c0-26.32,21.156-47.617,47.398-47.945  c52.242,2.984,95.961,37.297,112.891,84.438c0.078,0.078,0.234,0.156,0.258,0.234c2.094,6.398,8.102,11.016,15.203,11.016H432  c0.891,0,1.766,0.102,2.656,0.141C434.812,224,435.047,224,435.18,224C477.891,225.688,512,260.734,512,303.844L512,303.844z   M208,383.844c-8.844,0-16-7.156-16-16v-176c0-8.844,7.156-16,16-16s16,7.156,16,16v176C224,376.688,216.844,383.844,208,383.844  L208,383.844z M144,383.844c-8.844,0-16-7.156-16-16V183.836c0-8.828,7.156-16,16-16s16,7.172,16,16v184.008  C160,376.688,152.844,383.844,144,383.844L144,383.844z M80,383.844c-8.844,0-16-7.156-16-16v-128c0-8.844,7.156-16,16-16  s16,7.156,16,16v128C96,376.688,88.844,383.844,80,383.844L80,383.844z M16,351.844c-8.844,0-16-7.156-16-16v-64  c0-8.844,7.156-16,16-16s16,7.156,16,16v64C32,344.688,24.844,351.844,16,351.844L16,351.844z M336,351.844c8.844,0,16,7.156,16,16  s-7.156,16-16,16s-16-7.156-16-16S327.156,351.844,336,351.844L336,351.844z"/>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Monitor assets, manage inventories and record inspections.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 40 40" id="Слой_1" version="1.1" viewBox="0 0 40 40" xml:space="preserve">
                <g>
                  <path class="mbs-svg-orange" d="M9.4,29.8c-2.4,0-4.9-0.9-6.7-2.8c-3.7-3.7-3.7-9.8,0-13.5c3.3-3.3,8.5-3.7,12.3-1c0.4,0.3,0.5,1,0.2,1.4   c-0.3,0.4-0.9,0.5-1.4,0.2c-3-2.2-7.1-1.9-9.7,0.8c-2.9,2.9-2.9,7.7,0,10.6c2.9,2.9,7.7,2.9,10.6,0c0.1-0.1,0.2-0.2,0.3-0.4   c0.4-0.4,1-0.5,1.4-0.1c0.4,0.4,0.5,1,0.1,1.4c-0.1,0.2-0.3,0.3-0.4,0.4C14.2,28.9,11.8,29.8,9.4,29.8z"/>
                </g>
                <g>
                  <path class="mbs-svg-orange" d="M9.4,24.6c-1.1,0-2.2-0.4-3-1.3c-0.8-0.8-1.3-1.9-1.3-3c0-1.1,0.4-2.2,1.3-3c0.8-0.8,1.9-1.3,3-1.3c1.1,0,2.2,0.4,3,1.3   c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-1,0.4-1.4,0c-0.9-0.9-2.4-0.9-3.2,0c-0.4,0.4-0.7,1-0.7,1.6c0,0.6,0.2,1.2,0.7,1.6   c0.9,0.9,2.4,0.9,3.2,0c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4C11.6,24.2,10.5,24.6,9.4,24.6z"/>
                </g>
                <g>
                  <path class="mbs-svg-orange" d="M36,25.8c-1.4,0-2.1-0.8-2.6-1.3c-0.5-0.5-0.7-0.7-1.2-0.7c-0.5,0-0.7,0.2-1.2,0.7c-0.5,0.6-1.2,1.3-2.6,1.3   c-1.4,0-2.1-0.8-2.6-1.3c-0.5-0.5-0.7-0.7-1.2-0.7s-0.7,0.2-1.2,0.7c-0.5,0.6-1.2,1.3-2.6,1.3c-1.4,0-2.1-0.8-2.6-1.3   c-0.4-0.4-0.6-0.6-0.9-0.7c-0.5-0.1-0.9-0.7-0.8-1.2s0.7-0.9,1.2-0.8c0.9,0.2,1.5,0.8,1.9,1.2c0.5,0.5,0.7,0.7,1.2,0.7   c0.5,0,0.7-0.2,1.2-0.7c0.5-0.6,1.2-1.3,2.6-1.3c1.4,0,2.1,0.8,2.6,1.3c0.5,0.5,0.7,0.7,1.2,0.7c0.5,0,0.7-0.2,1.2-0.7   c0.5-0.6,1.2-1.3,2.6-1.3c1.4,0,2.1,0.8,2.6,1.3c0.3,0.3,0.5,0.5,0.7,0.6l2.3-3.4l-2.3-3.5H16.6c-0.6,0-1-0.4-1-1s0.4-1,1-1H36   c0.3,0,0.6,0.2,0.8,0.4l3,4.5c0.2,0.3,0.2,0.8,0,1.1l-3,4.5C36.6,25.7,36.3,25.8,36,25.8z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Sign keys in and out, track overdue keys, manage visitor and contractors.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <path class="mbs-svg-orange" d="M75,28.33325h-4.8161C71.94159,26.10553,73,23.30151,73,20.25024c0-7.21436-5.86914-13.0835-13.08301-13.0835   c-5.09375,0-9.50568,2.93237-11.66589,7.19171c-1.69574-1.19147-3.75616-1.89728-5.98157-1.89728   c-5.75439,0-10.43604,4.68164-10.43604,10.43604c0,1.99292,0.57147,3.8504,1.54462,5.43604H25c-7.99512,0-14.5,6.50488-14.5,14.5   v8.08008c0,1.65674,1.34326,3,3,3h1v35.91992c0,1.65674,1.34326,3,3,3h65c1.65674,0,3-1.34326,3-3V62.9104c0-1.65674-1.34326-3-3-3   s-3,1.34326-3,3v23.92285h-20V53.91333h27c1.65674,0,3-1.34326,3-3v-8.08008C89.5,34.83813,82.99512,28.33325,75,28.33325z    M52.8335,20.25024c0-3.90576,3.17773-7.0835,7.0835-7.0835S67,16.34448,67,20.25024s-3.17725,7.08301-7.08301,7.08301h-7.0835   V20.25024z M42.26953,18.46118c2.44629,0,4.43652,1.99023,4.43652,4.43604v4.43604h-4.43652   c-2.4458,0-4.43604-1.99023-4.43604-4.43604S39.82373,18.46118,42.26953,18.46118z M16.5,42.83325c0-4.68701,3.81299-8.5,8.5-8.5   h15.5v13.58008h-24V42.83325z M20.5,53.91333h20v32.91992h-20V53.91333z M46.5,86.83325v-52.5h7v52.5H46.5z M83.5,47.91333h-24   V34.33325H75c4.68701,0,8.5,3.81299,8.5,8.5V47.91333z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Send parcel notifications and track handing over and delivery.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg style="enable-background:new 0 0 100 100;" version="1.1" viewBox="0 0 100 100" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Layer_2"/>
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-orange" d="M90.97687,55.19925c-0.62646-1.53418-2.37646-2.27002-3.91162-1.64307c-1.53369,0.62646-2.26953,2.37744-1.64307,3.91162    c0.4292,1.05127,0.14844,1.85205-0.05322,1.9668l-3.96149-0.30878l-18.99603-46.5094l2.62353-3.00848    c0.22461-0.03662,0.979,0.34082,1.40771,1.3916c0.62695,1.5332,2.37842,2.26709,3.91211,1.64258    c1.53369-0.62695,2.26904-2.37793,1.64258-3.91211c-1.09619-2.68311-3.3584-4.61279-5.9043-5.03613    c-2.13476-0.35693-4.17089,0.37109-5.58545,1.99316L40.30341,28.83994l-20.56824,8.40082    c-9.34131,3.81543-13.8374,14.52002-10.02246,23.86182c1.84863,4.52539,5.34863,8.06006,9.85498,9.95313    c2.28174,0.95801,4.68311,1.4375,7.08545,1.4375c1.86475,0,3.72784-0.30176,5.53809-0.87988    c0.013,0.03558,0.01727,0.07208,0.03174,0.10742l7.77734,19.04053c0.91406,2.23828,2.64453,3.98682,4.87402,4.92285    c1.12793,0.47412,2.31543,0.71094,3.50342,0.71094c1.15918,0,2.31836-0.22559,3.42383-0.67676    c4.61963-1.88721,6.84277-7.18115,4.95605-11.80078l-4.99414-12.22705c-0.62695-1.53418-2.37842-2.26855-3.91162-1.64307    c-1.53418,0.62646-2.26953,2.37793-1.64307,3.91162l4.99414,12.22705c0.63623,1.55713-0.11328,3.34131-1.67041,3.97754    c-0.75439,0.30908-1.5835,0.3042-2.33496-0.01123S45.8631,89.2476,45.555,88.4932l-7.77734-19.04102    c-0.00537-0.01306-0.01398-0.02368-0.01947-0.03662l15.77838-6.4444l31.36609,2.44489    c0.16895,0.01318,0.33643,0.02002,0.50244,0.02002c1.94775,0,3.70654-0.89648,4.88184-2.50684    C91.80841,60.84476,92.07257,57.88285,90.97687,55.19925z M74.71313,58.60404l-17.44342-1.35968L46.4964,30.86661l11.5011-13.1886    L74.71313,58.60404z M21.89142,65.52348c-0.24792-0.10413-0.48389-0.22705-0.72253-0.34576l16.70056-6.82123    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62646-1.53418-2.37988-2.26758-3.91162-1.64307l-19.36206,7.90833    c-0.37012-0.59338-0.7016-1.21582-0.97144-1.87659c-0.27179-0.66541-0.46753-1.34204-0.61658-2.02142L34.0091,48.9058    c1.53369-0.62646,2.26953-2.37793,1.64307-3.91162c-0.62695-1.53418-2.37842-2.26904-3.91162-1.64307l-16.69421,6.81824    c1.11835-3.22827,3.55353-5.98364,6.9574-7.3739l19.25922-7.86627c0.10065,0.01013,0.20093,0.02643,0.30182,0.02643    c0.03876,0,0.07678-0.01056,0.11548-0.01202L50.9173,57.5596l-19.61182,8.01026    C28.26349,66.81303,24.91974,66.79545,21.89142,65.52348z"/>
                    <path class="mbs-svg-orange" d="M69.50226,19.39896l12.09961,29.62451c0.2688,0.65771,0.74786,1.16174,1.32422,1.48151    c0.18164,0.14124,0.37665,0.2691,0.59521,0.36859c0.40332,0.18359,0.82568,0.27051,1.24121,0.27051    c1.13965,0,2.22949-0.65332,2.73242-1.75781c2.87354-6.3125,3.01758-13.61523,0.39551-20.03662    c-2.62256-6.4209-7.83838-11.53467-14.31006-14.03027c-0.99597-0.38507-2.06665-0.19604-2.86493,0.3963    C69.48425,16.47007,68.93854,18.01883,69.50226,19.39896z M82.33575,31.6182c0.88116,2.15778,1.32825,4.44666,1.3927,6.74341    l-5.11902-12.53339C80.17145,27.51328,81.45459,29.46061,82.33575,31.6182z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Easily roadcast messages to residents, owners, strata members or contractors.</p>
            </div>
          </div>
          <div class="col-sm-4 mbs-facility-section-subdiv">
            <div class="mbs-msnsgement-svg-container">
              <svg enable-background="new 0 0 100 100" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <g>
                    <path class="mbs-svg-orange" d="M91.5,85.16949h-4.83008v-8.01855c0-1.65723-1.34277-3-3-3s-3,1.34277-3,3v8.01855h-3.15039V37.12067h3.15039v26.03027    c0,1.65723,1.34277,3,3,3s3-1.34277,3-3V36.8609c0-3.16504-2.5791-5.74023-5.75-5.74023h-3.66016    c-3.16504,0-5.74023,2.5752-5.74023,5.74023v48.30859h-4.46875V56.19196c0-3.16797-2.57813-5.74609-5.74609-5.74609h-3.66016    c-3.16797,0-5.74512,2.57813-5.74512,5.74609v28.97754h-4.46533V52.37067c0-3.16797-2.57715-5.74609-5.74512-5.74609h-3.66064    c-3.16797,0-5.74561,2.57813-5.74561,5.74609v32.79883h-4.46484V67.78375c0-3.16797-2.57715-5.74609-5.74512-5.74609h-3.66064    c-3.16797,0-5.74561,2.57813-5.74561,5.74609v17.38574H8.5c-1.65674,0-3,1.34277-3,3s1.34326,3,3,3h83c1.65723,0,3-1.34277,3-3    S93.15723,85.16949,91.5,85.16949z M57.89941,56.44586h3.15137v28.72363h-3.15137V56.44586z M38.28271,52.62457h3.15137v32.54492    h-3.15137V52.62457z M18.6665,68.03766h3.15137v17.13184H18.6665V68.03766z"/>
                    <path class="mbs-svg-orange" d="M20.23999,58.83051c3.31,0,6-2.68994,6-6c0-0.56-0.08002-1.09998-0.21997-1.60999L38.37,38.64056    c0.47998,0.12994,0.97003,0.18994,1.48999,0.18994c1.38,0,2.66003-0.46997,3.67004-1.25995l10,4.07996    c0.39996,2.92004,2.91998,5.17999,5.93994,5.17999c3.31006,0,6-2.68994,6-6c0-0.82996-0.16998-1.60999-0.46997-2.32996    l13.35999-17.72003c0.23999,0.03003,0.48004,0.04999,0.73004,0.04999c3.31,0,6-2.68994,6-6c0-3.31-2.69-6-6-6s-6,2.69-6,6    c0,0.83002,0.16998,1.62006,0.47998,2.34003L60.21002,34.88055c-0.24005-0.03003-0.49005-0.05005-0.74005-0.05005    c-1.37994,0-2.64996,0.47003-3.66998,1.26001l-10-4.07996c-0.39996-2.92004-2.90997-5.18005-5.94-5.18005c-3.31,0-6,2.69-6,6    c0,0.56006,0.08002,1.10004,0.22003,1.61005L21.73999,47.02051c-0.47998-0.12-0.97998-0.19-1.5-0.19c-3.31,0-6,2.69-6,6    C14.23999,56.14056,16.92999,58.83051,20.23999,58.83051z M76.70001,13.02051c0.04999-0.06995,0.09998-0.12994,0.15997-0.19    c0.03003-0.03998,0.07001-0.07996,0.10999-0.12c0.24005-0.25,0.53003-0.44995,0.85004-0.59998    c0.08997-0.03998,0.17999-0.08002,0.27002-0.10999c0.09998-0.04004,0.20996-0.07001,0.31995-0.09003    c0.08002-0.01996,0.17004-0.03998,0.25-0.03998c0.14001-0.03003,0.28003-0.04004,0.43005-0.04004    c0.14996,0,0.28998,0.01001,0.44,0.04004c0.00995-0.01001,0.01996,0,0.03998,0c0.15997,0.02002,0.31,0.06,0.46002,0.10999    c0.15997,0.04999,0.31,0.10999,0.44995,0.19c0.01001,0.01001,0.03003,0.01001,0.04004,0.02002    c0.13,0.07996,0.26001,0.15997,0.38,0.25c0.08997,0.07001,0.17999,0.14996,0.26996,0.22998    c0.09003,0.08002,0.17004,0.16998,0.25,0.27002c0.07001,0.08997,0.14001,0.17999,0.20001,0.27997    c0.07001,0.10004,0.12,0.20001,0.16998,0.29999c0.05005,0.10004,0.10004,0.21002,0.13,0.32001    c0.04004,0.10004,0.07001,0.21002,0.09003,0.32001c0.02997,0.09003,0.03998,0.19,0.04999,0.28003    c0.02002,0.13,0.03003,0.26001,0.03003,0.38995v0.04004c0,0.13-0.01001,0.25-0.04004,0.38c0.01001,0.02997,0,0.06-0.01001,0.08997    c-0.01996,0.15002-0.04999,0.29004-0.09998,0.42999c-0.04999,0.16003-0.10999,0.31-0.19,0.45001    c-0.01001,0.01001-0.01001,0.03003-0.02002,0.04004c-0.06,0.12-0.13,0.22998-0.20996,0.32996    c-0.04004,0.05005-0.07001,0.10004-0.11005,0.15002c-0.54999,0.66998-1.38995,1.08997-2.31995,1.08997c-1.66003,0-3-1.33997-3-3    c0-0.62,0.17999-1.18994,0.5-1.66998C76.63,13.11053,76.65997,13.07056,76.70001,13.02051z M38.44,30.19055    c0.16998-0.10004,0.35999-0.17999,0.54999-0.23004c0.10999-0.03998,0.22998-0.06,0.35004-0.07996    c0.16998-0.04004,0.34998-0.05005,0.51996-0.05005c0.19,0,0.38,0.02002,0.57001,0.05005    c0.22998,0.04999,0.46002,0.12,0.66998,0.20996c1.04004,0.48004,1.76001,1.52002,1.76001,2.73999c0,1.66003-1.33997,3-3,3    c-1.65997,0-3-1.33997-3-3c0-0.81995,0.33002-1.56,0.85999-2.09998C37.92999,30.51056,38.16998,30.33051,38.44,30.19055z     M62.46997,40.83051c0,0.62006-0.18994,1.20001-0.50995,1.68005c-0.07001,0.09998-0.14001,0.19-0.22003,0.27997    c-0.04999,0.06-0.09998,0.10999-0.14996,0.16003c-0.23004,0.22998-0.49005,0.41998-0.79004,0.56995    c-0.08997,0.05005-0.17999,0.09003-0.27997,0.12006c-0.02002,0.00995-0.04004,0.01996-0.07001,0.02997    c-0.10004,0.03003-0.20001,0.06-0.29999,0.08002c-0.08002,0.01996-0.17004,0.03998-0.25,0.03998    c-0.14001,0.03003-0.28003,0.03998-0.43005,0.03998c-0.20996,0-0.40997-0.01996-0.59998-0.06    c-0.21997-0.04999-0.44-0.12-0.64001-0.19995c-1.02997-0.48004-1.76001-1.52002-1.76001-2.74005c0-1.65997,1.35004-3,3-3    C61.13,37.83051,62.46997,39.17053,62.46997,40.83051z M17.28998,52.26056c0.01001-0.01001,0.01001-0.02002,0.01001-0.04004    c0.02002-0.08997,0.04999-0.19,0.08002-0.27997c0.04999-0.17004,0.12-0.34003,0.20001-0.48999    c0.08997-0.17004,0.19-0.34003,0.32001-0.49005c0.06995-0.08997,0.13995-0.16998,0.21997-0.25    c0.53998-0.54999,1.29999-0.88,2.12-0.88c1.66003,0,3,1.34003,3,3c0,0.82001-0.33002,1.56006-0.85999,2.10004    c0,0.01001-0.01001,0.02002-0.02002,0.02002c-0.14001,0.13995-0.27997,0.25995-0.44,0.37    c-0.08997,0.06-0.17999,0.10999-0.26996,0.15997c-0.17004,0.09003-0.35004,0.16998-0.54004,0.22003    c-0.08997,0.02997-0.19,0.06-0.28998,0.06995c-0.03998,0.01001-0.07001,0.02002-0.10004,0.02002    c-0.15997,0.03003-0.31995,0.03998-0.47998,0.03998c-0.20001,0-0.40997-0.01996-0.59998-0.06    c-0.39001-0.07996-0.75-0.23999-1.08002-0.44995c-0.14001-0.10004-0.27997-0.21002-0.40997-0.34003    C17.87,54.70056,17.63,54.37054,17.47998,54.00055C17.44,53.91052,17.40997,53.82056,17.38,53.72052    c-0.01001-0.01996-0.02002-0.03998-0.02002-0.07001c-0.02997-0.07996-0.04999-0.15997-0.06-0.23999    c-0.01001-0.03998-0.01996-0.07001-0.01996-0.09998c-0.03003-0.16003-0.04004-0.32001-0.04004-0.48004    C17.23999,52.64056,17.26001,52.45056,17.28998,52.26056z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-msnsgement-text-container">
              <p>Present your performance with one-click management report or customise your own.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container_fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-insight-section-div">
          <p class="mbs-insight-section-text insight-section-text-big">User Insight</p>
          <p class="mbs-insight-section-text insight-section-text-small">“MYBOS system is easy to use and is quite extensive in its ability to record all facets of the day to day running of our property. The support team are great and action any requests in a timely fashion. All an all a great product.”</p>
          <p class="mbs-insight-section-text insight-section-text-big" style="margin:0;">John Brooker</p>
          <p class="mbs-insight-section-text insight-section-text-small" style="padding-top:0;">Colgate Palmolive Building Management</p>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-2" style="background-color: #ffffff;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_facility_top.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_facility_top_mobile.jpg') }}" />
            </div>
        </div>
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="mbs-product-top-img__container">
                <img class="mbs-product-top-img__container-img-desktop" src="{{ cdn('assets/images/product_facility_under.jpg') }}" />
                <img class="mbs-product-top-img__container-img-mobile" style="display: none;" src="{{ cdn('assets/images/product_facility_under_mobile.jpg') }}" />
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-product-overview-undertext">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-product-overview-undertext-text">FOR A FULL LIST OF FEATURES <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CONTACT US</a> TODAY</h3>
                <h3 class="mbs-product-overview-undertext-text">OR <a style="color: #00bdf2;text-decoration: none;cursor: pointer;">CLICK HERE</a> TO DOWNLOAD OUR BROCHURES</h3>
            </div>
        </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-3" style="background-color: #ebebeb;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Additional Pluses</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/Rectangle_imac-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div"  style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/cloud_product-svg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div">
                <p style="margin:0;font-size: 18px;">Cloud-based system guarantees regular back-up, maximum service uptime and no additional hardware cost to the subscriber</p>
              </div>
            </div>
            <div class="mbs-product-facility-plus-svg-div__container waves-effect">
              <div class="mbs-product-facility-plus-svg-div">
                <img class="mbs-product-facility-plus-svg-div-img" src="{{ cdn('assets/images/productpage-mybos-svgimg.png') }}" />
              </div>
              <div class="mbs-product-facility-plus-txt-div" style="padding-top:40px;">
                <p style="margin:0;font-size: 18px;">Great user experience with a simple, straight forward, intuitive user interface</p>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="mbs-section__container container" style="padding-bottom:100px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse" data-animation="zoomIn"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Request A Demo</a></div>
            </div>
        </div>
    </div>
  </section>
@endsection
