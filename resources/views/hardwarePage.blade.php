@extends('master')
@section('headScripts')
  <link rel="stylesheet" href="{{ cdn('assets/mybos/css/product-style.css') }}" type="text/css">
@endsection
@section('style')
  <!--//header navbar redefine start-->
  <style>
    .mbs-navbar__container{
      height: 75px!important;
    }
    .mbs-navbar--transparent .mbs-navbar__section {
      background: #ffffff!important;
      box-shadow: 0px 3px 5px 0px rgba(0, 0, 0, 0.3);
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-white {
      display: none!important;
    }
    img.mbs-navbar__brand-img.mbs-brand__img.logo-black {
      display: block!important;
    }
    .mbs-buttons--right .mbs-buttons__btn, .mbs-buttons--right .mbs-buttons__link {
      color: #393838!important;
    }
    .mbs-navbar-dropdown .mbs-navbar-dropdown-menu {
      background-color: #ffffff;
      border: 1px solid rgba(255,255,255,.5);
      border-radius: 4px;
      background-clip: padding-box;
      border-top-color: #fff;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    #menu-0 .mbs-navbar__hamburger {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile {
      background-color: #ffffff!important;
    }
    .mbs-sidebar-dropdown span.dropdown-toggle {
      color: #393838!important;
    }
    .mbs-sidebar__menu-mobile a {
      color: #393838!important;
    }
    @media (max-width: 991px) {
      .mbs-navbar:before, .mbs-navbar__container {
      height: auto!important;
      }
    }
  </style>
  <!--//header navbar redefine end-->
@endsection
@section('content')
  <section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/hardware_backround.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.5; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
      <div class="mbs-box__container mbs-section__container container-fluid">
        <div class="mbs-box mbs-box--stretched">
          <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:middle;">
            <div class="row">
              <div class=" col-sm-8 col-sm-offset-2">
                <div class="mbs-hero animated fadeInUp">
                    <h1 class="mbs-software-backround__text">WITH CLOUDSENSE</h1>
                    <p class="mbs-software-backround__subtext">YOU ARE IN CONTROL</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ffffff;box-shadow: inset 0 11px 12px -7px rgba(0,0,0,0.7);">
    <div class="mbs-section__container container mbs-section-currentpath">
      <label>HOME</label><label class="path-eclips"></label><label>PRODUCTS</label><label class="path-eclips"></label><label>CLOUDSENSE HARDWARE</label>
    </div>
    <div class="mbs-section__container container-fluid">
      <div class="col-md-6 mbs-hardware-ipad__container">
        <img class="mbs-hardware-product__img product__img-red" data-animation="bounceInLeft" data-timeout="1000" src="{{ cdn('assets/images/common_red.png') }}" />
        <img class="mbs-hardware-product__img product__img-blue" data-animation="bounceInLeft" data-timeout="1400" src="{{ cdn('assets/images/common_blue.png') }}" />
        <img class="mbs-hardware-product__img product__img-green" data-animation="bounceInLeft" data-timeout="1300" src="{{ cdn('assets/images/common_green.png') }}" />
        <img class="mbs-hardware-ipad__img" data-animation="fadeInUp" data-timeout="50" src="{{ cdn('assets/images/ipad_hand.png') }}" />
      </div>
      <div class="col-md-6 mbs-hardware-ipad__container-text">
        <div class="row mbs-hardware-text-form1">
          <div style="padding-left:20px;padding-right:20px;text-align:center;">
            <p style="font-family: lato-bold;font-size:50px;color:#00aeef;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">CLOUD <span style="color:#7e7c7d;font-family:lato-light;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="400">SENSE&nbsp;</span><i class="revealOnScroll fa fa-feed" data-animation="fadeInUp" data-timeout="400" aria-hidden="true"></i></p>
            <p style="font-family:roboto-light;font-size:30px;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">Wireless Asset Monitoring Sensors Remotely Monitor Your Property</p>
            <p style="font-family:roboto-light;font-size:18px;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">Cloudsense™ is the ultimate wireless monitoring solution from MYBOS with a large variety of sensors having unlimited applications in almost any built environment – and at a very low cost.</p>
          </div>
          <div style="padding-left:20px;padding-right:20px;text-align:left;">
            <p style="font-family: quicksand-bold;font-size:30px;color:#00aeef;" class="revealOnScroll mbs-why-sense-at-all" data-animation="fadeInUp" data-timeout="50">Why sense at all</p>
            <p style="font-family:roboto-light;font-size:18px;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">You cannot manage what you do not monitor. Same goes for any  facility under your management, be it a residential or commercial building, an art gallery, a cool room or an industrial unit.</p>
            <p style="font-family:roboto-light;font-size:18px;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">You cannot take care of your assets and people if you do not monitor   how well your facility is functioning.</p>
            <p style="font-family:roboto-light;font-size:18px;" class="revealOnScroll" data-animation="fadeInUp" data-timeout="50">Sensors that continuously measure critical information are essential to ensuring the well-being of a facility and its users. No one likes disruption and down-time, why should you take the risk?</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size" id="section-2" style="background-color: #01aeef;padding-top:30px;padding-bottom:30px;box-shadow: inset 0px 10px 4px -6px rgba(0,0,0, 0.5);">
    <div class="mbs-section__container container-fluid">
      <div class="col-sm-3 mbs-hardware-description-div">
        <img class="mbs-hardware-description-img mbs-hardware-coinmoney-img " data-animation="fadeInUp" data-timeout="50" src="{{ cdn('assets/images/coin-money-6.png') }}"/>
        <p style="font-family:roboto-light;font-size:25px;padding-top:20px;color:#ffffff;" class="" data-animation="fadeInUp" data-timeout="50">Economical</p>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Easy on the pocket – We charge you a small monthly per device. </p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" src="{{ cdn('assets/images/check.png') }}" data-animation="zoomIn" data-timeout="500"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">No additional computing infrastructure required.</p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">With your eye on equipment performance, you avoid unnecesary damages or failures thereby reducing the overall running cost of your facility.</p>
        </div>
      </div>
      <div class="col-sm-3 mbs-hardware-description-div">
        <img class="mbs-hardware-description-img mbs-hardware-coinmoney-img " data-animation="fadeInUp" data-timeout="50" src="{{ cdn('assets/images/Productivity.png') }}"/>
        <p style="font-family:roboto-light;font-size:25px;padding-top:20px;color:#ffffff;" class="" data-animation="fadeInUp" data-timeout="50">Efficient</p>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Easy to Setup and Use – Can be setup in just a few minutes.</p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" src="{{ cdn('assets/images/check.png') }}" data-animation="zoomIn" data-timeout="500"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Can be easily monitored  anytime from anywhere in the world with total ease.</p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Power-efficient and long life sensors – With best in class power management, sensors last for up to 10 years.</p>
        </div>
      </div>
      <div class="col-sm-3 mbs-hardware-description-div">
        <img class="mbs-hardware-description-img mbs-hardware-coinmoney-img " data-animation="fadeInUp" data-timeout="50" src="{{ cdn('assets/images/parts.png') }}"/>
        <p style="font-family:roboto-light;font-size:25px;padding-top:20px;color:#ffffff;" class="" data-animation="fadeInUp" data-timeout="50">Completeness</p>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" style="top:25px;" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Limitless applications – With a range of sensors capable of measuring temperature, water leak, humidity, voltage, motion, contact, light, pressure, harmful gases and more, you can monitor nearly everything you would ever need to. </p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" src="{{ cdn('assets/images/check.png') }}" data-animation="zoomIn" data-timeout="500"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Push notifications capable.</p>
        </div>
      </div>
      <div class="col-sm-3 mbs-hardware-description-div">
        <img class="mbs-hardware-description-img mbs-hardware-coinmoney-img " data-animation="fadeInUp" data-timeout="50" src="{{ cdn('assets/images/reliableLogo.png') }}"/>
        <p style="font-family:roboto-light;font-size:25px;padding-top:20px;color:#ffffff;" class="" data-animation="fadeInUp" data-timeout="50">Reliability</p>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Calibrated measurement configuration gives you enhanced accuracy. </p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Self powered, requiring no external power supply. and rugged.</p>
        </div>
        <div style="width:85%;float:right;position:relative;">
          <img class="mbs-hardware-check-img revealOnScroll" data-animation="zoomIn" data-timeout="500" src="{{ cdn('assets/images/check.png') }}"/>
          <p style="font-family:roboto-light;font-size:15px;padding-top:20px;color:#ffffff;text-align:justify;" class="" data-animation="fadeInUp" data-timeout="50">Advanced low battery notifications so your sensor never dies suddenly.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size" id="section-3" style="background-color: #ffffff;padding-top:30px;padding-bottom:30px;box-shadow: 0px 0px 3px 3px rgba(0,0,0, 0.5);">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2" style="padding-bottom:30px;">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">Heres how it works</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle mbs-mobile-show-padding">
      <div class="col-sm-4 mbs-hardware-work__container">
        <img class="mbs-hardware-arrow-desktop method-div-green-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-green" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-ipad" data-animation="zoomIn" data-timeout="1000">
              <svg enable-background="new 0 0 256 256" id="Layer_1" version="1.1" viewBox="0 0 256 256" xml:space="preserve">
                <g>
                  <g>
                    <path class="method-div-green"  clip-rule="evenodd" d="M145.491,162.193c-2-4.317-0.124-9.442,4.186-11.444     c4.31-2.001,9.426-0.124,11.422,4.196c4.774,10.275,9.036,20.484,12.853,30.829c1.075-2.067,2.978-3.69,5.401-4.339     c3.186-0.85,6.432,0.214,8.523,2.484c1.117-1.707,2.84-3.025,4.964-3.593c3.171-0.846,6.398,0.201,8.492,2.45     c0.689-1.096,1.637-2.049,2.815-2.763c4.061-2.46,9.344-1.156,11.798,2.913c9.355,15.509,13.322,37.118,15.055,55.005     c-4.406,8.478-30.882,19.001-50.767,18.001c-7.527-9.859-18.177-26.791-39.058-38.625c-4.23-2.166-5.908-7.359-3.747-11.598     c2.16-4.24,7.344-5.921,11.573-3.756c6.438,3.299,11.363,6.656,15.719,10.312c-1.069-3.519-2.189-7.018-3.361-10.503     C156.897,188.495,151.649,175.458,145.491,162.193L145.491,162.193z"/>
                    <path class="method-div-green"  clip-rule="evenodd" d="M160.618,243.903H40.916c-8.767,0-15.916-7.164-15.916-15.949     V15.948C25,7.164,32.148,0,40.916,0h159.096c8.767,0,15.916,7.164,15.916,15.948v156.386c-3.928-1.908-8.542-2.261-12.79-0.798     V27.886c0-1.709-1.419-3.132-3.126-3.132H40.916c-1.691,0-3.102,1.399-3.125,3.092v185.839c0,1.71,1.42,3.133,3.125,3.133h90.385     c0.276,3.106,3.509,6.351,5.99,7.659c8.525,4.85,15.829,11.028,22.36,18.329C159.979,243.171,160.3,243.535,160.618,243.903     L160.618,243.903z M119.302,225.445c2.75,0,4.98,2.235,4.98,4.99c0,2.756-2.23,4.99-4.98,4.99s-4.98-2.234-4.98-4.99     C114.322,227.681,116.552,225.445,119.302,225.445L119.302,225.445z"/>
                    <path class="method-div-green"  d="M172.021,109.777c2.699,0,4.89,2.196,4.89,4.899c0,2.707-2.192,4.901-4.889,4.901H68.908     c-2.7,0-4.891-2.195-4.891-4.9c0.002-2.705,2.192-4.9,4.89-4.9H172.021L172.021,109.777z M172.021,62.735     c2.699,0,4.89,2.196,4.89,4.898c0,2.707-2.192,4.902-4.889,4.902H68.908c-2.7,0-4.891-2.196-4.891-4.9     c0.002-2.705,2.192-4.9,4.89-4.9H172.021L172.021,62.735z M172.021,86.256c2.699,0,4.89,2.195,4.89,4.899     c0,2.706-2.192,4.901-4.889,4.901H68.908c-2.7,0-4.891-2.195-4.891-4.9c0.002-2.705,2.192-4.9,4.89-4.9H172.021L172.021,86.256z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-ipad-text">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">User specifies the tolerable limits for sensor readings and frequency of measurements</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mbs-hardware-work__container">
        <img class="mbs-hardware-arrow-desktop method-div-purple-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-purple" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-sensor" data-animation="zoomIn" data-timeout="1000">
              <svg id="Sensor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 209 181">
                <metadata>
                  <?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?>
                  <x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01        ">
                     <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                        <rdf:Description rdf:about=""/>
                     </rdf:RDF>
                  </x:xmpmeta>
                  <?xpacket end="w"?>
                </metadata>
                <defs>
                  <style>
                    .cls-1, .cls-2 {
                      fill: #673AB7;
                    }
                    .cls-1 {
                      fill-rule: evenodd;
                    }
                    .cls-2 {
                      stroke: #fff;
                      stroke-linejoin: round;
                      stroke-width: 5px;
                    }
                    .cls-3 {
                      fill: #fff;
                    }
                  </style>
                </defs>
                <path id="Rounded_Rectangle_4" data-name="Rounded Rectangle 4" class="cls-1" d="M184.271,6.6l19.49-6.432c2.153-.71,4.331,1.036,4.865,3.9l0.242,1.3c0.533,2.865-.779,5.763-2.932,6.474l-19.49,6.431c-2.153.71-4.331-1.036-4.865-3.9l-0.242-1.3C180.805,10.209,182.118,7.311,184.271,6.6Z"/>
                <path class="cls-1" d="M125.553,78.428c-1.561-1.553-3.068-3.055-4.587-4.568,12.01-10.064,18.2-28.181,30-40.151,10.878-12.89,27.371-19.634,38.623-28.184,1.5,1.313,3,2.759,4.572,4.4-11.474,8.426-28.92,14.32-39.728,27.294-11.877,11.92-17.115,30.921-28.883,41.209h0Z"/>
                <path id="Rounded_Rectangle_3" data-name="Rounded Rectangle 3" class="cls-1" d="M143.087,70.186L185.678,112.6a9.2,9.2,0,0,1,0,13.049l-42.591,42.411a9.294,9.294,0,0,1-13.1,0L87.392,125.646a9.2,9.2,0,0,1,0-13.049l42.591-42.411A9.293,9.293,0,0,1,143.087,70.186Z"/>
                <path id="Rounded_Rectangle_3_copy" data-name="Rounded Rectangle 3 copy" class="cls-1" d="M58.408,70.186L101,112.6a9.2,9.2,0,0,1,0,13.049L58.408,168.057a9.3,9.3,0,0,1-13.1,0L2.713,125.646a9.2,9.2,0,0,1,0-13.049L45.3,70.186A9.294,9.294,0,0,1,58.408,70.186Z"/>
                <rect id="Rounded_Rectangle_2" data-name="Rounded Rectangle 2" class="cls-2" x="34.813" y="59.938" width="119.5" height="119.063" rx="15" ry="15"/>
                <circle class="cls-3" cx="169.625" cy="119.484" r="9.719"/>
                <circle id="Ellipse_1_copy" data-name="Ellipse 1 copy" class="cls-3" cx="18.594" cy="119.484" r="9.75"/>
              </svg>
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-sensor-text">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">Each sensor sends its measurements wirelessly to the local gateway</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mbs-hardware-work__container">
        <img class="mbs-hardware-arrow-desktop method-div-pink-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-pink" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-modem" data-animation="zoomIn" data-timeout="1000">
              <img style="width: 75%;" src="{{ cdn('assets/images/47-Modem.png') }}" />
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-modem-text">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">Gateway sends local sensor readings to the cloud through Ethernet or 3G network.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle1 mbs-mobile-show-padding">
      <div class="col-sm-4 mbs-hardware-work__container mbs-mobile-show-float" style="float:right;">
        <img class="mbs-hardware-arrow-desktop method-div-blue-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-blue" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-finance" data-animation="zoomIn" data-timeout="1000">
              <svg enable-background="new 0 0 128 128" id="Layer_1" version="1.1" viewBox="0 0 128 128" xml:space="preserve" >
                <g>
                  <path class="method-div-blue" d="M90.742,32.592c-10.582,0.654-18.963,9.435-18.963,20.181C71.779,63.947,80.835,73,92.008,73   c3.585,0,6.956-0.938,9.876-2.58L90.741,53.148L90.742,32.592L90.742,32.592z"/>
                  <path class="method-div-blue" d="M114,52.85c0-4.039-1.098-7.82-3.004-11.067L94.544,54.102l10.558,16.367C110.498,66.482,114,60.073,114,52.85z"/>
                  <path class="method-div-blue" d="M109.465,39.511c-3.746-4.866-9.482-8.109-15.992-8.511v20.485L109.465,39.511z"/>
                </g>
                <g>
                  <path class="method-div-blue" d="M124,13H4c-2.2,0-4,1.8-4,4v72c0,2.201,1.8,4,4,4h48l-0.715,10.012c-0.158,2.195-1.829,4.914-3.715,6.047L43.5,111.5   l-1.643,0.984c0,0,0,0.002-0.002,0.002C41.385,112.77,41,113.449,41,114s0.45,1,1,1h44c0.55,0,1-0.449,1-1s-0.387-1.23-0.855-1.514   L84.5,111.5l-4.07-2.441c-1.885-1.133-3.557-3.852-3.715-6.047L76,93h48c2.2,0,4-1.799,4-4V17C128,14.8,126.2,13,124,13z M122,85H6   V19h116V85z"/>
                </g>
                <g>
                  <path class="method-div-blue" d="M57.401,31H49c-0.772,0-1.399,0.626-1.399,1.4c0,0.773,0.627,1.399,1.399,1.399h5.222L42.357,47.272L31.15,38.306   c-0.545-0.436-1.326-0.405-1.834,0.074l-14.875,14c-0.562,0.53-0.589,1.416-0.059,1.979c0.275,0.293,0.647,0.441,1.02,0.441   c0.344,0,0.689-0.127,0.959-0.382l13.99-13.166l11.3,9.041c0.584,0.467,1.432,0.393,1.925-0.168L56,36.017v4.782   c0,0.773,0.627,1.4,1.402,1.4c0.772,0,1.399-0.627,1.399-1.4V32.4C58.801,31.626,58.173,31,57.401,31z"/>
                  <g>
                    <g>
                      <path class="method-div-blue" d="M50.4,49.2c0-0.77,0.63-1.4,1.4-1.4h5.601c0.769,0,1.399,0.63,1.399,1.4v22.401c0,0.771-0.63,1.398-1.399,1.398H51.8     c-0.771,0-1.4-0.629-1.4-1.398V49.2z"/>
                    </g>
                  </g>
                  <g>
                    <g>
                      <path class="method-div-blue" d="M38.267,57.6c0-0.77,0.63-1.4,1.4-1.4h5.6c0.77,0,1.4,0.631,1.4,1.4v14.001c0,0.771-0.63,1.398-1.4,1.398h-5.6     c-0.771,0-1.4-0.629-1.4-1.398V57.6z"/>
                    </g>
                  </g>
                  <g>
                    <g>
                      <path class="method-div-blue" d="M26.133,53.4c0-0.77,0.63-1.4,1.401-1.4h5.599c0.771,0,1.401,0.63,1.401,1.4v18.202c0,0.771-0.63,1.398-1.401,1.398     h-5.599c-0.771,0-1.401-0.629-1.401-1.398V53.4z"/>
                    </g>
                  </g>
                  <g>
                    <g>
                      <path class="method-div-blue" d="M14,61.801c0-0.771,0.63-1.4,1.4-1.4H21c0.771,0,1.4,0.629,1.4,1.4v9.801C22.4,72.372,21.77,73,21,73h-5.6     c-0.77,0-1.4-0.629-1.4-1.398V61.801z"/>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-finance-finance">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">Live and historical measurements are viewable via online portal.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mbs-hardware-work__container mbs-mobile-show-float" style="float:right;">
        <img class="mbs-hardware-arrow-desktop method-div-orange-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-orange" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-message" data-animation="zoomIn" data-timeout="1000">
              <svg viewBox="0 0 48 48">
                <path class="method-div-orange" d="M40 4h-32c-2.21 0-3.98 1.79-3.98 4l-.02 36 8-8h28c2.21 0 4-1.79 4-4v-24c0-2.21-1.79-4-4-4zm-14 18h-4v-12h4v12zm0 8h-4v-4h4v4z"/>
                <path d="M0 0h48v48h-48z" fill="none"/>
              </svg>
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-message-text">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">Critical readings (outside the acceptable range) are instantly pushed as SMS and/or email messages to users.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mbs-hardware-work__container">
        <img class="mbs-hardware-arrow-desktop method-div-red-arrow" src="{{ cdn('assets/images/hardwarepage_arrow.gif') }}" />
        <div class="mbs-work-method-div method-div-red" data-animation="zoomIn">
          <div class="mbs-work-method-div-childdiv" data-animation="zoomIn" data-timeout="500">
            <div class="mbs-work-method-div-childdiv-img-div childdiv-img-solveissue" data-animation="zoomIn" data-timeout="1000">
              <svg enable-background="new 0 0 48 48" id="Layer_1" version="1.1" viewBox="0 0 48 48" xml:space="preserve">
                <g>
                  <path class="method-div-red" d="M24,24.9c5.7,0,10.3-4.9,10.3-11H13.7C13.7,19.9,18.3,24.9,24,24.9z"/>
                  <path class="method-div-red" d="M36.1,10.1c-1.1-3.9-3.8-7-7.3-8.6L26.3,8V0.7c-0.7-0.1-1.5-0.2-2.3-0.2c-0.8,0-1.5,0.1-2.3,0.2V8l-2.5-6.5   C15.7,3,13,6.2,11.9,10.1h-1.3v1.8h26.8v-1.8H36.1z"/>
                  <g>
                    <path class="method-div-red" d="M33.8,31.2v16.3h11c0-8.1-4.7-15.2-11.4-18.6C33.7,29.6,33.8,30.4,33.8,31.2z"/>
                    <path class="method-div-red" d="M29.9,27.5C28,27,26.1,26.6,24,26.6h0c-2.1,0-4,0.3-5.9,0.9c-1.2,0.8-1.9,2.2-1.9,3.7v16.3h15.7V31.2    C31.8,29.7,31.1,28.3,29.9,27.5z"/>
                    <path class="method-div-red" d="M14.2,31.2c0-0.8,0.2-1.6,0.4-2.3C7.8,32.3,3.1,39.4,3.1,47.5h11V31.2z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-work-method-div-childdiv-text-div childdiv-img-solveissue-text">
              <p style="font-family: lato-light;font-size:15px;font-weight:600;">User takes immediate corrective action to resolve the issue</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size" id="section-3" style="background-color: #ffffff;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="revealOnScroll mbs-brand-header__text" data-animation="zoomIn">Typical Industry Applications</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle" style="padding-bottom:80px;">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="col-md-3">
          <div class="revealOnScroll mbs-hardware-typical-industry-div" data-animation="flipInY ">
            <div class="mbs-hardware-typical-industry-div-overview">
              <label style="font-family:lato-bold;font-size:18px;color: #ffffff;">Environment</label>
              <p style="font-family:lato-light;font-size:15px;color: #ffffff;margin:0;">Temperature, Humidity, Light, CO</p>
            </div>
            <div class="mbs-hardware-typical-industry-div-overlay"></div>
            <img src="{{ cdn('assets/images/industry_application_1.jpg') }}" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="revealOnScroll mbs-hardware-typical-industry-div" data-animation="flipInY ">
            <div class="mbs-hardware-typical-industry-div-overview">
              <label style="font-family:lato-bold;font-size:18px;color: #ffffff;">Safety & Security</label>
              <p style="font-family:lato-light;font-size:15px;color: #ffffff;margin:0;">Motion, Vibration, Open, Close</p>
            </div>
            <div class="mbs-hardware-typical-industry-div-overlay"></div>
            <img src="{{ cdn('assets/images/industry_application_2.jpg') }}" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="revealOnScroll mbs-hardware-typical-industry-div" data-animation="flipInY">
            <div class="mbs-hardware-typical-industry-div-overview">
              <label style="font-family:lato-bold;font-size:18px;color: #ffffff;">Equipment</label>
              <p style="font-family:lato-light;font-size:15px;color: #ffffff;margin:0;">Temperature, Vivration, Air Flow, Water</p>
            </div>
            <div class="mbs-hardware-typical-industry-div-overlay"></div>
            <img src="{{ cdn('assets/images/industry_application_3.jpg') }}" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="revealOnScroll mbs-hardware-typical-industry-div" data-animation="flipInY">
            <div class="mbs-hardware-typical-industry-div-overview">
              <label style="font-family:lato-bold;font-size:18px;color: #ffffff;">Services</label>
              <p style="font-family:lato-light;font-size:15px;color: #ffffff;margin:0;">Vehicle Detection, Button Press</p>
            </div>
            <div class="mbs-hardware-typical-industry-div-overlay"></div>
            <img src="{{ cdn('assets/images/industry_application_4.jpg') }}" />
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mbs-section mbs-section--relative mbs-section--fixed-size" id="section-3" style="background-color: #ebebeb;">
    <div class="mbs-section__container container-fluid mbs-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-brand-header__text" data-animation="bounceInDown">A Whole Range Of Sensors Available</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container-fluid mbs-section__container--middle" style="padding-bottom:80px;">
      <div class="mbs-header mbs-header--wysiwyg row">
        <div class="mbs-sensor-container-div">
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="50">
            <img class="mbs-sensor-img sensor-img-temperator" src="{{ cdn('assets/images/temperator.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Temperature</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="150">
            <img class="mbs-sensor-img sensor-img-water" src="{{ cdn('assets/images/water.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Water</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="250">
            <img class="mbs-sensor-img sensor-img-water_lop" src="{{ cdn('assets/images/water_lop.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Water Rope</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="350">
            <img class="mbs-sensor-img sensor-img-air" src="{{ cdn('assets/images/air.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Air</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="450">
            <img class="mbs-sensor-img sensor-img-humidity" src="{{ cdn('assets/images/humidity.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Humidity</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="550">
            <img class="mbs-sensor-img sensor-img-open_door" src="{{ cdn('assets/images/open_door.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Open / Close</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="50">
            <img class="mbs-sensor-img sensor-img-electrical_sensor" src="{{ cdn('assets/images/electrical_sensor.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Activity</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="150">
            <img class="mbs-sensor-img sensor-img-Car_Front_View" src="{{ cdn('assets/images/Car_Front_View.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Vehicle</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="250">
            <img class="mbs-sensor-img sensor-img-lightning-circle-fast-charge" src="{{ cdn('assets/images/lightning-circle-fast-charge.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Ac Current</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="350">
            <img class="mbs-sensor-img sensor-img-run_exit_2" src="{{ cdn('assets/images/run_exit_2.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Motion</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="450">
            <img class="mbs-sensor-img sensor-img-light" src="{{ cdn('assets/images/light.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">Light</p>
          </div>
          <div class="col-sm-2 col-xs-4 mbs-sensor-img-div" data-animation="bounceInRight" data-timeout="550">
            <img class="mbs-sensor-img sensor-img-CO" src="{{ cdn('assets/images/CO.png') }}" />
            <p style="font-family:roboto-light;font-size: 15px;font-weight:bold;color:#707070;margin-top:10px;">CO</p>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-section__container container" style="padding-bottom:100px;">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse" data-animation="zoomIn"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Contact Us To Learn More</a></div>
            </div>
        </div>
    </div>
  </section>
@endsection
