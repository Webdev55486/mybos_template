@extends('master')
@section('content')
<section class="mbs-box mbs-section mbs-section--relative mbs-section--fixed-size mbs-section--full-height mbs-section--bg-adapted mbs-parallax-background" id="section-0" style="background-image: url({{ cdn('assets/images/homepage_background.jpg') }});">
  <div class="mbs-box__magnet mbs-box__magnet--sm-padding mbs-box__magnet--center-center mbs-after-navbar">
    <div class="mbs-box__container mbs-section__container container-fluid">
      <div class="mbs-box mbs-box--stretched">
        <div class="mbs-box__magnet mbs-box__magnet--center-center" style="vertical-align:top;">
          <div class="row">
            <div class=" col-sm-8 col-sm-offset-2">
              <div class="mbs-hero animated fadeInUp">
                  <h1 class="mbs-hero__text">powerfully simple</h1>
                  <p class="mbs-hero__subtext">facilities management technologies</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mbs-arrow mbs-arrow--floating text-center">
        <div class="mbs-section__container container-fluid">
            <a class="mbs-arrow__link" href="#section-1"><i class="glyphicon glyphicon-menu-down" style="font-size:40px;color:#03a8f3;"></i></a>
        </div>
    </div>
  </div>
</section>

<section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-1" style="background-color:#ffffff;">
  <div class="mbs-section__container container-fluid mbs-section__container--first">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
              <h3 class="mbs-brand-header__text" data-animation="bounceInDown">COMPANIES THAT TRUST OUR BRANDS</h3>
          </div>
      </div>
  </div>
  <div class="mbs-section__container container-fluid mbs-section__container--middle" style="padding-bottom:50px;">
      <div class="row">
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="200"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/BrookfieldLogo.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="230"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/SydnyHorbour_logo.jpg') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="260"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo3.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="290"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo4.jpg') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="320"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/AccorHotels_logo.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="350"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/Chanel-logo-wordmark.png') }}" /></div>
      </div>
      <div class="row">
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="380"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/q1_official_logo_m1.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="410"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo8.jpg') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="440"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo9.jpg') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="470"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/stm_logo.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="500"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo11.png') }}" /></div>
          <div class="mbs-brand-header__div" data-animation="bounceInRight" data-timeout="530"><img class="mbs-brand-header__img" src="{{ cdn('assets/images/brand/logo12.jpg') }}" /></div>
      </div>
  </div>
</section>

<section class="mbs-section mbs-section--relative mbs-section--fixed-size" id="section-2" style="background-color:#eaecee;">
  <div class="mbs-section__container container-fluid mbs-section__container--first">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
              <h3 class="revealOnScroll mbs-brand-header__text" data-animation="bounceInDown">WE’RE NOT JUST A SOFTWARE DEVELOPMENT COMPANY</h3>
          </div>
      </div>
  </div>
  <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="row">
          <div class="mbs-article mbs-article--wysiwyg col-sm-8 col-sm-offset-2">
            <p class="revealOnScroll mbs-imac-subtext" data-animation="zoomIn" data-timeout="200">
              Our philosophy is to provide our clients with cost‐effective, personalised and innovative facilities management related software and hardware solutions, designed to allow managers to easily, effectively and efficiently maintain their clients most valuable asset and, in turn, ensure the value and quality are sustained.
            </p>
          </div>
      </div>
  </div>
  <div class="mbs-section__container container-fluid">
      <div class="row mbs-section-imac-div">
        <div class="imacOnScroll mbs-common-div mbs-common-green" data-animation="bounceInLeft"><img src="{{ cdn('assets/images/common_green.png') }}" /></div>
        <div class="imacOnScroll mbs-common-div mbs-common-blue" data-animation="bounceInLeft" data-timeout="400"><img src="{{ cdn('assets/images/common_blue.png') }}" /></div>
        <div class="imacOnScroll mbs-common-div mbs-common-red" data-animation="bounceInRight" data-timeout="200"><img src="{{ cdn('assets/images/common_red.png') }}" /></div>
        <div class="imacOnScroll mbs-common-div mbs-common-grey" data-animation="bounceInRight" data-timeout="600"><img src="{{ cdn('assets/images/common_grey.png') }}" /></div>
        <div class="mbs-imac-img" style="background-image: url({{ cdn('assets/images/imac.png') }});">
          <div class="imacOnScroll mbs-imac-subimg-div" data-animation="zoomIn" data-timeout="700">
            <div class="mbs-imac-subimg" style="background-image: url({{ cdn('assets/images/imac_background.jpg') }});"></div>
          </div>
        </div>
      </div>
      <div class="row mbs-section-imac-button-div">
        <div class="col-md-6 mbs-imac-button-div-colmn">
          <a href="{{url('software')}}" type="button" class="revealOnScroll btn mbs-imac-select-button waves-effect" data-animation="bounceInLeft" data-timeout="100">LEARN MORE ABOUT MYBOS SOFTWARE</a>
        </div>
        <div class="col-md-6 mbs-imac-button-div-colmn">
          <a href="{{url('hardware')}}" type="button" class="revealOnScroll btn mbs-imac-select-button waves-effect" data-animation="bounceInRight" data-timeout="100">LEARN MORE ABOUT CLOUDSENSE</a>
        </div>
      </div>
  </div>
</section>

<section class="mbs-section-service mbs-section--relative mbs-section--fixed-size" id="section-3" style="background-color:#eaecee;">
  <div class="mbs-section__container container-fluid mbs-section-service__container" style="padding-top:0!important;">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-md-3">
            <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
              <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <path d="M50,6.4668C25.99609,6.4668,6.4668,25.99609,6.4668,50c0,11.76154,4.69647,22.44122,12.30286,30.2832   c0.1272,0.16779,0.27582,0.32098,0.43982,0.4613C27.09412,88.64087,37.98633,93.5332,50,93.5332   c11.73438,0,22.7373-4.59863,30.98047-12.9502c0.06873-0.06958,0.11963-0.14911,0.18018-0.22345   c0.03253-0.03998,0.06622-0.07825,0.0965-0.11963c0.08905-0.12146,0.1651-0.24762,0.23352-0.3783   c0.02014-0.03839,0.04114-0.07581,0.05957-0.11499c0.06439-0.13702,0.11578-0.27661,0.1582-0.41998   c0.01233-0.04138,0.02533-0.08191,0.03589-0.12378c0.03522-0.14075,0.05823-0.2821,0.07269-0.42578   c0.00537-0.05139,0.01105-0.10205,0.01373-0.15375c0.00665-0.1355,0.0022-0.26965-0.0094-0.40485   c-0.00513-0.06122-0.0097-0.12158-0.01868-0.18268c-0.0191-0.12921-0.04919-0.25537-0.08521-0.38159   c-0.01807-0.06372-0.03503-0.12689-0.05762-0.18988c-0.04376-0.12213-0.09894-0.23932-0.15906-0.35602   c-0.02197-0.04303-0.03412-0.08844-0.05841-0.1308c-0.02197-0.03833-0.04944-0.07446-0.07172-0.11267   c-0.01831-0.02869-0.03522-0.0575-0.05457-0.08569c-2.0614-3.48193-5.51935-6.42334-10.29266-8.73621   c-5.69238-2.77295-9.54645-5.63574-9.83514-10.42505c4.17902-5.29523,6.77557-12.77283,6.77557-18.46851   c0-9.94727-8.09277-18.04004-18.04004-18.04004c-9.94824,0-18.04102,8.09277-18.04102,18.04004   c0,5.76123,2.65594,13.34668,6.92004,18.65143c-0.37408,4.68073-4.20563,7.50409-9.82043,10.2392   c-3.3233,1.61035-5.99536,3.52844-8.01819,5.71423C15.65729,67.27917,12.4668,59.00592,12.4668,50   c0-20.69629,16.83691-37.5332,37.5332-37.5332S87.5332,29.30371,87.5332,50c0,6.1123-1.4209,11.93945-4.22363,17.31738   c-0.76563,1.46973-0.19531,3.28125,1.27344,4.04688c1.47168,0.76758,3.28223,0.19531,4.04688-1.27344   C91.83789,63.93555,93.5332,56.98828,93.5332,50C93.5332,25.99609,74.00391,6.4668,50,6.4668z M49.92383,27.10938   c6.63867,0,12.04004,5.40137,12.04004,12.04004c0,6.98828-5.94922,19.02734-12.04004,19.02734S37.88281,46.1377,37.88281,39.14941   C37.88281,32.51074,43.28418,27.10938,49.92383,27.10938z M31.60352,73.4375c3.43475-1.67285,9.8241-4.80096,12.25458-11.06689   c1.88007,1.13721,3.9162,1.80615,6.06573,1.80615c2.19568,0,4.27399-0.6955,6.1875-1.87805   c2.40881,6.31525,8.83813,9.45996,12.29102,11.14172c2.7926,1.35284,4.96094,2.91089,6.48212,4.64722   C68.0163,84.19153,59.27618,87.5332,50,87.5332c-9.53485,0-18.24512-3.57971-24.87268-9.45795   C26.64825,76.34375,28.81323,74.78925,31.60352,73.4375z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-service-div">
              <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Occupants, Owners and Managing Agents</h3>
              <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                Benefit from increased rental and asset value through a better maintained building, assets and facilities.
              </p>
              <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-manage-agents')}}">Read more</a></label>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
              <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <path d="M92.496521,33.06134c-0.000427-0.02655-0.00293-0.052429-0.004028-0.078796   c-0.070557-8.20459-6.866211-14.859497-15.217102-14.859497H63.842651c0.002747-0.052856,0.015747-0.102722,0.015747-0.15625   V16.70459c0-4.960449-4.035156-8.996094-8.996094-8.996094h-9.78418c-4.960938,0-8.996094,4.035645-8.996094,8.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H22.666016c-8.307922,0-15.069214,6.637817-15.15686,14.82959   c-0.002258,0.042786-0.006226,0.084656-0.006592,0.127686c0,0.01062-0.001587,0.020874-0.001587,0.031494l0.029297,41.301758   c0,3.710327,2.461121,6.861511,5.860901,7.975281c0.330811,5.591125,6.010376,9.902649,13.222107,9.902649h46.713867   c7.213135,0,12.893494-4.313232,13.22229-9.905884c3.383484-1.116211,5.831421-4.263367,5.831421-7.963257l0.118164-41.3125   C92.499023,33.093445,92.496582,33.077698,92.496521,33.06134z M42.082031,17.966797V16.70459   c0-1.651855,1.34375-2.996094,2.996094-2.996094h9.78418c1.652344,0,2.996094,1.344238,2.996094,2.996094v1.262207   c0,0.053528,0.013,0.103394,0.015747,0.15625H42.066284C42.069031,18.07019,42.082031,18.020325,42.082031,17.966797z    M22.666016,24.123047h54.609375c4.915283,0,9.070679,3.978882,9.210449,8.733398   c-1.340881,7.173462-4.711609,13.725525-9.775879,18.958008c-1.151367,1.190918-1.120117,3.090332,0.070313,4.242188   c0.582031,0.563965,1.333984,0.844238,2.085938,0.844238c0.78418,0,1.567383-0.305664,2.15625-0.913574   c2.057556-2.126526,3.869507-4.443542,5.430847-6.90918l-0.072449,25.335449c0,1.306641-1.158203,2.410156-2.529297,2.410156   H60.384766H16.088867c-1.363281,0-2.558594-1.126465-2.558594-2.412109l-0.018005-25.390076   c7.704041,12.219727,21.28595,20.121033,36.486755,20.121033c6.727539,0,13.444336-1.594238,19.424805-4.609863   c1.479492-0.746094,2.074219-2.550293,1.328125-4.029297c-0.746094-1.47998-2.551758-2.07373-4.029297-1.328125   c-2.703491,1.363281-5.584839,2.37915-8.54895,3.045837c0.201599-0.726685,0.317749-1.488953,0.317749-2.279846   c0-4.706116-3.815063-8.521118-8.521118-8.521118s-8.521057,3.815002-8.521057,8.521118   c0,0.79248,0.116516,1.556335,0.318909,2.284302C27.581909,59.01532,16.293762,47.665283,13.512939,32.878418   C13.639771,28.030396,17.691956,24.123047,22.666016,24.123047z M49.970337,62.781616   c-1.568665,0-2.840332-1.271667-2.840332-2.840332c0-1.568726,1.271667-2.840393,2.840332-2.840393   c1.568726,0,2.840393,1.271667,2.840393,2.840393C52.81073,61.509949,51.539063,62.781616,49.970337,62.781616z    M73.327148,86.291504H26.613281c-3.633606,0-6.323853-1.681396-7.051086-3.467773h60.81604   C79.651001,84.610107,76.960754,86.291504,73.327148,86.291504z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-service-div">
              <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Strata & Property Managers</h3>
              <p class="" data-animation="zoomIn" data-timeout="200">
                Take the advantage with smarter facility management to smoothen building functioning and communication.
              </p>
              <label class="mbs-service-header__subtext" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-property-management')}}">Read more</a></label>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
              <svg style="enable-background:new 0 0 100 100;" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <path d="M75.4583,32.15699c-0.1001-1.82324-0.40088-3.63867-0.89404-5.39844c-0.44727-1.5957-2.10547-2.5293-3.69824-2.0791   c-1.59521,0.44727-2.52637,2.10254-2.0791,3.69824c0.37549,1.33887,0.604,2.7207,0.68066,4.11328   c0.32129,5.64844-1.80518,11.14551-5.83594,15.08398l-0.98438,0.95313c-3.35107,3.23242-5.99854,5.78516-5.99854,11.91406v2.25879   H43.3538v-2.25879c0-6.26367-2.62012-8.73828-6.24707-12.16406l-0.52637-0.49805c-3.9209-3.72168-6.08008-8.74609-6.08008-14.14844   c0-5.35449,2.12939-10.34766,5.99609-14.06055c3.86426-3.70899,8.94385-5.64649,14.31787-5.41406   c3.16113,0.12695,6.27734,1.04492,9.01221,2.65332c1.42969,0.8418,3.26758,0.36426,4.10693-1.06543   c0.83985-1.42773,0.36329-3.2666-1.06494-4.10645c-3.5835-2.10742-7.66797-3.30957-11.81104-3.47656   c-7.01904-0.28613-13.66211,2.22949-18.7168,7.08105c-5.05566,4.85449-7.84033,11.38477-7.84033,18.38867   c0,7.06445,2.82324,13.63477,7.95068,18.50098l0.53564,0.50684c3.2666,3.08594,4.36719,4.125,4.36719,7.80273v3.3819   c-2.21075,1.25317-3.7085,3.6239-3.7085,6.34173c0,1.63684,0.54883,3.14404,1.46307,4.362   c-0.91522,1.24408-1.46307,2.77435-1.46307,4.4339c0,3.99152,3.13348,7.25635,7.06848,7.4856   c0.07416,0.00549,0.14502,0.02222,0.22058,0.02222h2.30939c0.70569,3.08215,3.4646,5.39063,6.75751,5.39063   s6.05182-2.30847,6.75751-5.39063h3.88654c1.65674,0,3-1.34277,3-3s-1.34326-3-3-3H41.15263   c-0.83105,0-1.50732-0.67676-1.50732-1.50781s0.67627-1.50684,1.50732-1.50684h19.49268c1.65674,0,3-1.34277,3-3s-1.34326-3-3-3   H40.93437c-0.71094,0-1.28906-0.57813-1.28906-1.28906s0.57813-1.28906,1.28906-1.28906c0.3432,0,0.66748-0.0697,0.97461-0.17578   h17.73975c1.65674,0,3-1.34277,3-3v-5.25879c0-3.58008,1.13379-4.67285,4.16358-7.59473   c0.3252-0.31348,0.66357-0.63965,1.01367-0.98242C73.09599,46.71656,75.87773,39.53003,75.4583,32.15699z"/>
                </g>
              </svg>
            </div>
            <div class="mbs-service-div">
              <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Building & Facility Managers</h3>
              <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                Empower yourself with total control over your facility and seamless interaction with your community and contractors.
              </p>
              <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-facility-management')}}">Read more</a></label>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-service-header-circleimg" data-animation="zoomIn" data-timeout="200">
              <svg enable-background="new 0 0 100 100" class="mbs-service-svg" version="1.1" viewBox="0 0 100 100" xml:space="preserve">
                <g id="Layer_1">
                  <g>
                    <path d="M91.5,83.5h-7V35.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H66.5c-1.65723,0-3,1.34326-3,3s1.34277,3,3,3h9.99219    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5H59.50781V18.50781c0-4.41553-3.59277-8.00781-8.00781-8.00781H23.50391    C19.09082,10.5,15.5,14.09033,15.5,18.50391V83.5h-7c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h83c1.65723,0,3-1.34277,3-3    S93.15723,83.5,91.5,83.5z M21.5,18.50391c0-1.10498,0.89941-2.00391,2.00391-2.00391H51.5    c1.10742,0,2.00781,0.90088,2.00781,2.00781V83.5h-9.76654c0.0025-0.04218,0.01257-0.08221,0.01257-0.125v-11.75    c0-1.17365-0.95142-2.125-2.125-2.125h-8.25c-1.17365,0-2.125,0.95135-2.125,2.125v11.75c0,0.04279,0.01007,0.08282,0.01257,0.125    H21.5V18.50391z"/><path d="M67.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S65.84277,43.5,67.5,43.5z"/><path d="M67.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C64.5,52.15723,65.84277,53.5,67.5,53.5z"/>
                    <path d="M67.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,63.5,67.5,63.5z"/>
                    <path d="M67.5,73.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S65.84277,73.5,67.5,73.5z"/>
                    <path d="M42.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,43.5,42.5,43.5z"/>
                    <path d="M27.5,43.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,43.5,27.5,43.5z"/>
                    <path d="M42.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C39.5,52.15723,40.84277,53.5,42.5,53.5z"/>
                    <path d="M27.5,53.5h5c1.65723,0,3-1.34277,3-3c0-1.65674-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3    C24.5,52.15723,25.84277,53.5,27.5,53.5z"/>
                    <path d="M42.5,63.5h5c1.65723,0,3-1.34277,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3S40.84277,63.5,42.5,63.5z"/>
                    <path d="M35.5,60.5c0-1.65723-1.34277-3-3-3h-5c-1.65723,0-3,1.34277-3,3s1.34277,3,3,3h5C34.15723,63.5,35.5,62.15723,35.5,60.5z"/>
                    <path d="M42.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S40.84277,33.5,42.5,33.5z"/>
                    <path d="M27.5,33.5h5c1.65723,0,3-1.34326,3-3s-1.34277-3-3-3h-5c-1.65723,0-3,1.34326-3,3S25.84277,33.5,27.5,33.5z"/>
                  </g>
                </g>
              </svg>
            </div>
            <div class="mbs-service-div">
              <h3 class="mbs-service-header__text" data-animation="zoomIn" data-timeout="200">For Developers & &nbsp;&nbsp; Builders</h3>
              <p class="mbs-service-header__subtext" data-animation="zoomIn" data-timeout="200">
                Destine your development for excellence – Equip it with the system managers, owners and occupants love.
              </p>
              <label class="" data-animation="bounceInUp" data-timeout="200"><a href="{{url('industries-construction-development')}}">Read more</a></label>
            </div>
          </div>
      </div>
  </div>
</section>

<section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-4" style="background-color:#ffffff;">
  <div class="mbs-section__container container-fluid mbs-section-abotus__container">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-sm-8 col-sm-offset-2">
              <h3 class="mbs-brand-header__text" data-animation="bounceInDown">WHAT’S BEING SAID ABOUT US</h3>
          </div>
      </div>
  </div>
  <div class="mbs-section__container container-fluid mbs-section__container--middle">
      <div class="mbs-header mbs-header--wysiwyg row">
          <div class="col-md-3">
            <div class="mbs-aboutus-div__container" data-animation="flipInX">
              <div class="mbs-aboutus-div-content-text">
                <p class="" data-animation="zoomIn" data-timeout="400">
                  My experience with MYBOS Building Management System has been very positive. Their customer service and responsiveness is truly exceptional. The interface is very nice and easy, providing an easy learning curve for the users.
                </p>
              </div>
              <div class="mbs-aboutus-div-content-author">
                <div class="mbs-aboutus-div-content-author-quota-div" data-animation="bounceInDown" data-timeout="400">
                  <svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  	 viewBox="0 0 23 18" style="enable-background:new 0 0 23 18;" xml:space="preserve">
                    <style type="text/css">
                    	.st0{enable-background:new    ;}
                    	.st1{fill:#00AEEF;}
                    </style>
                    <g id="_" class="st0">
                    	<path class="st1" d="M10.1,0.6l0.6,1C7.2,3.4,5.5,5.2,5.5,6.8c0,0.7,0.7,1.4,2,2.1c1.2,0.6,2,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4
                    		c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7c-0.9-1.1-1.3-2.6-1.3-4.5c0-3,1.1-5.6,3.4-7.8
                    		C5.3,3.1,6,2.6,6.8,2.1C7.5,1.7,8.6,1.2,10.1,0.6z M22.2,0.6l0.6,1c-3.5,1.9-5.2,3.6-5.2,5.2c0,0.4,0.1,0.7,0.4,1s0.8,0.6,1.5,1
                    		c1.1,0.6,1.9,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7
                    		c-0.9-1.1-1.3-2.6-1.3-4.5c0-1.4,0.3-2.8,0.9-4.2c0.6-1.3,1.4-2.5,2.6-3.6C17.9,2.6,19.8,1.5,22.2,0.6z"/>
                    </g>
                  </svg>
                </div>
                <label>- Daniel O'Brien</label>
                <h3 class="mbs-author-subtext">Building Manager</h3>
                <h3 class="mbs-author-subtext">Burling Building Management</h3>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-aboutus-div__container" data-animation="flipInX">
              <div class="mbs-aboutus-div-content-text">
                <p class="" data-animation="zoomIn" data-timeout="400">
                  The MYBOS system is easy to use and is quite extensive in its ability to record all facets of the day to day running of our property. The support team are great and action any requests in a timely fashion. All an all a great product.
                </p>
              </div>
              <div class="mbs-aboutus-div-content-author">
                <div class="mbs-aboutus-div-content-author-quota-div" data-animation="bounceInDown" data-timeout="400">
                  <svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  	 viewBox="0 0 23 18" style="enable-background:new 0 0 23 18;" xml:space="preserve">
                    <style type="text/css">
                    	.st0{enable-background:new    ;}
                    	.st1{fill:#00AEEF;}
                    </style>
                    <g id="_" class="st0">
                    	<path class="st1" d="M10.1,0.6l0.6,1C7.2,3.4,5.5,5.2,5.5,6.8c0,0.7,0.7,1.4,2,2.1c1.2,0.6,2,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4
                    		c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7c-0.9-1.1-1.3-2.6-1.3-4.5c0-3,1.1-5.6,3.4-7.8
                    		C5.3,3.1,6,2.6,6.8,2.1C7.5,1.7,8.6,1.2,10.1,0.6z M22.2,0.6l0.6,1c-3.5,1.9-5.2,3.6-5.2,5.2c0,0.4,0.1,0.7,0.4,1s0.8,0.6,1.5,1
                    		c1.1,0.6,1.9,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7
                    		c-0.9-1.1-1.3-2.6-1.3-4.5c0-1.4,0.3-2.8,0.9-4.2c0.6-1.3,1.4-2.5,2.6-3.6C17.9,2.6,19.8,1.5,22.2,0.6z"/>
                    </g>
                  </svg>
                </div>
                <label>- John Brooker</label>
                <h3 class="mbs-author-subtext">SP 61138</h3>
                <h3 class="mbs-author-subtext"> </h3>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-aboutus-div__container" data-animation="flipInX">
              <div class="mbs-aboutus-div-content-text">
                <p class="" data-animation="zoomIn" data-timeout="400">
                  Makes facility management easy to track and great for residential developments to inform occupants of scheduled works with the broadcast system.
                </p>
              </div>
              <div class="mbs-aboutus-div-content-author">
                <div class="mbs-aboutus-div-content-author-quota-div" data-animation="bounceInDown" data-timeout="400">
                  <svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  	 viewBox="0 0 23 18" style="enable-background:new 0 0 23 18;" xml:space="preserve">
                    <style type="text/css">
                    	.st0{enable-background:new    ;}
                    	.st1{fill:#00AEEF;}
                    </style>
                    <g id="_" class="st0">
                    	<path class="st1" d="M10.1,0.6l0.6,1C7.2,3.4,5.5,5.2,5.5,6.8c0,0.7,0.7,1.4,2,2.1c1.2,0.6,2,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4
                    		c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7c-0.9-1.1-1.3-2.6-1.3-4.5c0-3,1.1-5.6,3.4-7.8
                    		C5.3,3.1,6,2.6,6.8,2.1C7.5,1.7,8.6,1.2,10.1,0.6z M22.2,0.6l0.6,1c-3.5,1.9-5.2,3.6-5.2,5.2c0,0.4,0.1,0.7,0.4,1s0.8,0.6,1.5,1
                    		c1.1,0.6,1.9,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7
                    		c-0.9-1.1-1.3-2.6-1.3-4.5c0-1.4,0.3-2.8,0.9-4.2c0.6-1.3,1.4-2.5,2.6-3.6C17.9,2.6,19.8,1.5,22.2,0.6z"/>
                    </g>
                  </svg>
                </div>
                <label>- Chris Kingdom</label>
                <h3 class="mbs-author-subtext">JLL</h3>
                <h3 class="mbs-author-subtext"> </h3>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="mbs-aboutus-div__container" data-animation="flipInX">
              <div class="mbs-aboutus-div-content-text">
                <p class="" data-animation="zoomIn" data-timeout="400">
                  Using Mybos has assisted us in management of our buildings, residents, security and work orders.

                  It is an effective tool to ensure compliance matters, maintenance schedules and work history. One of the best programs I have ever used.
                </p>
              </div>
              <div class="mbs-aboutus-div-content-author">
                <div class="mbs-aboutus-div-content-author-quota-div" data-animation="bounceInDown" data-timeout="400">
                  <svg version="1.0" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  	 viewBox="0 0 23 18" style="enable-background:new 0 0 23 18;" xml:space="preserve">
                    <style type="text/css">
                    	.st0{enable-background:new    ;}
                    	.st1{fill:#00AEEF;}
                    </style>
                    <g id="_" class="st0">
                    	<path class="st1" d="M10.1,0.6l0.6,1C7.2,3.4,5.5,5.2,5.5,6.8c0,0.7,0.7,1.4,2,2.1c1.2,0.6,2,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4
                    		c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7c-0.9-1.1-1.3-2.6-1.3-4.5c0-3,1.1-5.6,3.4-7.8
                    		C5.3,3.1,6,2.6,6.8,2.1C7.5,1.7,8.6,1.2,10.1,0.6z M22.2,0.6l0.6,1c-3.5,1.9-5.2,3.6-5.2,5.2c0,0.4,0.1,0.7,0.4,1s0.8,0.6,1.5,1
                    		c1.1,0.6,1.9,1.2,2.4,1.8c0.5,0.6,0.7,1.4,0.7,2.4c0,1.3-0.5,2.4-1.4,3.4c-0.9,0.9-2,1.4-3.3,1.4c-1.4,0-2.6-0.6-3.5-1.7
                    		c-0.9-1.1-1.3-2.6-1.3-4.5c0-1.4,0.3-2.8,0.9-4.2c0.6-1.3,1.4-2.5,2.6-3.6C17.9,2.6,19.8,1.5,22.2,0.6z"/>
                    </g>
                  </svg>
                </div>
                <label>- Stella McWiggan</label>
                <h3 class="mbs-author-subtext">Senior Licensed Strata Manager</h3>
                <h3 class="mbs-author-subtext">Premium Strata</h3>
              </div>
            </div>
          </div>
      </div>
  </div>
</section>

<section class="mbs-section mbs-section--relative mbs-section--fixed-size mbs-parallax-background" id="section-5" style="background-image: url({{ cdn('assets/images/contact_background.jpg') }});">
    <div class="mbs-overlay" style="opacity: 0.8; background-color: rgb(0, 0, 0);"></div>
    <div class="mbs-section__container container mbs-contact-section__container--first">
        <div class="mbs-header mbs-header--wysiwyg row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="mbs-contactform-header__text">BOOK A DEMONSTRATION TODAY</h3>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container mbs-section__container--middle">
        <div class="row">
            <div class="mbs-article mbs-article--wysiwyg col-sm-8 col-sm-offset-2">
              <p>
                Contact us to arrange a personalised demo
                of our software and hardware products or to receive
                additional information.
              </p>
            </div>
        </div>
    </div>
    <div class="mbs-section__container container mbs-contact-section__container--last">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="mbs-buttons mbs-buttons--center btn-inverse"><a class="mbs-contact-buttons__btn btn mbs-contactform-contacbutton waves-effect">Contact Us</a></div>
            </div>
        </div>
    </div>
</section>
@endsection
