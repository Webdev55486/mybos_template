<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\ImageManagerStatic as Image;

use Corcel\Post as Corcel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('homePage');
    }

    public function product_software()
    {
        return view('softwarePage');
    }

    public function product_hardware()
    {
        return view('hardwarePage');
    }
    public function facility_management()
    {
        return view('buildingFacilityManagement');
    }
    public function property_management()
    {
        return view('stratsPropertyManagement');
    }
    public function construction_development()
    {
        return view('constructionDevelopment');
    }
    public function manage_agents()
    {
        return view('manageAgents');
    }
    public function case_studies()
    {
        return view('caseStudies');
    }
    public function documentation()
    {
        $documents = DB::table('document')->orderBy('id', 'DESC')->get();
        return view('documentation', ['documents' => $documents]);
    }
    public function news()
    {
        return view('news');
    }
    public function support()
    {
        return view('support');
    }
    public function upload_document(Request $request)
    {
      if($request->for_upload_posttype == 'add'){
        $this->validate($request, [
            'document_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $image = $request->document_img;
        $filename  = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('document_images/' . $filename);
        $document_img_path = 'document_images/' . $filename;
        Image::make($image->getRealPath())->resize(180, 120)->save($path);
        $size = Image::make($path)->filesize();
        $description = $request->document_description;
        $data = array("description"=>$description,"image_path"=>$document_img_path,"image_size"=>$size);

        DB::table('document')->insert($data);
        return back()->with('done', '');
      }
      if($request->for_upload_posttype == 'edit') {
        $document_id = $request->for_upload_document_id;
        $description = $request->document_description;
        if($request->document_img){
          $this->validate($request, [
              'document_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          ]);
          $image = $request->document_img;
          $filename  = time() . '.' . $image->getClientOriginalExtension();
          $path = public_path('document_images/' . $filename);
          $document_img_path = 'document_images/' . $filename;
          Image::make($image->getRealPath())->resize(180, 120)->save($path);
          $size = Image::make($path)->filesize();
          $data = array("description"=>$description,"image_path"=>$document_img_path,"image_size"=>$size);
        }
        else {
          $data = array("description"=>$description);
        }
        //
        DB::table('document')->where('id', '=', $document_id)->update($data);
        return back()->with('done', '');
      }
    }
    public function delete_document(Request $request)
    {
      $document_id = $request->document_id;
      $delete_row = DB::table('document')->where('id', '=', $document_id)->get();
      $image_path = public_path($delete_row[0]->image_path);
      unlink($image_path);
      DB::table('document')->where('id', '=', $document_id)->delete();
      Cache::flush();
    }
}
