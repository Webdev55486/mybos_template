$(document).ready(function () {

    $('.mbs-document-delete').on('click', function(){
      var id = $(this).data('documentid');
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
          if (isConfirm) {
            axios.post("/deleteDocument", {document_id:id})
            .then(function (response) {
                $("#document_"+ id).remove();
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            })
            .catch(function (error) {
              console.log(error);
            });
          } else {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
      });
    })
    $('.mbs-document-edit').on('click', function(){
      var id = $(this).data('documentid');
      var imagepath = window.location.protocol + "//" + window.location.host + "/"+$(this).data('documentimagepath');
      $('#for_upload_document_id').val(id);
      $('#for_upload_posttype').val('edit');
      $('.mbs-document-upload-description-field').val($(this).data('documentdescription'))
      $('#image-preview').css('background-image','url('+imagepath+')');
      $('.mbs-document-upload-form__container').css('display','block');
    })
    $('#document-post-and-edit-cancel').on('click', function() {
      $('.mbs-document-upload-form__container').removeClass('uploadform-show');
      $('.mbs-document-upload-form__container').css('display','none');
      $('#image-preview').css('background-image','url("")');
      $('.mbs-document-upload-description-field').val("");
    })

    $('.mbs-show-upload-form').on('click', function() {
      $('.mbs-document-upload-form__container').toggleClass('uploadform-show');
    });

  	function showMenu() {
			$(this).removeClass("drop-collapsed");
			$(this).addClass("open");
		}
		function hideMenu(){
			$(this).removeClass("open");
			var $dropdown = $(".mbs-navbar-dropdown");

			$dropdown.each(function () {
				$(this).addClass("drop-collapsed");
			});
		}


  		var $dropdown = $(".mbs-navbar-dropdown");
      var $dropdownsidebar = $(".mbs-sidebar-dropdown");

			$dropdown.each(function () {
				var $this = $(this);

				var $dropmenu = $this.find(".mbs-navbar-dropdown-menu");
				$dropmenu.css("height", $dropmenu.outerHeight());
				$this.addClass("drop-collapsed");
			});

      $dropdownsidebar.each(function () {
				var $this = $(this);

				var $dropsidebarmenu = $this.find(".mbs-sidebar-dropdown-menu");
				$dropsidebarmenu.css("height", $dropsidebarmenu.outerHeight());
				$this.addClass("drop-collapsed");
			});


			 // dropdown menu hover intent
			var hovsettings = {
					timeout:0,
			    interval: 0,
			    over: showMenu,
			    out: hideMenu
			};

			$(".mbs-navbar-dropdown").hoverIntent(hovsettings);

      $(".mbs-sidebar-dropdown").each(function(){
        var sidebar = $(this);
        sidebar.on('click', function(){
          sidebar.prevAll().each(function(){
            if($(this).hasClass('open')){
              $(this).removeClass('open').addClass('drop-collapsed');
            }
          });
          sidebar.nextAll().each(function(){
            if($(this).hasClass('open')){
              $(this).removeClass('open').addClass('drop-collapsed');
            }
          });
          sidebar.toggleClass('open');
          sidebar.toggleClass('drop-collapsed');
        })
      })

      //scroll animation
      var $window           = $(window),
      win_height_padded = $window.height(),
      isTouch           = Modernizr.touch;

      if (isTouch) { $('.revealOnScroll').addClass('animated'); }
      if (isTouch) { $('.imacOnScroll').addClass('animated'); }

      $window.on('scroll', revealOnScroll);
      $window.on('scroll', imacOnScroll);

      function revealOnScroll() {
        var scrolled = $window.scrollTop(),
            win_height_padded = $window.height()*1;

        // Showed...
        $(".revealOnScroll:not(.animated)").each(function () {
          var $this     = $(this),
              offsetTop = $this.offset().top;

          if (scrolled + win_height_padded > offsetTop) {
            if ($this.data('timeout')) {
              window.setTimeout(function(){
                $this.addClass('animated ' + $this.data('animation'));
              }, parseInt($this.data('timeout'),10));
            } else {
              $this.addClass('animated ' + $this.data('animation'));
            }
          }
        });
      }
      //imac scrol animation
      function imacOnScroll() {
        var scrolled = $window.scrollTop(),
            win_height_padded = $window.height();

        // Showed...
        $(".imacOnScroll:not(.animated)").each(function () {
          var $this     = $(this),
              offsetTop = $('.mbs-section-imac-div').offset().top * 1.15;

          if (scrolled + win_height_padded > offsetTop) {
            if ($this.data('timeout')) {
              window.setTimeout(function(){
                $this.addClass('animated ' + $this.data('animation'));
              }, parseInt($this.data('timeout'),10));
            } else {
              $this.addClass('animated ' + $this.data('animation'));
            }
          }
        });
      }

      imacOnScroll();
      revealOnScroll();
});

window.onload = function () { setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50); }
