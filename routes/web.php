<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('software','HomeController@product_software');
Route::get('hardware','HomeController@product_hardware');
Route::get('industries-facility-management','HomeController@facility_management');
Route::get('industries-property-management','HomeController@property_management');
Route::get('industries-construction-development','HomeController@construction_development');
Route::get('industries-manage-agents','HomeController@manage_agents');
Route::get('resources-case-studies','HomeController@case_studies');
Route::get('resources-documentation','HomeController@documentation');
Route::get('news','HomeController@news');
Route::get('company-support','HomeController@support');
Route::post('document_upload','HomeController@upload_document');
Route::post('deleteDocument','HomeController@delete_document');
